import math
import statistics

# sampleの幅でdataのmodeを取る
# 0だと、statistics.modeと同じ
# ex) ([1, 1, 5, 6, 7], 1) -> 6
def continuousMode(data, sample):
    extendData = []
    for d in data:
        extendData.append(d)
        for i in range (1, sample + 1):
            extendData.append(d + i)
            extendData.append(d - i)

    return statistics.mode(extendData)
