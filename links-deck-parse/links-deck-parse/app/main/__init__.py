from .split import split
from .cardname import cardname


def cards(img):
    '''
    Parameters
    ----------
    img : cv2.image
      クエリのデッキ画像

    Returns
    -------
    names : [str]
      カード名のリスト
    '''
    imgs = split(img)
    names = [cardname(img) for img in imgs]
    return names


__all__ = [cards]
