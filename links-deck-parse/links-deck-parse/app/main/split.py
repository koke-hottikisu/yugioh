import cv2
import numpy as np
import functools
import statistics
import math

from . import mode

def gimp2cv(hsv):
    h, s, v = hsv
    return (int(h * 180 / 360), int(s * 256 / 100), int(v * 256 / 100))

def inRangeGimp(hsv, lower, upper):
    return cv2.inRange(hsv, gimp2cv(lower), gimp2cv(upper))

def getCardMask(img):
    maskRanges = [
        ((9, 25, 150), (25, 160, 240)),    #通常モンスター
        ((5, 35, 127), (15, 180, 240)),    #効果
        ((105, 15, 60), (125, 140, 240)),  #儀式
        ((135, 15, 110), (160, 135, 225)),  #融合
        ((5, 0, 130), (155, 10, 255)),     #シンクロ
        # ((0, 5, 5), (360, 50, 10)),      #エクシーズ なくても認識する、背景と誤認するので適用しない
        ((95, 25, 65), (120, 40, 255)), #リンク
        ((75, 10, 95), (120, 30, 230)),  #魔法
        ((75, 210, 95), (120, 235, 230)),  #魔法2
        ((150, 25, 140), (180, 40, 240)),  #罠
        ((100, 30, 50), (150, 100, 180))   #枠
    ]
    # 検出したい色のみを抜き出す
    hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
    return functools.reduce(lambda prev, curr: cv2.bitwise_or(prev, cv2.inRange(hsv, *curr)),
                            maskRanges,
                            cv2.inRange(hsv, (1, 1, 1), (0, 0, 0)))

def showMask(imgr, mask):
    img = imgr.copy()
    showImg(mask)
    # showImg(cv2.bitwise_and(img, img, mask=mask))

def showRectContours(imgr, rectContours):
    img = imgr.copy()
    for x, y, w, h in rectContours:
        cv2.rectangle(img, (x, y), (x + w, y + h), (0, 0, 255), 2)
    showImg(img)

# 矩形がカードの比率とあっているかどうか
# カードの比率は20:29なので、20:28~30まで
def isCardAspectRatio(w, h):
    return (w * 28 < h * 20) and (h * 20 < w * 30)

# rectのうち、主流rectの(w, h)を返す
def modeRect(rects):
    modeW = mode.continuousMode([r[2] for r in rects], 2)
    modeH = mode.continuousMode([r[3] for r in rects], 2)
    return modeW, modeH

def showWithXY(imgr, xs, ys):
    img = imgr.copy()
    for x in xs:
        cv2.line(img, (x, -9990), (x, 9999), (0, 0, 255), 2)
    for y in ys:
        cv2.line(img, (-9990, y), (9999, y), (0, 0, 255), 2)
    showImg(img)

def showGrid(imgr, xy, w, h):
    img = imgr.copy()
    for x, y in xy:
        cv2.rectangle(img, (x, y), (x + w, y + h), (0, 0, 255), 2)
    showImg(img)

# 集合(data)において、intervalの幅でmodeを取る
def modesValue(data, interval):
    groups = []
    representative = sorted(data)[0] - interval
    for e in sorted(data):
        if e - interval / 2 < representative and representative < e + interval / 2:
            groups[-1].append(e)
        else:
            groups.append([e])
            representative = e
    return [statistics.mode(e) for e in groups]

# カードか、カードのないただの枠かを返す
# カード枠の幅は、200x290に対し7
# カードイラストまでの幅は 横0-24, 176-200 上0-49 下210-290
def notEmptyCard(img):
    h, w = img.shape[:2]
    frameX1 = math.ceil(w * 24 / 200)
    frameY1 = math.ceil(h * 49 / 290)
    frameX2 = math.ceil(w * 176 / 200)
    frameY2 = math.ceil(h * 210 / 290)

    cardMask = getCardMask(img)

    maskBase = np.full(img.shape[:2], 0, dtype=img.dtype)
    framePlaceMask = cv2.rectangle(maskBase, (frameX1, frameY1), (frameX2, frameY2), (255,255,255), -1)
    framePlaceMask = cv2.bitwise_not(framePlaceMask)
    framePlaceBitCount = np.count_nonzero(framePlaceMask == 255)

    frameMask = cv2.bitwise_and(cardMask, framePlaceMask)
    frameBitCount = np.count_nonzero(frameMask == 255)

    return (frameBitCount / framePlaceBitCount) > 0.3

def showImg(img):
    img = cv2.resize(img, dsize=None, fx=0.7, fy=0.7)
    cv2.imshow('window', img)
    cv2.waitKey(0)

# カードからイラスト部分を切り出す
# he, wi -> y1, y2, x1, x2
def cutIllustPoint(he, wi):
    h, w, h1, w1, h2, w2 = 290, 200, 53, 24, 204, 175
    return int(he * h1 / h), int(he * h2 / h), int(wi * w1 / w), int(wi * w2 / w)

def cutIllust(img):
    he, wi = img.shape[:2]
    y1, y2, x1, x2 = cutIllustPoint(he, wi)
    return img[y1: y2, x1: x2]

def showCardsIllust(imgr, modeXY, modeW, modeH):
    img = imgr.copy()
    y1, y2, x1, x2 = cutIllustPoint(modeH, modeW)
    for x, y in modeXY:
        cv2.rectangle(img, (x + x1, y + y1), (x + x2, y + y2), (0, 0, 255), 2)
    showImg(img)

def split(img, debug=False):
    '''
    Parameters
    ----------
    img : cv2.image
      デッキ画像
    
    Returns
    -------
    imgs : [cv2.image]
      カード画像(イラストのみ)
    '''
    mask = getCardMask(img)
    if debug:
        showMask(img, mask)

    # 輪郭を検出
    contours, hierarchy = cv2.findContours(mask, cv2.RETR_TREE,
                                           cv2.CHAIN_APPROX_SIMPLE)
    contoursTop = [c for c, h in zip(contours, hierarchy[0]) if h[3] == -1]

    # 輪郭の外接矩形を検出
    rectContours = [cv2.boundingRect(cnt) for cnt in contoursTop]
    if debug:
        showRectContours(img, rectContours)

    # カードの幅と高さを割り出す
    rectContoursCardAspect = [cnt for cnt in rectContours if isCardAspectRatio(cnt[2], cnt[3])]
    if debug:
        showRectContours(img, rectContoursCardAspect)
    modeW, modeH = modeRect(rectContoursCardAspect)

    # 信頼できるx, y座標データ(右下)を抽出する
    mergin = 0.02
    reliableX = [cnt[0] + cnt[2] for cnt in rectContours if
                 cnt[2] - modeW * mergin < modeW and modeW < cnt[2] + modeW * mergin]
    reliableY = [cnt[1] + cnt[3] for cnt in rectContours if
                 cnt[3] - modeH * mergin < modeH and modeH < cnt[3] + modeH * mergin]
    if debug:
        showWithXY(img, reliableX, reliableY)

    # 近いx, yにおいてmodeを取る
    modeXs = [x - modeW for x in modesValue(reliableX, modeW)]
    modeYs = [y - modeH for y in modesValue(reliableY, modeH)]
    modeXY = []
    for y in modeYs:
        for x in modeXs:
           modeXY.append((x, y))

    # カードがない場所を除く
    modeXY = [(x, y) for x, y in modeXY if notEmptyCard(img[y: y + modeH, x: x + modeW])]
    # showGrid(img, modeXY, modeW, modeH)
    
    cards = [img[y: y + modeH, x: x + modeW] for x, y in modeXY]
    cardsIllust = [cutIllust(img) for img in cards]
    if debug:
        showCardsIllust(img, modeXY, modeW, modeH)

    #for c in cardsIllust:
    #   showImg(c)
    return cardsIllust
