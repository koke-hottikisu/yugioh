import matplotlib.pyplot as plt
import cv2
from PIL import Image
import sys
import math
import numpy as np

def showHist(img):
    img2 = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
    h, s, v = img2[:,:,0], img2[:,:,1], img2[:,:,2]
    hist_h = cv2.calcHist([h],[0],None,[256],[0,256])
    hist_s = cv2.calcHist([s],[0],None,[256],[0,256])
    hist_v = cv2.calcHist([v],[0],None,[256],[0,256])
    plt.plot(hist_h, color='r', label="h")
    plt.plot(hist_s, color='g', label="s")
    plt.plot(hist_v, color='b', label="v")
    plt.legend()
    plt.show()
    return

def showImg(imgr):
    img = imgr.copy()
    cv2.imshow('window', img)
    cv2.waitKey(0)

def maskBack(img):
    h, w = img.shape[:2]
    frameX1 = math.ceil(w * 20 / 200)
    frameY1 = math.ceil(h * 13 / 290)
    frameX2 = math.ceil(w * 180 / 200)
    frameY2 = math.ceil(h * 210 / 290)

    maskBase = np.full(img.shape[:2], 0, dtype=img.dtype)
    framePlaceMask = cv2.rectangle(maskBase, (frameX1, frameY1), (frameX2, frameY2), (255,255,255), -1)
    framePlaceMask = cv2.bitwise_not(framePlaceMask)

    return framePlaceMask

def maskFrame(img):
    h, w = img.shape[:2]
    frameX1 = math.ceil(w * 7 / 200)
    frameY1 = math.ceil(h * 7 / 290)
    frameX2 = math.ceil(w * 193 / 200)
    frameY2 = math.ceil(h * 283 / 290)

    maskBase = np.full(img.shape[:2], 0, dtype=img.dtype)
    framePlaceMask = cv2.rectangle(maskBase, (frameX1, frameY1), (frameX2, frameY2), (255,255,255), -1)

    return framePlaceMask

img = cv2.imread(sys.argv[1])
maskBack = maskBack(img)
maskFrame = maskFrame(img)

#showImg(maskBack)
#showImg(maskFrame)

# card
mask = cv2.bitwise_and(maskBack, maskFrame)

# frame
#mask = cv2.bitwise_not(maskFrame)

# showImg(cv2.bitwise_and(img, img, mask=mask))
showHist(cv2.bitwise_and(img, img, mask=mask))


