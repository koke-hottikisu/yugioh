from flask import Flask, request
import cv2
import base64
import numpy as np
from flask import Flask, jsonify, request
from werkzeug.middleware.profiler import ProfilerMiddleware

from main import cards

app = Flask(__name__)
app.config['JSON_AS_ASCII'] = False

if app.debug:
    app.config['PROFILE'] = True
    app.wsgi_app = ProfilerMiddleware(app.wsgi_app,
                                      sort_by=['tottime'],
                                      restrictions=[20])


def img2base64str(img):
    retval, buffer = cv2.imencode('.png', img)
    png_base64 = base64.b64encode(buffer)
    png_str = png_base64.decode()
    return png_str


def base64str2img(b):
    b = b.split(',')[-1]
    # base64 -> binary
    img_binary = base64.b64decode(b)
    png = np.frombuffer(img_binary, dtype=np.uint8)
    # binary -> img
    img = cv2.imdecode(png, cv2.IMREAD_COLOR)
    return img


@app.route('/', methods=['POST'])
def post_root():
    req = request.json
    q = base64str2img(req['q'])
    names = cards(q)
    return jsonify(names), 200
