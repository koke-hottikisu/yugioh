# Yugioh Web App
遊戯王に関するウェブアプリのリポジトリです

# argocd Applicationの登録
## argocdへのログイン
```
argocd --port-forward --port-forward-namespace argocd \
login localhost:8080 --name admin --username admin --password (kubectl -n argocd get secret argocd-initial-admin-secret -o jsonpath={.data.password} | base64 -d)
```

## argocd Applicationにargocd-image-updater用のannotationを追加

## develop用argocd Applicationの作成
```
ls argocd-config/base | grep -v namespace.yaml | xargs -I@ sh -c \
	'sed \
	-e "s/\(name: .*\)/\1-dev/g" \
	-e "s/:latest/:develop/g" \
	-e "s/targetRevision: master/targetRevision: develop/g" \
	-e "s/\(path:.*\)base/\1dev/g" \
	-e "s/namespace: yugioh-prod/namespace: yugioh-dev/g" \
	argocd-config/base/@ > argocd-config/dev/@'
```

## デプロイ
```
argocd --port-forward --port-forward-namespace argocd \
app create -f argocd-config.yaml

argocd --port-forward --port-forward-namespace argocd \
app create -f argocd-config-dev.yaml
```

