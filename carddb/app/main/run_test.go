package main

import (
	"io/ioutil"
	"main/db"
	"main/myhttp"
	"net/http"
	"strconv"
	"strings"
	"testing"
	"time"

	"github.com/jarcoal/httpmock"
)

func curlResponder(r *http.Request) (*http.Response, error) {
	queryUrl, err := myhttp.GetQueryUrl(r.URL)
	if err != nil {
		return nil, err
	}

	var body []byte
	switch queryUrl.Query().Get("ope") {
	case "":
		body, err = ioutil.ReadFile("run_test/top.html")
	case "1":
		body, err = ioutil.ReadFile("run_test/pack.html")
	case "2":
		body, err = ioutil.ReadFile("run_test/card.html")
		name := strconv.Itoa(httpmock.GetTotalCallCount())
		body = []byte(strings.ReplaceAll(string(body), "SED_ME_CARD_NAME", name))
	}
	if err != nil {
		return nil, err
	}
	resp := httpmock.NewBytesResponse(200, body)
	resp.Header.Add("Curlcache-Hit", "true")
	return resp, nil
}

type CardCollTestUpdate struct{
	count int
}
func (c *CardCollTestUpdate) Count() (int, error) {
	return c.count, nil
}
func (c *CardCollTestUpdate) Upsert(card db.Card) (error) {
	c.count = c.count + 1
	return nil
}
func (c *CardCollTestUpdate) Find(name string) (db.Card, error) {
	return db.Card{}, nil
}
func (c *CardCollTestUpdate) FindAll() ([]db.Card, error) {
	return []db.Card{}, nil
}

type DateCollTestUpdate struct{}
func (c *DateCollTestUpdate) Set(time.Time) (error) {
	return nil
}
func (c *DateCollTestUpdate) Get() (db.DateCollectionData ,error) {
	return db.DateCollectionData{}, nil
}

func TestUpdate(t *testing.T) {
	httpmock.Activate()
	defer httpmock.DeactivateAndReset()
	httpmock.RegisterResponder("GET", `=~^http://curljs:81/.*`, curlResponder)
	httpmock.RegisterResponder("GET", `=~^http://curlcache/.*`, curlResponder)
	httpmock.RegisterResponder("GET", `=~^https://www\.db\.yugioh-card\.com/yugiohdb/card_list\.action.*`, curlResponder)

	card_coll := &CardCollTestUpdate{count: 0}
	date_coll := &DateCollTestUpdate{}
	Update(card_coll, date_coll)

	if c, _ := card_coll.Count(); c != 27 {
		t.Fatalf("登録されたカード枚数が足りません。 expect: %d  actual: %d", 27, c)
	}
}

func TestUpdateTime(t *testing.T) {
	httpmock.Activate()
	defer httpmock.DeactivateAndReset()
	httpmock.RegisterResponder("GET", `=~^http://curljs:81/.*`, curlResponder)
	httpmock.RegisterResponder("GET", `=~^http://curlcache/.*`, curlResponder)
	httpmock.RegisterResponder("GET", `=~^https://www\.db\.yugioh-card\.com/yugiohdb/card_list\.action.*`, curlResponder)

	card_coll := &CardCollTestUpdate{count: 0}
	date_coll := &DateCollTestUpdate{}

	now := time.Now()
	Update(card_coll, date_coll)

	if time.Since(now) > time.Second {
		t.Fatalf("updateの実行がおそすぎます。%dcards/%dsec", 27, time.Since(now))
	}
}
