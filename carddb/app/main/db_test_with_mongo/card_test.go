package db_test_with_mongo

import (
	"context"
	"main/db"
	"testing"
	"time"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

func TestUpsert(t *testing.T) {
	ctx, cancel := context.WithTimeout(context.Background(), 10 * time.Second)
	defer cancel()
	client, err := mongo.Connect(ctx, options.Client().ApplyURI("mongodb://carddb-test:27017"))
	defer func() {
    if err = client.Disconnect(ctx); err != nil {
			panic(err)
    }
	}()
	if err != nil {t.Fatal(err)}

	cardColl := db.CardCollection(client)

	cardColl.Drop(context.Background())

	err = cardColl.Upsert(db.Card{
		Name:        "a",
	})
	if err != nil {t.Fatal(err)}
	err = cardColl.Upsert(db.Card{
		Name:     "b",
	})
	if err != nil {t.Fatal(err)}
	err = cardColl.Upsert(db.Card{
		Name:     "a",
	})
	if err != nil {t.Fatal(err)}

	count, err := cardColl.Count()
	if err != nil {t.Fatal(err)}
	if count != 2 {t.Fatalf("ans: 2,  res: %d", count)}
}

func TestFindAll(t *testing.T) {
	ctx, cancel := context.WithTimeout(context.Background(), 10 * time.Second)
	defer cancel()
	client, err := mongo.Connect(ctx, options.Client().ApplyURI("mongodb://carddb-test:27017"))
	defer func() {
    if err = client.Disconnect(ctx); err != nil {
			panic(err)
    }
	}()
	if err != nil {t.Fatal(err)}

	cardColl := db.CardCollection(client)

	cardColl.Drop(context.Background())

	err = cardColl.Upsert(db.Card{
		Name:        "a",
	})
	if err != nil {t.Fatal(err)}
	err = cardColl.Upsert(db.Card{
		Name:     "b",
	})
	if err != nil {t.Fatal(err)}

	res, err := cardColl.FindAll()
	if err != nil {t.Fatal(err)}
	if len(res) != 2 {t.Fatalf("ans: 2,  res: %d", len(res))}
}
