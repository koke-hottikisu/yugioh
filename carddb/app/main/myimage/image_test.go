package myimage

import (
	"io/ioutil"
	"testing"
)

func TestResize(t *testing.T) {
	data, err := ioutil.ReadFile("./testdata.txt")
	if err != nil {
		t.Fatal(err)
	}

	newimg, err := Resize(string(data), 0.5)
	if err != nil {
		t.Fatal(err)
	}

	img, err := fromBase64(newimg)
	if err != nil {
		t.Fatal(err)
	}
	if img.Bounds().Dx() != 100 {
		t.Fatalf("resize failed. res: %d ans: %d", img.Bounds().Dx(), 100)
	}
}
