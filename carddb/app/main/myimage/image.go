package myimage

import (
	"bytes"
	"encoding/base64"
	"image"
	"image/png"
	_ "image/png"
	"strings"

	"github.com/nfnt/resize"
)

func toBase64(img image.Image) (string, error) {
	imageByte := new(bytes.Buffer)
	err := png.Encode(imageByte, img)
	if err != nil {
		return "", err
	}

	return base64.StdEncoding.EncodeToString(imageByte.Bytes()), nil
}

func fromBase64(b string) (image.Image, error) {
	reader := base64.NewDecoder(base64.StdEncoding, strings.NewReader(b))
	img, _, err := image.Decode(reader)
	if err != nil { return nil, err }

	return img, nil
}

func Resize(b string, scale float64) (string, error) {
	img, err := fromBase64(b)
	if err != nil {
		return "", err
	}

	width := float64(img.Bounds().Dx()) * scale
	newImg := resize.Resize(uint(width) , 0, img, resize.Lanczos3)

	return toBase64(newImg)
}
