package cardfilter

import (
	"main/db"
	"time"
)

func FilterByDate(cards []db.Card, date time.Time) (res []db.Card, err error) {
	res = []db.Card{}

	for _, c := range cards {
		d, err := time.Parse("2006-01-02", c.ReleaseDate)
		if err != nil { return nil, err	}
		if d.Before(date) {
			res = append(res, c)
		}
	}

	return res, nil
}

func Filter02(cards []db.Card) (res []db.Card, err error) {
	res = []db.Card{}

	return FilterByDate(cards, time.Date(2002, 3, 21, 0, 0, 0, 1, time.UTC))
}

func Filter05(cards []db.Card) (res []db.Card, err error) {
	res = []db.Card{}

	return FilterByDate(cards, time.Date(2008, 2, 23, 0, 0, 0, 1, time.UTC))
}
