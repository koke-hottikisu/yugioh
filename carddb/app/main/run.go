package main

import (
	"crypto/md5"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"main/db"
	"main/exporter"
	"main/handler"
	"main/myhttp"
	"main/mys3"
	"main/scraping"
	"net/http"
	"net/url"
	"strings"
	"time"

	"github.com/gin-contrib/pprof"
	"github.com/gin-gonic/gin"
	"github.com/prometheus/client_golang/prometheus"
)

func main() {
	registry := exporter.NewRegistry()

	client, _, _ := db.GetClient()
	go exporter.KeepPollingExporter(registry, db.CardCollection(client))
	
	go func() {
		client, discon, _ := db.GetClient()
		defer discon()
		Update(db.CardCollection(client), db.DateCollection(client))
	}()

	setupGin(registry).Run(":80")
}

func setupGin(reg *prometheus.Registry) *gin.Engine {
	engine := gin.Default()

	pprof.Register(engine)

	engine.GET("/update", updateHandler())
	engine.GET("/update-if-empty", updateIfEmptyHandler())

	engine.GET("/card", cardHandler())
	engine.GET("/card02all", handler.Get02Handler())
	engine.GET("/card05all", handler.Get05Handler())

	engine.GET("/metrics", func(c *gin.Context) {
		exporter.GetHttpHandler(reg).ServeHTTP(c.Writer, c.Request)
	})
	return engine
}

func isContains(list []string, target string) bool {
	for _, f := range list {
		if f == target { return true }
	}
	return false
}

/*
   /card?field=image&imagescale=0.5
*/
func cardHandler() func(*gin.Context) {
	return func(c *gin.Context) {
		name := c.Query("name")

		client, discon, err := db.GetClient()
		defer discon()
		if err != nil {
			c.String(http.StatusBadRequest, "")
			return
		}
		cardColl := db.CardCollection(client)

		card, err := cardColl.Find(name)
		if err != nil {
			c.String(http.StatusBadRequest, "")
			return
		}

		cardstr, err := json.Marshal(card)
		if err == nil {
			etag := fmt.Sprintf("\"%x\"", md5.Sum(cardstr))
			c.Header("ETag", etag)

			if match := c.GetHeader("If-None-Match"); match != "" {
				if strings.Contains(match, etag) {
					c.Status(http.StatusNotModified)
					return
				}
			}
		}

		c.Header("Cache-Control", "public, max-age=86400")
		c.JSON(http.StatusOK, card)
	}
}

func updateHandler() func(*gin.Context) {
	return func(c *gin.Context) {
		client, discon, err := db.GetClient()
		if err != nil {
			c.String(http.StatusBadRequest, "")
			return
		}
		cardColl := db.CardCollection(client)
		dateColl := db.DateCollection(client)

		go func(){
			defer discon()
			Update(cardColl, dateColl)
		}()
	}
}

func updateIfEmptyHandler() func(*gin.Context) {
	return func(c *gin.Context) {
		client, discon, err := db.GetClient()
		if err != nil {
			c.String(http.StatusBadRequest, "")
			return
		}
		cardColl := db.CardCollection(client)
		dateColl := db.DateCollection(client)

		go func(){
			defer discon()
			UpdateIfEmpty(cardColl, dateColl)
		}()
	}
}

func UpdateIfEmpty(cards_coll db.CardCollectionI, dateColl db.DateCollectionI) {
	count, err := cards_coll.Count()
	if err != nil {
		log.Printf("E UpdateIfEmpty: can not count documents")
	}

	if count == 0 {
		Update(cards_coll, dateColl)
	}
}

func Update(card_coll db.CardCollectionI, dateColl db.DateCollectionI) {
	log.Printf("[info] %s Start Update", time.Now().String())

	toppage := "https://www.db.yugioh-card.com/yugiohdb/card_list.action?request_locale=ja"
	html, err := getHtml(myhttp.Curl(toppage, "", false, true))
	if err != nil {
		log.Printf("E Update: can not get toppage  %s", err)
	}
	packUrls := scraping.PackUrls(html)

	for _, packUrl := range packUrls {
		html, err := getHtml(myhttp.Curl(packUrl, "", true, true))
		if err != nil {
			log.Println(err.Error())
			continue
		}
		cardUrls := scraping.CardUrls(html)

		for _, cardUrl := range cardUrls {
			html, err := getHtml(myhttp.Curl(cardUrl, "", true, true))
			if err != nil {
				log.Println(err.Error())
				continue
			}

			doc, err := scraping.ScrapingDocument(html)
			if err != nil {
				log.Println(err.Error())
				continue
			}

			card, err := scraping.Card(doc, cardUrl)
			if err != nil {
				log.Printf("[Error] Skip register card to DB (%s): %s", cardUrl, err)
				continue
			}

			card = registS3(card)
			err = card_coll.Upsert(card)
			if err != nil {
				log.Println(err)
				continue
			}
		}
	}

	dateColl.Set(time.Now())
	log.Printf("[info] %s End Update", time.Now().String())
}

func getHtml(body io.ReadCloser, err error) (string, error) {
	if err != nil {
		return "", err
	}
	res, err := ioutil.ReadAll(body)
	body.Close()
	return string(res), err
}

func registS3(card db.Card) db.Card {
	var urlList []string
	for index, u := range card.ImageSrcUrl {
		i, err := myhttp.Curl(u, card.Url, true, false)
		if err != nil {
			log.Println(err)
			continue
		}
		cardurl, err := url.Parse(card.Url)
		if err != nil {
			log.Println(err)
			continue
		}

		cid := cardurl.Query().Get("cid")
		key := fmt.Sprintf("card/%s-%d.png", cid, index)
		_, url, err := mys3.UploadIfEmpty(i, key)
		if err != nil {
			log.Println(err)
			continue
		}
		urlList = append(urlList, url)
	}
	card.ImageUrl = urlList
	return card
}
