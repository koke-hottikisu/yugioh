package db

import (
	"context"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

const (
	TimeFormat = "2006-01-02T15:04:05.000Z"
)

type DateCollectionI interface {
	Set(time.Time) error
	Get() (DateCollectionData, error)
}

type DateCollectionMongo struct {
	*mongo.Collection
}

func DateCollection(client *mongo.Client) DateCollectionMongo {
	return DateCollectionMongo{
		client.Database("yugioh").Collection("update-timestamp"),
	}
}

func (c DateCollectionMongo) Set(t time.Time) error {
	update_data := bson.M{"$set": bson.M{
		"timestamp": t.Unix(),
	}}
	_, err := c.UpdateOne(context.Background(), bson.M{}, update_data, options.Update().SetUpsert(true))
	if err != nil {
		return err
	}
	return nil
}

func (c DateCollectionMongo) Get() (data DateCollectionData, err error) {
	err = c.FindOne(context.Background(), bson.M{}).Decode(&data)
	return
}

type DateCollectionData struct {
	Timestamp int64 "timestamp"
}
