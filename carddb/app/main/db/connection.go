package db

import (
	"context"
	"log"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

func GetClient() (client *mongo.Client, discon func()(), err error) {
	ctx := context.Background()
	client, err = mongo.Connect(ctx, options.Client().ApplyURI("mongodb://carddb-db:27017"))

	discon = func() {
    if err = client.Disconnect(ctx); err != nil {
			log.Printf("[error] can not disconnect client")
    }
	}

	return
}
