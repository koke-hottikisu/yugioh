package db

import (
	"context"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
)

type Card struct {
	Name string `bson:"name" json:"name"`
	Ruby string `bson:"ruby" json:"ruby"`
	ImageSrcUrl []string `bson:"imagesrcurl" json:"imagesrcurl"`
	ImageUrl []string `bson:"imageurl" json:"imageurl"`
	ReleaseDate string `bson:"releasedate" json:"releasedate"`
	Url string `bson:"url" json:"url"`
	Text string `bson:"text" json:"text"`
	Atk string `bson:"atk" json:"atk"`
	Def string `bson:"def" json:"def"`
	// 属性・魔法罠の種類
	Attribute string `bson:"attribute" json:"attribute"`
	MonsterType string `bson:"monstertype" json:"monstertype"`
	Level int `bson:"level" json:"level"`
	// シンクロ・効果とか
	CardType []string `bson:"cardtype" json:"cardtype"`
}

type CardCollectionI interface {
	Upsert(card Card) error
	Count() (int, error)
	Find(name string) (Card, error)
	FindAll() ([]Card, error)
}

type CardCollectionMongo struct {
	*mongo.Collection
}

func CardCollection(client *mongo.Client) CardCollectionMongo {
	return CardCollectionMongo{
		client.Database("yugioh").Collection("cards"),
	}
}

func (c CardCollectionMongo) Find(name string) (card Card, err error) {
	err = c.FindOne(context.Background(), bson.M{"name": name}).Decode(&card)
	return
}

func (c CardCollectionMongo) FindAll() (cards []Card, err error) {
	cards = []Card{}
	cur, err := c.Collection.Find(context.Background(), bson.M{})
	if err != nil {
		return nil, err
	}

	cur.All(context.Background(), &cards)
	return cards, nil
}

func (c CardCollectionMongo) Upsert(card Card) error {
	ctx, cancel := context.WithTimeout(context.Background(), 30 * time.Second)
	defer cancel()
	c.FindOneAndDelete(ctx, bson.M{"name": card.Name})
	_, err := c.InsertOne(ctx, card)
	if err != nil {
		return err
	}
	return nil
}

func (c CardCollectionMongo) Count() (int, error) {
	v, e := c.CountDocuments(context.Background(), bson.M{}, nil)
	if e != nil {
		return 0, e
	}
	return int(v), e
}
