package handler

import (
	"crypto/md5"
	"fmt"
	"log"
	"main/cardfilter"
	"main/db"
	"net/http"
	"strings"

	"github.com/gin-gonic/gin"
)

func GetFilterHandler(filter func([]db.Card)([]db.Card, error)) func(*gin.Context) {
	return func(c *gin.Context) {
		client, discon, err := db.GetClient()
		defer discon()
		if err != nil {
			c.String(http.StatusBadRequest, "")
			return
		}
		cardColl := db.CardCollection(client)
		dateColl := db.DateCollection(client)

		// set ETAG for using cache
		updatetime, err := dateColl.Get()
		if err != nil {
			c.String(http.StatusInternalServerError, "")
			return
		}

		etag := fmt.Sprintf("\"%x\"", md5.Sum(
			[]byte(fmt.Sprintf("%v v2", updatetime.Timestamp)),
		))

		if match := c.GetHeader("If-None-Match"); match != "" {
			if strings.Contains(match, etag) {
				log.Printf("[info] using cache")
				c.Status(http.StatusNotModified)
				return
			}
		}

		allcards, err := cardColl.FindAll()
		if err != nil {
			c.String(http.StatusInternalServerError, "")
			return
		}

		res, err := filter(allcards)
		if err != nil {
			c.String(http.StatusInternalServerError, "")
			return
		}

		log.Printf("[info] not using cache")

		c.Header("ETag", etag)
		c.JSON(http.StatusOK, res)
	}
}

func Get02Handler() func(*gin.Context) {
	return GetFilterHandler(cardfilter.Filter02)
}

func Get05Handler() func(*gin.Context) {
	return GetFilterHandler(cardfilter.Filter05)
}
