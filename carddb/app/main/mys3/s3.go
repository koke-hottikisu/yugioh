package mys3

import (
	"fmt"
	"io"
	"os"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/awserr"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
	"github.com/aws/aws-sdk-go/service/s3/s3manager"
)

var (
	awsAccessKey = os.Getenv("AWS_ACCESS_KEY_ID")
	awsSecretKey = os.Getenv("AWS_SECRET_ACCESS_KEY")
	region = os.Getenv("BUCKET_REGION")
	buecktName = os.Getenv("BUCKET_NAME")
	endpoint = os.Getenv("S3_ENDPOINT")
	publicendpoint = os.Getenv("PUBLIC_S3_ENDPOINT")
)

func s3session() *session.Session {
	return session.Must(session.NewSession(&aws.Config{
		Credentials:      credentials.NewStaticCredentials(awsAccessKey, awsSecretKey, ""),
		Endpoint:         aws.String(endpoint),
		Region:           aws.String(region),
		S3ForcePathStyle: aws.Bool(true),
	}))
}

func Upload(data io.ReadCloser, key string) (error) {
	sess := s3session()
	uploader := s3manager.NewUploader(sess)

	_, err := uploader.Upload(&s3manager.UploadInput{
		Body:   data,
		Bucket: aws.String(buecktName),
		Key:    aws.String(fmt.Sprintf("/%s", key)),
		ContentType: aws.String("image/png"),
	})
	return err
}

func keyExists(key string) (bool, error) {
	s3svc := s3.New(s3session())
	_, err := s3svc.HeadObject(&s3.HeadObjectInput{
		Bucket: aws.String(buecktName),
		Key:    aws.String(key),
	})
	if err != nil {
		if aerr, ok := err.(awserr.Error); ok {
			switch aerr.Code() {            
			case "NotFound": // s3.ErrCodeNoSuchKey does not work, aws is missing this error code so we hardwire a string
				return false, nil
			default:
				return false, err
			}
		}
		return false, err
	}
	return true, nil
}

func UploadIfEmpty(data io.ReadCloser, key string) (ok bool, url string, err error) {
	exists, err := keyExists(key)
	if err != nil { return false, "", err }

	url = fmt.Sprintf("%s/%s", publicendpoint, key)
	if !exists {
		err = Upload(data, key)
		return exists, url, err
	}
	return exists, url, nil
}
