package exporter

import (
	"io/ioutil"
	"main/db"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"github.com/gin-gonic/gin"
)

type CardCollTest struct {
	db.CardCollectionI
}

func (c CardCollTest) Count() (int, error) {
	return 3, nil
}
func (c CardCollTest) Upsert(card db.Card) (error) {
	return nil
}

func TestExporter(t *testing.T) {
	reg := NewRegistry()
	cardColl := &CardCollTest{}
	go KeepPollingExporter(reg, cardColl)

	engine := gin.Default()
	engine.GET("/metrics", func(c *gin.Context) {
		GetHttpHandler(reg).ServeHTTP(c.Writer, c.Request)
	})
	ts := httptest.NewServer(engine)
	defer ts.Close()

	resp, err := http.Get(ts.URL + "/metrics")

	data, err := ioutil.ReadAll(resp.Body)
	resp.Body.Close()
	if err != nil {
		t.Fatalf("can not read metrics response.\n  %s", err)
	}

	if !strings.Contains(string(data), "carddb_card_num 3") {
		t.Fatalf("メトリクスに、carddb_card_num 3 が含まれていません\n%s", string(data))
	}
}
