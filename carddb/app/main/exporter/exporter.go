package exporter

import (
	"log"
	"main/db"
	"main/myhttp"
	"net/http"
	"time"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
	"github.com/prometheus/client_golang/prometheus/promhttp"
)

func NewRegistry() *prometheus.Registry {
	return prometheus.NewRegistry()
}

func KeepPollingExporter(reg *prometheus.Registry, cardColl db.CardCollectionI) {
	cacheNum := promauto.With(reg).NewGaugeVec(
		prometheus.GaugeOpts{
			Namespace:   "",
			Subsystem:   "",
			Name:        "carddb_use_cache",
			Help:        "Cache hit counter",
		},
		[]string{"isHit"},
	)
	cardNum := promauto.With(reg).NewGauge(
		prometheus.GaugeOpts{
			Namespace:   "",
			Subsystem:   "",
			Name: "carddb_card_num",
			Help: "Num of registerd card at Carddb",
		})

	for {
		count, err := cardColl.Count()
		if err != nil {
			log.Println(err)
		}
		cardNum.Set(float64(count))

		cacheNum.With(prometheus.Labels{"isHit": "hit"}).Set(float64(myhttp.CountCacheHit))
		cacheNum.With(prometheus.Labels{"isHit": "miss"}).Set(float64(myhttp.CountCacheMiss))

		time.Sleep(time.Second * 10)
	}
}


func GetHttpHandler(reg *prometheus.Registry) http.Handler {
	return promhttp.HandlerFor(reg, promhttp.HandlerOpts{})
}
