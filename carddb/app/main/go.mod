module main

go 1.14

require (
	github.com/PuerkitoBio/goquery v1.7.1
	github.com/aws/aws-sdk-go v1.42.7
	github.com/carlescere/scheduler v0.0.0-20170109141437-ee74d2f83d82
	github.com/dvsekhvalnov/jose2go v1.5.0
	github.com/gin-contrib/pprof v1.3.0
	github.com/gin-gonic/gin v1.7.2
	github.com/go-playground/validator/v10 v10.7.0 // indirect
	github.com/golang/protobuf v1.5.2 // indirect
	github.com/jarcoal/httpmock v1.0.8
	github.com/json-iterator/go v1.1.11 // indirect
	github.com/kylelemons/godebug v1.1.0 // indirect
	github.com/leodido/go-urn v1.2.1 // indirect
	github.com/mattn/go-isatty v0.0.13 // indirect
	github.com/modern-go/concurrent v0.0.0-20180306012644-bacd9c7ef1dd // indirect
	github.com/modern-go/reflect2 v1.0.1 // indirect
	github.com/mongo-go/testdb v0.0.0-20201209140737-c4845cc6fe67 // indirect
	github.com/nfnt/resize v0.0.0-20180221191011-83c6a9932646
	github.com/prometheus/client_golang v1.11.0
	github.com/ugorji/go v1.2.6 // indirect
	go.mongodb.org/mongo-driver v1.6.0
	golang.org/x/crypto v0.0.0-20210711020723-a769d52b0f97 // indirect
	golang.org/x/sys v0.0.0-20210630005230-0f9fa26af87c // indirect
	google.golang.org/protobuf v1.27.1 // indirect
	gopkg.in/yaml.v2 v2.4.0 // indirect
)
