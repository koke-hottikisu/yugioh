package main

import (
	"log"
	"runtime"
	"time"

	"github.com/carlescere/scheduler"
	"github.com/hashicorp/go-retryablehttp"
)

func main() {
	retryClient := retryablehttp.NewClient()
	retryClient.RetryWaitMin = 5 * time.Second
	retryClient.RetryWaitMax = 60 * time.Minute
	retryClient.RetryMax = 9

	scheduler.Every().Thursday().Run(func() {
		retryClient.Get("http://localhost:80/update")
		log.Printf("%s [info] Done Weelly Update Request", time.Now().String())
	})

	scheduler.Every(5).Minutes().Run(func() {
		retryClient.Get("http://localhost:80/update-if-empty")
		log.Printf("%s [info] Done Minutesly Update-If-Empty Request", time.Now().String())
	})

	runtime.Goexit()
}
