package main

import (
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestPingRoute(t *testing.T) {
	router := setupRouter()

	w := httptest.NewRecorder()
	req, _ := http.NewRequest("GET", "/ping", nil)
	router.ServeHTTP(w, req)

	if w.Code != 200 {
		t.Fatalf("Return code is not 200, but %d", w.Code)
	}
	if w.Body.String() != "pong" {
		t.Fatalf("Response Body is not %s, but %s", "pong", w.Body.String())
	}
}
