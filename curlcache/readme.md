# 概要
html取得の際にキャッシュを使えるようにするサービスです。

`/cachedir`にキャッシュを置きます。必要に応じて永続化してください。

# API
http://hostname?q={url}&referer={referer_url}

* url[必須] : base64url encodeされたurl
* referer_url[任意] : base64url encodeされたurl
