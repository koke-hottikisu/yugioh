package search

import (
	"main/db"
	"testing"
)

func TestSimpleStr(t *testing.T) {
	res := simpleStr("aA*0ア+あ")
	if res != "AA0ああ" {
		t.Fatalf("expect: %s, actual: %s", "AA0ああ", res)
	}
}

func TestPartialMatch(t *testing.T) {
	set := []struct{
		target string
		query string
		ans bool
	}{
		{
			target: "ゴブリン突撃部隊",
			query:  "ゴブ突",
			ans:    true,
		},
		{
			target: "ゴブリン突撃部隊",
			query:  "ゴブ五",
			ans:    false,
		},
		{
			target: "ゴブリン突撃部隊",
			query:  "リンゴ",
			ans:    false,
		},
	}

	for _, s := range set {
		ans := s.ans
		res := partialMatch(s.target, s.query)
		if ans != res {
			t.Fatalf("%s %s %t %t", s.target, s.query, ans, res)
		}
	}

}

func TestAbbervRate(t *testing.T) {
	set := []struct{
		target string
		query string
		ans float64
	}{
		{
			target: "ゴブリン突撃部隊",
			query:  "ゴブ突",
			ans:    1,
		},
		{
			target: "ゴブリン突撃部隊",
			query:  "ゴブ五分",
			ans:    0.5,
		},
	}

	for _, s := range set {
		ans := s.ans
		res := abbrevRate(s.target, s.query)
		if ans != res {
			t.Fatalf("%s %s %f %f", s.target, s.query, ans, res)
		}
	}

}

func TestSearch(t *testing.T) {
	set := []struct {
		data []db.Card
		query string
		ans string
	}{
		{
			data: []db.Card{
				{Name: "11",	Ruby: "a"},
				{Name: "12",	Ruby: "aaa"},
				{Name: "13",	Ruby: "b"},
				{Name: "14",	Ruby: "bbb"},
				{Name: "15",	Ruby: "cde"},
			},
			query: "b",
			ans: "13",
		},
		{
			data: []db.Card{
				{Name: "1",	Ruby: "あ"},
				{Name: "2",	Ruby: "あああ"},
				{Name: "3",	Ruby: "か"},
				{Name: "4",	Ruby: "かきく"},
				{Name: "5",	Ruby: "さしす"},
			},
			query: "か",
			ans: "3",
		},
		{
			data: []db.Card{
				{Name: "東京",	Ruby: "とうきょう"},
				{Name: "神奈川",	Ruby: "かながわ"},
				{Name: "埼玉",	Ruby: "さいたま"},
				{Name: "千葉",	Ruby: "ちば"},
				{Name: "茨城",	Ruby: "いばらき"},
			},
			query: "玉",
			ans: "埼玉",
		},
		{
			data: []db.Card{
				{Name: "干ばつの結界像",	Ruby: "かんばつのけっかいぞ"},
				{Name: "万能地雷グレイモヤ",	Ruby: "ばんのうじらいグレイモヤ"},
			},
			query: "ばんのう",
			ans: "万能地雷グレイモヤ",
		},
		{
			data: []db.Card{
				{Name: "ジュラック・イグアノン",	Ruby: "ジュラック・イグアノン"},
				{Name: "万能地雷グレイモヤ",	Ruby: "ばんのうじらいグレイモヤ"},
			},
			query: "じらいぐ",
			ans: "万能地雷グレイモヤ",
		},
	}

	for _, s := range set {
		res := GetTarget(s.data).Search(s.query)
		if res[0] != s.ans {
			t.Fatalf("expect: %s , actual: %s", s.ans, res[0])
		}
	}
}
