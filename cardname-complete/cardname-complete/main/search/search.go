package search

import (
	"log"
	"main/db"
	"regexp"
	"sort"
	"strings"

	"github.com/adrg/strutil"
	"github.com/adrg/strutil/metrics"
)

type Target[] struct {
	Name string //カード名そのまま
	SimpleStr string //ルビ(英語大文字・ひらがな)
}

// 検索しやすいように、文字列を絞る
// A-Z, 大文字ひらがな, 0-9
var simpleStrRegexp = regexp.MustCompile(`[A-Z0-9あ-んー]*`)
func simpleStr(q string) string {
	q = strings.ToUpper(q)
	q = KatakanaToHiragana(q)

	matches := simpleStrRegexp.FindAllStringSubmatch(q, -1)

	res := ""
	for _, match := range matches {
		res = res + match[0]
	}

	return res
}

func GetTarget(card []db.Card) Target {
	res := Target{}
	for _, c := range card {
		res = append(res, struct{Name string; SimpleStr string}{
			Name:      c.Name,
			SimpleStr: simpleStr(c.Ruby),
		})
	}
	return res
}

func (t Target) Search(queryName string) ([]string) {
	type Score struct {
		Name string
		Similarity float64
	}

	scores := []Score{}
	for _, target := range t {
		scores = append(scores,
			Score{
				Name:       target.Name,
				Similarity: slimilarity(target, queryName),
			},
		)
	}

	sort.Slice(scores, func(i, j int) bool {
		return scores[i].Similarity > scores[j].Similarity
	})

	var res []string
	for _, s := range scores {
		res = append(res, s.Name)
	}
	return res
}

// return [0, 1]
// いい感じの省略形かどうか
func abbrevRate(target string, query string) float64 {
	targetRune := []rune(target)
	targetLen := len(targetRune)

	matchNum := 0
	latestMatch := -1
	for _, queryRune := range []rune(query) {
		seek := latestMatch + 1
		for seek < targetLen {
			v := targetRune[seek]
			if v == queryRune {
				matchNum = matchNum + 1
				latestMatch = seek
				break
			}
			seek = seek + 1
		}
	}

	if len([]rune(query)) == 0 {
		return 0.0
	}

	rate := float64(matchNum) / float64(len([]rune(query)))
	//log.Printf("%f %f %f %d", float64(matchNum), float64(len([]rune(query))), rate, int(rate))
	return rate
}

// クエリがターゲットの一部かどうか
// 連続していなくても良い
func partialMatch(target string, query string) bool {
	targetRune := []rune(target)
	targetLen := len(targetRune)

	seekTarget := 0
	for _, queryRune := range []rune(query) {
		for true {
			if seekTarget >= targetLen {
				return false
			}
			if targetRune[seekTarget] == queryRune {
				seekTarget++
				break
			}
			seekTarget++
		}
	}

	return true
}

func slimilarity(target struct{Name string; SimpleStr string}, queryName string) float64 {
	querySimpleStr := simpleStr(queryName)
	res := 0.0

	//一致
	if (queryName == target.Name || querySimpleStr == target.SimpleStr) {
		res += 1000
	}

	// 包含
	if strings.Contains(target.Name, queryName) || strings.Contains(target.SimpleStr, querySimpleStr) {
		res += 500
	}

	// 部分一致
	if partialMatch(target.Name, queryName) || partialMatch(target.SimpleStr, querySimpleStr) {
		res += 100
	}

	// 略称での一致
	res = res + 10 * abbrevRate(target.Name, queryName)
	res = res + 10 * abbrevRate(target.SimpleStr, querySimpleStr)

	// 名前での一致
	res = res + strutil.Similarity(queryName, target.Name, metrics.NewLevenshtein())
	// よみがなでの一致
	res = res + strutil.Similarity(querySimpleStr, target.SimpleStr, metrics.NewLevenshtein())
	log.Printf("%s\t%s\t%f", target.Name, queryName, res)

	return res
}
