package db

import (
	"context"
	"log"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

func getClient() (*mongo.Client, error) {
	db_url := "mongodb://carddb-db:27017"
	client, err := mongo.Connect(context.Background(), options.Client().ApplyURI(db_url))
	if err != nil {
		log.Println("Can not Connect to db: " + db_url)
		return nil, err
	}

	return client, err
}
