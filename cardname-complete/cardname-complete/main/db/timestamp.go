package db

import (
	"context"

	"go.mongodb.org/mongo-driver/bson"
)

type DateColl struct {
	Timestamp int64 "timestamp"
}
func GetTimestamp() (int64, error) {
	client, err := getClient()
	defer client.Disconnect(context.Background())

	if err != nil {
		return 0, err
	}

  var res DateColl
	client.Database("yugioh").Collection("update-timestamp").FindOne(context.Background(), bson.M{}).Decode(&res)

	timestamp := res.Timestamp
	return timestamp, nil
}
