package db

import (
	"context"
	"log"

	"go.mongodb.org/mongo-driver/bson"
)

type Card struct {
	Name string "name"
	Ruby string "ruby"
}

func GetCard() ([]Card, error) {
	client, err := getClient()
	defer func() {
    if err = client.Disconnect(context.Background()); err != nil {
			panic(err)
    }
	}()
	if err != nil {
		return nil, err
	}

	coll := client.Database("yugioh").Collection("cards")
	cur, err := coll.Find(context.Background(), bson.M{}, nil)
	if err != nil {
		return nil, err
	}

	res := []Card{}
	for cur.Next(context.Background()) {
		var doc Card
		if err := cur.Decode(&doc); err != nil {
			log.Printf("カードデータをDBから読み込めないので、次の要素へスキップします\n%s", err)
			continue
		}

		//念のためdeepcopy
		res = append(res, Card{
			Name: doc.Name,
			Ruby: doc.Ruby,
		})
	}
	
	return res, nil
}
