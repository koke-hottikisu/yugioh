package main

import (
	"log"
	"main/db"
	"main/search"
	"strconv"
	"time"

	"github.com/gin-contrib/pprof"
	"github.com/gin-gonic/gin"
)

var (
	target = search.Target{}
	lastUpdate = int64(0)
)

func setupRouter() *gin.Engine {
	r := gin.Default()

	pprof.Register(r)

	r.GET("/", SearchGin())
	return r
}

func main() {
	go pollingCard()

	r := setupRouter()
	r.Run(":80")
}

func pollingCard() {
	for {
		time.Sleep(time.Minute)

		timestamp, err := db.GetTimestamp()
		if err != nil {
			continue
		}

		if lastUpdate < timestamp {
			log.Printf("%s carddb is updated", time.Now())
			d, err := db.GetCard()
			if err != nil {
				continue
			}

			target = append(search.Target{}, search.GetTarget(d)...) 

			lastUpdate = time.Now().Unix()
			log.Printf("%s done polling", time.Now())
		} else {
			log.Printf("%s carddb is not updated", time.Now())
		}
	}
}

func SearchGin() func(c *gin.Context) {
	return func(c *gin.Context){
		query := c.Query("q")
		size, err := strconv.Atoi(c.Query("size"))
		if err != nil {
			c.JSON(400, gin.H{"res": "size is required int"})
		}

		res := target.Search(query)

		c.JSON(200, gin.H{"res": res[0:size]})
	}
}
