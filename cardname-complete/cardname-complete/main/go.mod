module main

go 1.14

require (
	github.com/adrg/strutil v0.2.3
	github.com/gin-contrib/pprof v1.3.0
	github.com/gin-gonic/gin v1.7.3
	go.mongodb.org/mongo-driver v1.7.2
)
