package main

import (
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"net/url"
	"testing"

	"github.com/elazarl/goproxy"
)

func TestMain(t *testing.T) {
	testHtml := "test"
	// target server
	srv := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		_, _ = w.Write([]byte(testHtml))
	}))
	defer srv.Close()

	// proxy server
	proxy := goproxy.NewProxyHttpServer()
	proxy.OnRequest().DoFunc(reqHandler)
	proxySrv := httptest.NewServer(proxy)
	defer proxySrv.Close()

	http.DefaultClient.Transport = &http.Transport{
		Proxy: func(req *http.Request) (*url.URL, error) {
			return url.Parse(proxySrv.URL)
		},
	}

	req, _ := http.NewRequest(http.MethodGet, srv.URL, nil)
	resp, _ := http.DefaultClient.Do(req)

	body, _ := ioutil.ReadAll(resp.Body)
	defer resp.Body.Close()

	if testHtml != string(body) {
		t.Fatalf("response body is wrong\n expect: %s\n actual %s", testHtml, body)
	}
}
