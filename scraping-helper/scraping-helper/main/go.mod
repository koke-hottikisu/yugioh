module main

go 1.14

require (
	github.com/dvsekhvalnov/jose2go v1.5.0
	github.com/elazarl/goproxy v0.0.0-20210801061803-8e322dfb79c4
	github.com/gin-gonic/gin v1.7.3
	github.com/hashicorp/go-retryablehttp v0.7.0
	github.com/tebeka/selenium v0.9.9
)
