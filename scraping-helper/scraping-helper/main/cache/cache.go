package cache

import (
	"io"
	"io/ioutil"
	"os"

	"github.com/dvsekhvalnov/jose2go/base64url"
)

func cachePath(cacheDir string, planeUrl string) string {
	encodedUrl := base64url.Encode([]byte(planeUrl))
	if _, err := os.Stat(cacheDir); os.IsNotExist(err) {
		os.Mkdir(cacheDir, 666)
	}
	return cacheDir + "/" + encodedUrl
}

func ReadCache(cacheDir string, planeUrl string) (hit bool, data *os.File) {
	if file, err := os.Open(cachePath(cacheDir, planeUrl)); err == nil {
		return true, file
	}
	return false, nil
}

func WriteCache(cacheDir string, planeUrl string, data io.Reader) error {
	byteData, err := ioutil.ReadAll(data)
	if err != nil {
		return err
	}
	return ioutil.WriteFile(cachePath(cacheDir, planeUrl), byteData, os.ModePerm)
}
