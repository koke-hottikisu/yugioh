package cache

import (
	"bytes"
	"io/ioutil"
	"os"
	"strings"
	"testing"

	"github.com/dvsekhvalnov/jose2go/base64url"
)

func TestAll(t *testing.T) {
	// setup test
	pwd, err := os.Getwd()
	if err != nil {
		t.Fatal(err)
	}
	testCacheDir := pwd + "/cache_test"
	testUrl := "http://a.b.c/?a=b&c=d#e"
	testCachePath := testCacheDir + "/" + base64url.Encode([]byte(testUrl))
	testData := "testdata"
	os.Remove(testCachePath)

	// write cache
	err = WriteCache(testCacheDir, testUrl, strings.NewReader(testData))
	if err != nil {
		t.Fatalf("can not write to cache\n%s", err)
	}

	// read cache
	hit, data := ReadCache(testCacheDir, testUrl)
	if !hit {
		t.Fatal("can not read form cache")
	}
	dataString, err := ioutil.ReadAll(data)
	if err != nil {
		t.Fatalf("can not read body\n%s", err)
	}
	if !bytes.Equal(dataString, []byte(testData)) {
		t.Fatalf("data is broken\n  expect: %s\n  actual: %s", testData, dataString)
	}

	// teardown
	os.Remove(testCachePath)
}
