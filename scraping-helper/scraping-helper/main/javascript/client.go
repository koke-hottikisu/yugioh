package javascript

import (
	"net/http"

	"github.com/dvsekhvalnov/jose2go/base64url"
	"github.com/hashicorp/go-retryablehttp"
)

func Get(url string) (*http.Response, error) {
	encodedUrl := "http://localhost/?q=" + base64url.Encode([]byte(url))
	return retryablehttp.Get(encodedUrl)
}
