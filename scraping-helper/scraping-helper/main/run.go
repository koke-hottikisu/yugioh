package main

import (
	"log"
	"main/cache"
	"main/javascript"
	"net/http"

	"github.com/elazarl/goproxy"
)

const (
	cacheDir = "/cacheDir"
)

func main() {
	proxy := goproxy.NewProxyHttpServer()
	proxy.Verbose = true

	proxy.OnRequest().DoFunc(reqHandler)

	log.Fatal(http.ListenAndServe(":8080", proxy))
}

func reqHandler(r *http.Request, ctx *goproxy.ProxyCtx) (*http.Request, *http.Response) {
	enableCache := r.Header.Get("SH-Cache-Enable") == "true"
	enableJavascript := r.Header.Get("SH-Javascript-Enable") == "true"

	if enableCache {
		resp, err := useCache(cacheDir, r.URL.String(), switchGet(r.URL.String(), enableJavascript))
		if err != nil {
			return r, nil
		}
		return r, resp
	} else {
		resp, err := switchGet(r.URL.String(), enableJavascript)(r.URL.String())
		if err != nil {
			return r, nil
		}
		return r, resp
	}
}


func useCache(caheDir string,  url string, get func(url string) (*http.Response, error)) (*http.Response, error) {
	if hit, data := cache.ReadCache(cacheDir, url); hit {
		return &http.Response{Body: data}, nil
	} else {
		resp, err := get(url)
		if err != nil {
			return nil, err
		}
		cache.WriteCache(cacheDir, url, resp.Body)
		return resp, nil
	}
}

func switchGet(url string, jsEnabled bool) (func (url string) (*http.Response, error)) {
	if jsEnabled {
		return javascript.Get
	} else {
		return http.Get
	}
}
