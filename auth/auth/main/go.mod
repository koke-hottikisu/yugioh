module main

go 1.14

require (
	github.com/dghubble/oauth1 v0.7.0
	github.com/gin-contrib/sessions v0.0.3
	github.com/gin-gonic/gin v1.7.3
)
