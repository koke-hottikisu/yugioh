package user

import (
	"crypto/rand"
	"time"
)

// loginData[userKey]
var loginData = map[string]LoginData{}

type LoginData struct {
	Id string
	LoginDate time.Time
	LastUsedDate time.Time
}

func (l *LoginData) isExpired() bool {
	return time.Now().Before(l.LastUsedDate.Add(time.Hour))
}

func Regsit(userId string) (key string, err error) {
	key, err = makeRandomStr(64)
	if err != nil {
		return
	}
	loginData[key] = LoginData{
		Id:           userId,
		LoginDate:    time.Now(),
		LastUsedDate: time.Now(),
	}
	return
}

func UnRegist(userKey string) {
	delete(loginData, userKey)
}

func GetUserId(key string) (userId string, exists bool) {
	data, exists := loginData[key]
	if !exists {
		return "", false
	}

	loginData[key] = LoginData{
		Id:           data.Id,
		LoginDate:    data.LoginDate,
		LastUsedDate: time.Now(),
	}

	return data.Id, true
}

func KeepGC() {
	for {
		for key, value := range loginData {
			if value.isExpired() {
				delete(loginData, key)
			}
		}

		time.Sleep(time.Hour)
	}
}

// https://qiita.com/nakaryooo/items/7d269525a288c4b3ecda
func makeRandomStr(digit uint32) (string, error) {
    const letters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"

    b := make([]byte, digit)
    if _, err := rand.Read(b); err != nil {
        return "", err
    }

    var result string
    for _, v := range b {
        result += string(letters[int(v)%len(letters)])
    }
    return result, nil
}
