package main

import (
	"log"
	"main/twitter"
	"main/user"
	"net/http"

	"github.com/gin-contrib/sessions"
	"github.com/gin-contrib/sessions/cookie"
	"github.com/gin-gonic/gin"
)

func main() {
	r := setupRouter()
	r.Run(":80")
}

func setupRouter() *gin.Engine {
	r := gin.Default()
	store := cookie.NewStore([]byte("secret"))
	store.Options(sessions.Options{
		Path:     "/",
		Domain:   "slimemoss.com",
		MaxAge:   3600,
		Secure:   false,
		HttpOnly: false,
		SameSite: 0,
	})
	r.Use(sessions.Sessions("mysession", store))

	r.GET("/twitter_login", twitterLogin)
	r.GET("/twitter_callback", twitterCallback)

	// logout
	r.GET("/logout", logout)

	// frontendがセッション内のuserKeyを得る
	r.GET("/get_userKey", getUserKeyFromSession)

  // /getUser?userKey=***
  // apiサーバがuserKeyからuserIdを得る
	r.GET("/get_userId", getUser)

	// /userInfo?userId=***
	r.GET("/get_userInfo", getUserInfo)
	return r
}

// https://developer.twitter.com/en/docs/authentication/guides/log-in-with-twitter
func twitterLogin(c *gin.Context) {
	session := sessions.Default(c)
	session.Set("loginReferer", c.Request.Referer())

	// check if already login

	requestToken, requestSecret, authorizationURL, err := twitter.Login()
	if err != nil {
		c.String(http.StatusInternalServerError, err.Error())
		log.Println(err)
		return
	}

	session.Set("requestToken", requestToken)
	session.Set("requestSecret", requestSecret)

	err = session.Save()
	if err != nil {
		c.String(http.StatusInternalServerError, err.Error())
		log.Println(err)
		return
	}

	c.Redirect(http.StatusFound, authorizationURL.String())
}

func twitterCallback(c *gin.Context) {
	session := sessions.Default(c)

	requestSecret := session.Get("requestSecret")
	loginReferer := session.Get("loginReferer")
	if requestSecret == nil {
		c.String(http.StatusInternalServerError, "Session has no value: requestSecret")
		log.Println("Session has no value: requestSecret")
		return
	}
	if loginReferer == nil {
		c.String(http.StatusInternalServerError, "Session has no value: loginReferer")
		log.Println("Session has no value: loginReferer")
		return
	}

	token, err := twitter.Callback(c.Request, requestSecret.(string))
	if err != nil {
		c.String(http.StatusInternalServerError, err.Error())
		log.Println(err)
		return
	}

	userId, err := twitter.UserId(token)
	if err != nil {
		c.String(http.StatusInternalServerError, err.Error())
		log.Println(err)
		return
	}

	key, err := user.Regsit(userId)
	if err != nil {
		c.String(http.StatusInternalServerError, err.Error())
		log.Println(err)
		return
	}

	session.Set("userKey", key)
	err = session.Save()
	if err != nil {
		c.String(http.StatusInternalServerError, err.Error())
		log.Println(err)
		return
	}

	c.Redirect(301, loginReferer.(string))
}

func logout(c *gin.Context) {
	session := sessions.Default(c)
	userKey := session.Get("userKey")
	if userKey == nil {
		c.String(http.StatusUnauthorized, "")
		return
	}
	user.UnRegist(userKey.(string))
	session.Delete("userKey")

	c.String(http.StatusOK, "")
	return
}

type getUserKeyFromSessionResponse struct {
	Exists bool `json:"exists"`
	UserKey string `json:"userKey"`
	UserId string `json:"userId"`
}
func getUserKeyFromSession(c *gin.Context) {
	notExistResp := getUserKeyFromSessionResponse{
		Exists:  false,
		UserKey: "",
		UserId:  "",
	}

	session := sessions.Default(c)
	userKey := session.Get("userKey")
	if userKey == nil {
		c.JSON(http.StatusOK, notExistResp)
		return
	}

	userKeyString := userKey.(string)
	userId, ok := user.GetUserId(userKeyString)
	if !ok {
		session.Clear()
		c.JSON(http.StatusOK, notExistResp)
	}
	c.JSON(http.StatusOK, getUserKeyFromSessionResponse{
		Exists:  true,
		UserKey: userKeyString,
		UserId:  userId,
	})
	return
}

func getUser(c *gin.Context) {
	userKey := c.Request.URL.Query().Get("userKey")
	userId, exists := user.GetUserId(userKey)

	c.JSON(http.StatusOK, gin.H{"userId": userId, "exists": exists})
}

func getUserInfo(c *gin.Context) {
	userId := c.Query("userId")
	if userId == "" {
		c.String(http.StatusBadRequest, "URL Query %s is required", "userId")
		return
	}

	data, contentType, err := twitter.GetUserInfo(userId, c.Query("tweet.fields"), c.Query("user.fields"))
	if err != nil {
		c.String(http.StatusInternalServerError, "%s", err)
		return
	}

	c.Data(http.StatusOK, contentType, data)
}
