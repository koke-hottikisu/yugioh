package twitter

import (
	"net/http"
	"net/url"

	"github.com/dghubble/oauth1"
)

func Login() (requestToken string, requestSecret string, authorizationURL *url.URL, err error) {
	requestToken, requestSecret , err = TwitterOauthConfig.RequestToken()
	if err != nil {
		return
	}

	authorizationURL, err = TwitterOauthConfig.AuthorizationURL(requestToken)
	if err != nil {
		return
	}

	return
}

func Callback(req *http.Request, requestSecret string) (token *oauth1.Token, err error) {
	requestToken, verifier, err := oauth1.ParseAuthorizationCallback(req)
	if err != nil {
		return
	}

	accessToken, accessSecret, err := TwitterOauthConfig.AccessToken(requestToken, requestSecret, verifier)
	if err != nil {
		return
	}

	token = oauth1.NewToken(accessToken, accessSecret)
	return
}
