package twitter

import (
	"os"

	"github.com/dghubble/oauth1"
	"github.com/dghubble/oauth1/twitter"
)

var (
	TwitterOauthConfig = oauth1.Config{
		ConsumerKey:    os.Getenv("TWITTER_API_KEY"),
		ConsumerSecret: os.Getenv("TWITTER_API_KEY_SECRET"),
		CallbackURL:    os.Getenv("TWITTER_API_CALLBACK"),
		Endpoint:       twitter.AuthenticateEndpoint,
	}
)
