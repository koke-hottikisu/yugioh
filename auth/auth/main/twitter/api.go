package twitter

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"strconv"

	"github.com/dghubble/oauth1"
)

type VeryfyCredential struct {
	Id int `json:"id"`
}

//どのユーザが認証しているかを返す
func UserId(token *oauth1.Token) (userId string, err error) {
	httpClient := TwitterOauthConfig.Client(oauth1.NoContext, token)

	resp, err := httpClient.Get("https://api.twitter.com/1.1/account/verify_credentials.json")
	if err != nil {
		return
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return
	}

	var vc VeryfyCredential
	err = json.Unmarshal(body, &vc)
	if err != nil {
		return
	}

	userId = strconv.Itoa(vc.Id)
	return
}

// return https://api.twitter.com/2/users/:id
// see https://developer.twitter.com/en/docs/twitter-api/users/lookup/api-reference/get-users-id#tab1
func GetUserInfo(userId string, tweetFields string, userFields string) (
	data []byte, contentType string, err error) {

	config := oauth1.NewConfig(os.Getenv("TWITTER_API_KEY"), os.Getenv("TWITTER_API_KEY_SECRET"))
	token := oauth1.NewToken(os.Getenv("TWITTER_API_ACCESS_TOKEN"), os.Getenv("TWITTER_API_ACCESS_TOKEN_SECRET"))
	httpClient := config.Client(oauth1.NoContext, token)

	url := fmt.Sprintf("https://api.twitter.com/2/users/%s?tweet.fields=%s&user.fields=%s",
		userId, tweetFields, userFields)

	resp, err := httpClient.Get(url)
	if err != nil {
		return nil, "", err
	}
	if resp.StatusCode != http.StatusOK {
		return nil, "", fmt.Errorf("http response code is not %d: %d  (%s)", http.StatusOK, resp.StatusCode, url)
	}

	data, err = ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, "", err
	}
	resp.Body.Close()

	return data, "application/json", nil
}
