const webpack = require('webpack');
var path = require('path');

module.exports = {
    entry: {
        index: './src/index.tsx',
    },
    output: {
        path: path.join(__dirname, 'dist'),
        filename: '[name].js',
    },
    module: {
        rules: [
            {
                test: /\.jsx?$/,
                use: ['babel-loader'],
                exclude: /node_modules/,
            },
            {
                test: /\.tsx?$/,
                use: ['ts-loader'],
                exclude: /node_modules/,
            },
            {
                test: /\.css?$/,
                use: ['style-loader', 'css-loader'],
            },
            {
              test: /\.(csv)$/i,
              use: [
                {
                  loader: 'raw-loader'
                }
              ],
            },            {
              test: /\.(png|jpe?g|gif)$/i,
              use: [
                {
                  loader: 'file-loader',
                  options: {
                    name: '[path][name].[ext]'
                  }
                }
              ],
            }
        ]
    },
    resolve: {
        extensions: ['.js', '.jsx', '.ts', '.tsx', '.json'],
        modules: ['node_modules']
    },
    mode: 'development',
    devServer: {
        port: 8080,
        host: '0.0.0.0',
        historyApiFallback: true,
        static: path.join(__dirname, 'dist'),
        hot: true,
        liveReload: true,
        proxy: {
            '/api/auth': {
	              target: 'http://auth',
                changeOrigin: true,
                pathRewrite: {'^/api/auth': ''}
            },
            '/api/carddb-app': {
	              target: 'http://carddb-app',
                changeOrigin: true,
                pathRewrite: {'^/api/carddb-app': ''}
            },
            '/api/cardname-complete': {
	              target: 'http://cardname-complete',
                changeOrigin: true,
                pathRewrite: {'^/api/cardname-complete': ''}
            },
            '/api/links-deck-parse': {
	              target: 'http://links-deck-parse',
                changeOrigin: true,
                pathRewrite: {'^/api/links-deck-parse': ''}
            },
            '/api/deckrule': {
	              target: 'http://deckrule',
                changeOrigin: true,
                pathRewrite: {'^/api/deckrule': ''}
            },
            '/api/draft': {
	              target: 'http://draft',
                changeOrigin: true,
                pathRewrite: {'^/api/draft': ''}
            }
        }
    },
    watchOptions: {
        aggregateTimeout: 200,
        poll: 1000,
        ignored: '**/node_modules'
    },
    performance: {
        maxEntrypointSize: 1000000,
        maxAssetSize: 1000000
    }
};
