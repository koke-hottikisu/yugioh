import * as React from 'react'
import { Button, Row, Container, Col, Alert } from 'react-bootstrap'

export const TopPage = () => {
  return (
    <>
      <Container>
        <Row style={{ maxWidth: "45rem", marginLeft: 'auto', marginRight: 'auto', marginTop: '8rem' }}>
          <Row>
            <Col>
              <h1 style={{fontSize: "3rem", fontFamily: "Hannotate SC", fontWeight: "bold"}}>遊戯王 ブースター・ドラフト</h1>
            </Col>
          </Row>
          <Row>
            <Col>
              <Button href="/cardpool/top" className="m-2 p-2" variant="outline-light" style={{minWidth: "10rem"}}>対CPUプレイ</Button>
              <Button href="/cardpool/top" className="m-2 p-2" variant="outline-light" style={{minWidth: "10rem"}}>対人ドラフト作成</Button>
            </Col>
          </Row>
        </Row>
        <Row>
          <Col sm="4">
            <div className="p-2">
              <h3>とりあえず試す</h3>
              <p>対CPUプレイをどうぞ！</p>
              <p>使い勝手がわかったら、twitter等で対人ドラフトの募集を探すか、自分で作成して募集しましょう。</p>
            </div>
          </Col>
          <Col sm="4">
            <div className="p-2">
              <h3>ブースター・ドラフトとは</h3>
              <p>ブースター・ドラフトとは、パックを開封してその場でデッキを組んで対戦する遊びです。</p>
              <p>
                1〜3を繰り返してカードを集め、デッキを組みます。<br/>
                1. プレーヤー全員にパックが配られる<br/>
                2. パックからカードを１枚ピックして、隣に回す<br/>
                3. パックが空になるまで繰り返す
              </p>
            </div>
          </Col>
          <Col sm="4">
            <div className="p-2">
              <h3>カードプール</h3>
              <p>使用するカードとパック封入率は自由に設定できます。</p>
              <p>MTGでは、スーパーレア(80種・1枚封入) レア(80種・3枚封入) ノーマル(80種・11枚封入)の割合でカードプールが作られています。</p>
              <p>
                <a href="/cardpool/top">カードプール一覧</a>
              </p>
            </div>
          </Col>
        </Row>
      </Container>
    </>
  )
}
