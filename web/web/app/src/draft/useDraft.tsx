import * as React from 'react'
import { useCookies } from 'react-cookie'
import useInterval from 'use-interval'
import { DraftStateRes, DraftStateResInit, useJsonrpcDraftState } from './jsonrpc/useJsonrpcDraftState'

export interface Draft {
  exists: boolean,
  draft: {
    draftID: string,
    playerID: string,
    state: DraftStateRes,
  }
}

export interface DraftHooks {
  setDraft: (draftID: string, playerID: string) => void
  exitDraft: () => void
}

export const useDraft = (): [Draft, DraftHooks] => {
  const [respDraftState, reqDraftState] = useJsonrpcDraftState()
  const [exists, setExists] = React.useState(false)
  const [cookies, setCookie, removeCookie] = useCookies(["playerid", "draftid"])

  useInterval(() => {
    if(cookies['draftid'] == undefined || cookies['draftid'] == "undefined") {
      return
    }
    if(exists) {
      if(respDraftState.data.IsEnd) {
        return
      }
    }
    reqDraftState({
      DraftID: cookies['draftid'],
      PlayerID: cookies['playerid'],
    })
  }, 1000)

  React.useEffect(() => {
    if(respDraftState.error && !respDraftState.response.error){
      setExists(false)
    }
    if(respDraftState.data !== undefined && !respDraftState.error && !respDraftState.loading){
      setExists(true)
    }
  }, [respDraftState])

  const setDraft = (draftID: string, playerID: string) => {
    setCookie("draftid", draftID, {sameSite: 'strict'})
    setCookie("playerid", playerID, {sameSite: 'strict'})
  }
  const exitDraft = () => {
    removeCookie("playerid", {sameSite: 'strict'})
    removeCookie("draftid", {sameSite: 'strict'})
    if(window.location.pathname != "/draft/top"){
      window.location.href = "/draft/top"
    }
  }

  return [{
    exists: exists,
    draft: {
      draftID: cookies['draftid'],
      playerID: cookies['playerid'],
      state: respDraftState.data === undefined ? DraftStateResInit : respDraftState.data
    }
  }, {setDraft, exitDraft}]
}
