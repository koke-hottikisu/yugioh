import * as React from 'react'
import { useLocation } from 'react-router-dom';
import { Alert, Button, Form, Toast } from 'react-bootstrap'
import { useJsonrpcDraftJoin } from './jsonrpc/useJsonrpcDraftJoin'
import { DraftContext } from '.'

export const JoinPage = () => {
  const query = new URLSearchParams(useLocation().search)
  const [_, draftHooks] = React.useContext(DraftContext)
  const [playerName, setPlayerName] = React.useState<string>("")
  const [__, doReq] = useJsonrpcDraftJoin()

  const [showCannotJoin, setShowCannotJoin] = React.useState<boolean>(false)

  return (
    <>
      <Alert variant="danger">
        開始後は、終了するまで退出できません
      </Alert>

      <Form>
        <Form.Group className="mb-3">
          <Form.Control type="text" required
                        placeholder="プレーヤー名  ex)アテム"
                        value={playerName}
                        onChange={(e) => {setPlayerName(e.target.value)} } />
          <Button variant="primary"
                  onClick={() => {
                    if(playerName != "") {
                      doReq({PlayerName: playerName, DraftID: query.get('draftid')})
                        .then((resp) => {
                          draftHooks.setDraft(resp.data.result.DraftID, resp.data.result.PlayerID)
                        })
                        .catch(() => {
                          setShowCannotJoin(true)
                        })
                    }
                  }}>
            参加
          </Button>
        </Form.Group>
      </Form>

      <Toast show={showCannotJoin} onClose={() => {setShowCannotJoin(false)}}>
        <Toast.Header>
          hi
        </Toast.Header>
        <Toast.Body>ドラフトが存在しないか、すでに開始されているので、参加できませんでした</Toast.Body>
      </Toast>
    </>
)
}
