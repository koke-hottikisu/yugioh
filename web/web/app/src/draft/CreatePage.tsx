import * as React from 'react'
import { Button, Form, Row, Col } from 'react-bootstrap'
import { useLocation } from 'react-router-dom'

import { DraftContext } from './index'
import { useJsonrpcDraftCreate } from './jsonrpc/useJsonrpcDraftCreate'
import { DraftSetForm } from '../draft_cardpool/draftset/DraftSetForm'
import { useDraftSet } from '../draft_cardpool/draftset/useDraftSet'

export const CreatePage = () => {
  const query = new URLSearchParams(useLocation().search)

  const [_, draftHooks] = React.useContext(DraftContext)
  const [__, doReq] = useJsonrpcDraftCreate()

  const [playerName, setPlayerName] = React.useState<string>('')
  const [rotation, setRotation] = React.useState<number>(3)
  const [cardNumInPack, setCardNumInPack] = React.useState<number>(15)
  const [draftSet, draftSetHooks] = useDraftSet([])

  React.useEffect(() => {
    const cardpoolid = query.get('cardpoolid')
    if (cardpoolid) {
      draftSetHooks.load(cardpoolid)
    }
  }, [])

  const start = () => {
    doReq({
      Card: draftSet, PlayerName: playerName, Rotation: rotation, CardNumInPack: cardNumInPack
    }).then((resp) => {
      if(!resp.data.error) {
        draftHooks.setDraft(resp.data.result.DraftID, resp.data.result.PlayerID)
      }
    })
  }

  return (
    <>
      <Form>
        <Row className="p-5" style={{ maxWidth: "45rem", marginLeft: 'auto', marginRight: 'auto' }}>
          <Button variant="primary"
                  onClick={start}>
            対人ドラフト作成
          </Button>
        </Row>

        <Row>
          <Col sm="2">
            <Form.Label>パックのカード枚数</Form.Label>
            <Form.Control type="number" value={cardNumInPack} required
                          onChange={(e) => {setCardNumInPack(Number(e.target.value))}}/>
          </Col>
          <Col sm="2">
            <Form.Label>ローテーション回数</Form.Label>
            <Form.Control type="number" value={rotation} required
                          onChange={(e) => {setRotation(Number(e.target.value))}}/>
          </Col>
          <Col sm="2">
            <div>プレーヤーピック枚数</div>
            <div>{rotation * cardNumInPack}枚</div>
          </Col>
        </Row>
        <Form.Label>プレーヤー名</Form.Label>
        <Form.Control type="text" placeholder="アテム" required
                      onChange={(e) => {setPlayerName(e.target.value)} } />
        <DraftSetForm draftSet={draftSet} draftSetHooks={draftSetHooks} />
      </Form>
    </>
  )
}
