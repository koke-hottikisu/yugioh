const AttList = [
  "闇属性",
  "光属性",
  "地属性",
  "水属性",
  "炎属性",
  "風属性",
  "神属性",
  "通常魔法",
  "速攻魔法",
  "フィールド魔法",
  "装備魔法",
  "永続魔法",
  "儀式魔法",
  "通常罠",
  "カウンター罠",
  "永続罠",
]

export const CompareAttribute = (a: string, b: string): number => {
  return AttList.indexOf(a) > AttList.indexOf(b) ? 1 : -1
}
