import * as React from 'react'

import { YugiohCardDefault } from '../../../card_info/useYugiohCard'
import { PickedCard, PickedCardDefault } from './card/useCard'

export type DispatchPickedCard = React.Dispatch<((prev: PickedCard) => PickedCard)>
export type DispatchPickedCards = React.Dispatch<((prev: PickedCard[]) => PickedCard[])>

interface Hooks {
  setPickedCards: DispatchPickedCards
  getPickedCardDispatch: (id: number) => DispatchPickedCard
}

export const usePickedCards = (names: string[]): [PickedCard[], Hooks] => {
  const [pickedCards, setPickedCards] = React.useState<PickedCard[]>([])

  React.useEffect(() => {
    if (!names) { return }
    setPickedCards(prev => {
      const next = names.map((name, index) => {
        if (prev.length > index) {
          return {
            ...prev[index],
            latestPick: false
          }
        } else {
          return {
            ...PickedCardDefault,
            id: index,
            info: {
              ...YugiohCardDefault,
              name: name,
            },
            latestPick: true
          }
        }
      })
      return next
    })
  }, [JSON.stringify(names)])

  const getPickedCardDispatch = (id: number) => {
    return (action: (prev: PickedCard) => PickedCard) => {
      setPickedCards((prevCards: PickedCard[]) => {
        const next = [...prevCards]
        const index = next.findIndex(c => c.id == id)
        next[index] = action(prevCards[index])
        return next
      })
    }
  }

  return [pickedCards, {setPickedCards, getPickedCardDispatch}]
}
