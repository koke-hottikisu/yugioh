import * as React from 'react'

import { usePickedCards } from './usePickedCards'
import { CardDom, CardDomPlane, FocusedCardDom } from './card/'
import { CompareAttribute } from './attributeOrder'
import { YugiohCardDefault } from '../../../card_info/useYugiohCard'
import { Col, Row } from 'react-bootstrap'
import { PickedCard, PickedCardDefault } from './card/useCard'

interface Props {
  names: string[]
}

export const PickedCardsDom = (props: Props) => {
  const [pickedCards, pickedCardsHooks] = usePickedCards(props.names)
  const [selected, setSelected] = React.useState<PickedCard>(PickedCardDefault)

  React.useEffect(() => {
    // wait fetch cardinfo
    if (pickedCards.length == 0) { return }
    for(let i = 0; i < pickedCards.length; i++) {
      if (pickedCards[i].info.url == YugiohCardDefault.url) { return }
    }

    pickedCardsHooks.setPickedCards(prev => {
      const next = [...prev]
      next.sort((a, b) => a.info.name > b.info.name ? 1 : -1)
      next.sort((a, b) => a.info.level - b.info.level)
      next.sort((a, b) => CompareAttribute(a.info.attribute, b.info.attribute))
      return next
    })
  }, [JSON.stringify(pickedCards)])

  return (
    <>
      <Row>
        <Col sm="10" xs="12">
          <div>
            <div>メイン {pickedCards.concat().filter(c => !c.isSide).length}枚</div>
            <div style={{display: 'flex', flexWrap: 'wrap'}}>
              {pickedCards.map((card) => (
                <div key={card.id} hidden={card.isSide}
                     onClick={() => {
                       setSelected(card)
                     }}>
                  <CardDom card={card} setCard={pickedCardsHooks.getPickedCardDispatch(card.id)} />
                </div>
              ))}
            </div>
          </div>

          <div className="mt-3">
            <div>サイド</div>
            <div style={{display: 'flex', flexWrap: 'wrap'}}>
              {pickedCards.map((card) => (
                <div key={card.id} hidden={!card.isSide}
                     onClick={() => {
                       setSelected(card)
                     }}>
                  <CardDom card={card} setCard={pickedCardsHooks.getPickedCardDispatch(card.id)} />
                </div>
              ))}
            </div>
          </div>
        </Col>

        <Col sm={2} className="p-0 d-none d-sm-block">
          <FocusedCardDom card={selected} />
        </Col>
        
      </Row>
    </>
  )
}

export const PickedCardsDomSortByTime = (props: Props) => {
  const [pickedCards, pickedCardsHooks] = usePickedCards(props.names)

  return (
    <>
      <div>
        <div style={{display: 'flex', flexWrap: 'wrap'}}>
          {pickedCards.map((card, index) => (
            <>
              <div key={card.id}>
                <CardDomPlane card={card} setCard={pickedCardsHooks.getPickedCardDispatch(card.id)} />
              </div>
              <div key={"jun" + index} style={{ width: '100rem', float: 'left'}}
                    hidden={index % 15 != 14}>
              </div>
            </>
          ))}
        </div>
      </div>
    </>
  )
}
