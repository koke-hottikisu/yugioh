import * as React from 'react'
import { Card } from 'react-bootstrap'
import { BsBoxArrowRight } from 'react-icons/bs'

import { DispatchPickedCard } from '../usePickedCards'
import { usePickedCard, PickedCard, PickedCardDefault } from './useCard'

interface Props {
  card: PickedCard
  setCard: DispatchPickedCard
}

export const CardDom = (props: Props) => {
  const {card, setCard} = {...props}
  const [pickedCard, pickedCardHooks] = usePickedCard(card, setCard)
  const [focus, setFocus] = React.useState(card.latestPick)

  React.useEffect(() => {
    new Promise( resolve => setTimeout(resolve, 3000) )
      .then(() => {
        setFocus(false)
      })
  }, [])

  return (
        <Card className="p-0" style={{width: '6rem', fontSize: '0.75rem'}}
              bg={focus ? "info" : ""}>
          <Card.Header className="p-0"  onClick={() => {
            pickedCardHooks.setSide(!pickedCard.isSide)
          }} >
            <div style={{overflow: 'hidden', textOverflow: 'ellipsis', whiteSpace: 'nowrap'}}>
              {pickedCard.info.name}
            </div>
            <div>
              <BsBoxArrowRight/>
            </div>
          </Card.Header>
          <Card.Img src={"https://" + pickedCard.info.imageurl[0]} alt={pickedCard.info.name} />
        </Card>
  )
}

export const CardDomPlane = (props: Props) => {
  const {card, setCard} = {...props}
  const [pickedCard, _] = usePickedCard(card, setCard)

  return (
    <div>
      <Card className="p-0" style={{width: '6rem', fontSize: '0.75rem'}}>
        <Card.Header className="p-0">
          <div style={{overflow: 'hidden', textOverflow: 'ellipsis', whiteSpace: 'nowrap'}}>
            {pickedCard.info.name}
          </div>
        </Card.Header>
        <Card.Img src={"https://" + pickedCard.info.imageurl[0]} alt={pickedCard.info.name} />
      </Card>
    </div>
  )
}

export const FocusedCardDom = (props: {card: PickedCard}) => {
  const {card} = {...props}

  return (
    <div>
      <Card className="p-0">
        <Card.Header className="p-0">
          <div>
            {card.info.name}
          </div>
        </Card.Header>
        <Card.Img src={"https://" + card.info.imageurl[0]} alt={card.info.name} />
        <Card.Body>
          {card.info.text}
        </Card.Body>
      </Card>
    </div>
  )
}
