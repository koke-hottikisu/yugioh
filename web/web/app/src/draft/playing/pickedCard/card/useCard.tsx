import * as React from 'react'
import useAxios from 'axios-hooks'

import { YugiohCard, YugiohCardDefault } from '../../../../card_info/useYugiohCard'
import { DispatchPickedCard } from '../usePickedCards'

export interface PickedCard {
  id: number
  info: YugiohCard
  isSide: boolean
  latestPick: boolean
}
export const PickedCardDefault: PickedCard = {
  id: -1,
  info: YugiohCardDefault,
  isSide: false,
  latestPick: false,
}

interface Hooks {
  setSide: (isSide: boolean) => void
}

export const usePickedCard = (card: PickedCard, setCard: DispatchPickedCard): [PickedCard, Hooks] => {
  const [_, doReq] = useAxios<YugiohCard>({
    url: "/api/carddb-app/card",
    method: "get",
  }, {
    manual: true
  })

  React.useEffect(() => {
    if (card.info.url == YugiohCardDefault.url) {
      doReq({params: {name: card.info.name}}).then((resp) => {
        setCard(prev => (
          {
            ...prev,
            info: resp.data,
          }
        ))
      })
    }
  }, [JSON.stringify(card)])

  const setSide = (isSide: boolean) => {
    setCard(prev => ({
      ...prev,
      isSide,
    }))
  }

  return [card, {setSide}]
}
