import * as React from 'react'
import useInterval from 'use-interval'
import { Card } from 'react-bootstrap'
import Draggable from 'react-draggable'

export const ZDraggable = (props) => {
  const [z, setZ] = React.useState<number>(0)
  const draggableRef = React.useRef(null);

  useInterval(() => {
    const pos = parseInt(draggableRef.current.getBoundingClientRect().top)
    if(pos == z) {
      setZ(pos + 1)
    } else {
      setZ(pos)
    }
  }, 300)

  function convertRemToPx(rem) {
    var fontSize = getComputedStyle(document.documentElement).fontSize;
    return rem * parseFloat(fontSize);
  }

  return (
    <Draggable grid={[convertRemToPx(5), convertRemToPx(2)]}>
      <div style={{ zIndex: z, position: 'relative', pointerEvents: 'all' }}
           ref={draggableRef}>
        {props.children}
      </div>
    </Draggable>
  )
}
