import * as React from 'react'
import { Row, Card, Badge, Col, Button, Container } from 'react-bootstrap'
import { RiSwapBoxFill, RiSwapBoxLine } from 'react-icons/ri'
import {HiOutlineChevronDoubleRight} from 'react-icons/hi'
import { DraftContext } from '../index'
export const AllPlayers = () => {
  const [draft, _] = React.useContext(DraftContext)

  return (
    <>
      <Row style={{justifyContent: 'center'}}>
        {draft.draft.state.AllPlayer.map((player, index) => (
          <Card bg="dark" className="p-1 m-2 border-1 border-light" key={index} style={{width: 'auto'}}>
            <Card.Body className="p-2">
              <Badge hidden={draft.draft.playerID != player.ID}>You</Badge>
              {player.Name}
              <br/>
              {Array.from(Array(player.OwnPackNum).keys()).map((e, index) => (
                <Badge key={index} bg="secondary" className="m-0 p-0"> </Badge>
              ))}
            </Card.Body>
          </Card>
        ))}
      </Row>
    </>
  )
}

export const ThreePlayers = () => {
  const [draft, _] = React.useContext(DraftContext)

  const prevPlayer = () => {
    const players = draft.draft.state.AllPlayer.concat()
    players.push(players[0])
    for (let i = 1; i < players.length; i++) {
      if(players[i].ID == draft.draft.playerID) {
        return players[i - 1]
      }
    }
  }

  const nextPlayer = () => {
    const players = draft.draft.state.AllPlayer.concat()
    players.push(players[0])
    for (let i = 0; i < players.length; i++) {
      if(players[i].ID == draft.draft.playerID) {
        return players[i + 1]
      }
    }
  }

  const me = () => {
    const players = draft.draft.state.AllPlayer.concat()
    for (let i = 0; i < players.length; i++) {
      if(players[i].ID == draft.draft.playerID) {
        return players[i]
      }
    }
  }

  interface PlayerCardProps {
    player: {
      Name: string,
      ID: string,
      OwnPackNum: number
    }
  }
  const PlayerCard = (props: PlayerCardProps) => {
    return (
      <Card bg="dark" className="m-1 p-2 border-1 border-light" style={{width: '6rem'}}>
        <Card.Body className="p-0">
          {props.player.Name}
          <br/>
          {Array.from(Array(props.player.OwnPackNum).keys()).map((e, index) => (
            <Badge key={index} bg="secondary"> </Badge>
          ))}
        </Card.Body>
      </Card>
    )
  }

  return (
    <Row style={{flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
      <PlayerCard player={prevPlayer()} />
      <div className="m-0 p-0" style={{ width: "1rem" }}>
        <HiOutlineChevronDoubleRight/>
      </div>
      <PlayerCard player={me()} />
      <div className="m-0 p-0" style={{ width: "1rem" }}>
        <HiOutlineChevronDoubleRight/>
      </div>
      <PlayerCard player={nextPlayer()} />
    </Row>
  )
  
}

export const PlayersInfo = () => {
  const [isAll, setIsAll] = React.useState<boolean>(false)

  return (
    <>
      <Row style={{justifyContent: 'center', alignItems: 'center' }}>
        <Col xs="11">
          <div hidden={!isAll} >
            <AllPlayers/>
          </div>
          <div hidden={isAll} >
            <ThreePlayers/>
          </div>
        </Col>
        <Col xs="1">
          <Button variant="outline-secondary"
                  onClick={() => {setIsAll(!isAll)}}>
            <div hidden={!isAll} >
              <RiSwapBoxFill/>
            </div>
            <div hidden={isAll} >
              <RiSwapBoxLine/>
            </div>
          </Button>
        </Col>
      </Row>
    </>
  )
}
