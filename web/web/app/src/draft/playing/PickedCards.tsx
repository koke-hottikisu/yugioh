import * as React from 'react'
import { Tabs, Tab, Button } from 'react-bootstrap'
import { DraftContext } from '../'
import { ForceEndButton } from '../ForceEndButton'
import { PickedCardsDom, PickedCardsDomSortByTime } from './pickedCard'

export const PickedCards = () => {
  const [draft, draftHooks] = React.useContext(DraftContext)

  return (
    <>
      <Tabs defaultActiveKey="free">
        <Tab eventKey="free" title="ピックカード整理">
          <PickedCardsDom names={draft.draft.state.Me.PickedCard} />
        </Tab>
        <Tab eventKey="timesort" title="ピック順">
          <PickedCardsDomSortByTime names={draft.draft.state.Me.PickedCard} />
        </Tab>
        <Tab eventKey="exit" title="退出">
          <div hidden={draft.draft.state.IsEnd}>
            <ForceEndButton/>
          </div>
          <Button variant="danger"
                  hidden={!draft.draft.state.IsEnd}
                  onClick={() => {
                    draftHooks.exitDraft()
                  }}>
            退出する(このページに戻れなくなります)
          </Button>
        </Tab>
      </Tabs>
    </>
  )
}
