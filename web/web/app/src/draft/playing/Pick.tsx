import * as React from 'react'
import { Button, Card, Col, ProgressBar, Row } from 'react-bootstrap'
import useInterval from 'use-interval'

import { DraftContext } from '../index'
import { useJsonrpcDraftPick } from '../jsonrpc/useJsonrpcDraftPick'
import { YugiohCardImage, YugiohCardLink, YugiohCardText } from '../../card_info'

export const Pick = () => {
  const [draft, _] = React.useContext(DraftContext)
  const [_1, doPick] = useJsonrpcDraftPick()

  const [selectedCard, setSelectedCard] = React.useState<number>(0)
  const [pickable, setPickable] = React.useState<boolean>(true)

  const state = draft.draft.state
  const pickDeadline = Math.floor((Date.parse(state.Me.PickDeadline) - Date.now()) / 1000)

  const pick = async () => {
    setPickable(false)
    await doPick({
      DraftID: draft.draft.draftID,
      PlayerID: draft.draft.playerID,
      Card: state.Me.PickPack[selectedCard]
    })
    setPickable(true)
  }

  const [prevPickPack, setPrevPickPack] = React.useState(state.Me.PickPack.length)
  React.useEffect(() => {
    const cur = state.Me.PickPack.length
    if (prevPickPack != cur) {
      setPrevPickPack(cur)
      setSelectedCard(0)
    }
  }, [state.Me.PickPack])

  useInterval(() => {
    if(pickDeadline <= 0) {
      pick()
    }
  }, 2000)

  return (
    <>
      <ProgressBar animated
                   now={100 - ((state.Me.PickPack.length * 6) - pickDeadline) / (state.Me.PickPack.length * 6) * 100}
                   label={`${pickDeadline} sec`}
                   className="bg-dark"/>

      <Row>
        <Col sm={10}>
          <Row>
            {state.Me.PickPack.map((pickableCard: string, index:number) => (
              <Col key={index} xs={3} sm={1} className="p-0 m-0">
                <Card onClick={() => {setSelectedCard(index)}}
                      border={index==selectedCard ? "info" : "dark"}
                      className="border-4 p-0">
                  <YugiohCardImage name={pickableCard} />
                </Card>
              </Col>
            ))}
          </Row>
          <Row>
            <Card bg="dark" className="border-1 border-light">
              <Card.Header className="bg-secondaly">
                <YugiohCardLink name={state.Me.PickPack[selectedCard]} />
              </Card.Header>
              <Card.Body>
                <YugiohCardText name={state.Me.PickPack[selectedCard]} />
              </Card.Body>
            </Card>
          </Row>
        </Col>
        <Col sm={2} className="p-0 d-none d-sm-block">
          <Card>
            <YugiohCardImage name={state.Me.PickPack[selectedCard]} />
          </Card>
        </Col>
      </Row>
      <Row>
        <Button variant="primary"
                disabled={!pickable}
                onClick={() => {
                  pick()
                }}>
          ピック
        </Button>
      </Row>
    </>
  )
}
