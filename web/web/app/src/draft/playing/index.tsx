import * as React from 'react'
import { Alert, Container, Row } from 'react-bootstrap'
import { DraftContext } from '..'
import { PlayersInfo } from './PlayersInfo'
import { Pick } from './Pick'
import { PickedCards } from './PickedCards'

export const PlayingDom = () => {
  const [draft, _] = React.useContext(DraftContext)

  return (
    <>
      <Container>
        <Row hidden={!draft.draft.state.IsEnd}>
          <Alert variant="info">
            ドラフトが終了しました。
          </Alert>
        </Row>
        <Row hidden={draft.draft.state.IsEnd}>
          <PlayersInfo/>
          <Pick/>
        </Row>
        <Row>
          <PickedCards/>
        </Row>
      </Container>
    </>
  )
}
