import * as React from 'react'
import { Button, Col, Row } from 'react-bootstrap'
import { useLocation } from 'react-router-dom'

import { DraftContext } from './index'
import { useJsonrpcDraftCreateSinglePlay } from './jsonrpc/useJsonrpcDraftCreateSinglePlay'
import { DraftSetForm } from '../draft_cardpool/draftset/DraftSetForm'
import { useDraftSet } from '../draft_cardpool/draftset/useDraftSet'

export const CreateSinglePlayPage = () => {
  const query = new URLSearchParams(useLocation().search)

  const [_, draftHooks] = React.useContext(DraftContext)
  const [__, doReq] = useJsonrpcDraftCreateSinglePlay()

  const [draftSet, draftSetHooks] = useDraftSet([])

  React.useEffect(() => {
    const cardpoolid = query.get('cardpoolid')
    if (cardpoolid) {
      draftSetHooks.load(cardpoolid)
    }
  }, [])

  const start = () => {
    doReq({Card: draftSet}).then((resp) => {
      if(!resp.data.error) {
        draftHooks.setDraft(resp.data.result.DraftID, resp.data.result.PlayerID)
      }
    })
  }

  return (
    <>
      <Row className="p-5" style={{ maxWidth: "45rem", marginLeft: 'auto', marginRight: 'auto' }}>
        <Button variant="primary"
                onClick={() => {start()}}>
          対CPUプレイ 開始
        </Button>
      </Row>

      <Row>
        <Col>
          <DraftSetForm draftSet={draftSet} draftSetHooks={draftSetHooks} />
        </Col>
      </Row>
    </>
  )
}
