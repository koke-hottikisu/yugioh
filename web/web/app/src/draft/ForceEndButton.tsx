import * as React from 'react'
import { Button, OverlayTrigger, Tooltip } from "react-bootstrap"
import { DraftContext } from '.'
import { useJsonrpcDraftForceEnd } from './jsonrpc/useJsonrpcDraftForceEnd'

const ForceEndButtonEnable = () => {
  const [draft, _] = React.useContext(DraftContext)
  const [__, doReq] = useJsonrpcDraftForceEnd()

  return (
    <Button variant="danger"
            onClick={() => {
              doReq({DraftID: draft.draft.draftID, PlayerID: draft.draft.playerID})
            }}>
      中止する</Button>
  )
}

const ForceEndButtonDisable = () => {
 return (
   <>
     <OverlayTrigger
       placement="right"
       overlay={<Tooltip id="tooltip-disabled">
         Ownerのみ実行できます。
       </Tooltip>}>
       <span className="d-inline-block">
         <Button variant="danger"
                 disabled>
           中止する</Button>
       </span>
     </OverlayTrigger>
   </>
 )
}

export const ForceEndButton = () => {
  const [draft, _] = React.useContext(DraftContext)

  return (
    <>
      {draft.draft.playerID == draft.draft.state.OwnerID?
       (<ForceEndButtonEnable/>)
      :
       (<ForceEndButtonDisable/>)
      }
    </>
  )
}
