import * as React from 'react'
import { Alert, Badge, Container, Row, Col, ListGroup, Overlay, Spinner, Tooltip } from "react-bootstrap"
import { HiOutlineClipboardCopy } from 'react-icons/hi'
import { CopyToClipboard } from 'react-copy-to-clipboard'
import { DraftContext } from '..'
import { ForceEndButton } from '../ForceEndButton'
import { StartDraftButton } from './StartDraftButton'
import { ExitDraftButton } from './ExitDraftButton'

export const WaitingDom = () => {
  const [draft, _] = React.useContext(DraftContext)
  const [showCopyed, setShowCopyed] = React.useState(false)
  const showCopyedTarget = React.useRef(null);

  React.useEffect(() => {
    const f = async () => {
      await new Promise(r => setTimeout(r, 1000));
      setShowCopyed(false)
    }
    f()
  }, [showCopyed])

  return (
    <>
      <Container className="mt-3">
        <Alert variant="info">
          <Row className="flex-row">
            <div style={{width: 'auto', marginTop: 'auto', marginBottom: 'auto' }}>
              <Spinner animation="border" role="status" />
            </div>
            <div style={{width: 'auto'}}>
              <div>
                開始を待っています
              </div>
              <div>
                参加リンク
                <CopyToClipboard text={window.location.host + "/draft/join?draftid=" + draft.draft.draftID}
                                 onCopy={() => {setShowCopyed(true)}}>
                  <span className="m-1" ref={showCopyedTarget}>
                    <HiOutlineClipboardCopy/>
                  </span>
                </CopyToClipboard>
                <Overlay target={showCopyedTarget.current} show={showCopyed} placement="right">
                  <Tooltip hidden={!showCopyed}>
                    コピーしました!
                  </Tooltip>
                </Overlay>
              </div>
            </div>
          </Row>
        </Alert>

        <ListGroup variant="flush">
          {draft.draft.state.AllPlayer.map((player, index) => (
            <ListGroup.Item key={index}>
              {player.Name}
              <Badge bg="info"
                     className="m-2"
                     hidden={index!=0}>
                owner</Badge>
            </ListGroup.Item>
          ))}
        </ListGroup>

        <StartDraftButton/>

        <div hidden={draft.draft.playerID == draft.draft.state.OwnerID}>
          <ExitDraftButton/>
        </div>

        <div hidden={draft.draft.playerID != draft.draft.state.OwnerID}>
          <ForceEndButton/>
        </div>
      </Container>
    </>
  )
}
