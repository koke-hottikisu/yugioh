import * as React from 'react'
import { Button, OverlayTrigger, Tooltip } from "react-bootstrap"
import { DraftContext } from '..'
import { useJsonrpcDraftStart } from '../jsonrpc/useJsonrpcDraftStart'

const StartDraftButtonEnable = () => {
  const [draft, _] = React.useContext(DraftContext)
  const [__, doReq] = useJsonrpcDraftStart()

  return (
    <Button onClick={() => {
      doReq({DraftID: draft.draft.draftID, PlayerID: draft.draft.playerID})
    }}>
      ドラフトを開始する
    </Button>
  )
}

const StartDraftButtonDisable = () => {
 return (
   <>
     <OverlayTrigger
       placement="right"
       overlay={<Tooltip id="tooltip-disabled">
         Ownerのみ実行できます。
       </Tooltip>}>
       <span className="d-inline-block">
         <Button disabled>
           ドラフトを開始する
         </Button>
       </span>
     </OverlayTrigger>
   </>
 )
}

export const StartDraftButton = () => {
  const [draft, _] = React.useContext(DraftContext)

  return (
    <>
      {draft.draft.playerID == draft.draft.state.OwnerID?
       (<StartDraftButtonEnable/>)
      :
       (<StartDraftButtonDisable/>)
      }
    </>
  )
}
