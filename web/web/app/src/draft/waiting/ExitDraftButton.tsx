import * as React from 'react'
import { Button } from "react-bootstrap"
import { DraftContext } from '..'
import { useJsonrpcDraftExit } from '../jsonrpc/useJsonrpcDraftExit'

export const ExitDraftButton = () => {
  const [draft, draftHooks] = React.useContext(DraftContext)
  const [__, doReq] = useJsonrpcDraftExit()

  return (
    <Button onClick={() => {
      doReq({DraftID: draft.draft.draftID, PlayerID: draft.draft.playerID}).then(() => {
        draftHooks.exitDraft()
      })
    }}>
      退出する
    </Button>
  )
}
