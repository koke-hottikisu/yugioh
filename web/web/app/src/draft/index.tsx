import * as React from 'react'

import { Route, Switch } from 'react-router-dom'

import { TopPage } from './TopPage'
import { CreatePage } from './CreatePage'
import { CreateSinglePlayPage } from './CreateSinglePlayPage'
import { PlayingDom } from './playing'
import { WaitingDom } from './waiting'
import { JoinPage } from './JoinPage'
import { useDraft, Draft, DraftHooks } from './useDraft'

import BackGround from './bg.png'

export const DraftContext = React.createContext<[Draft, DraftHooks]>(undefined)

export const DraftRoute = () => {
  const [draft, draftHooks] = useDraft()

  React.useEffect(() => {
    document.title = "ブースター・ドラフト | slimemoss"
  }, []);

  return (
    <DraftContext.Provider value={[draft, draftHooks]}>
      <div style={{backgroundImage: `url(${BackGround})`,
                   flex: "1 1 100%",
                   backgroundSize: "contain",
                   backgroundColor: "#1f1f1e",
                   backgroundRepeat: "no-repeat",
                   backgroundPosition: "center"}}>

        {
          draft.exists?
          <>
            <div hidden={draft.draft.state.IsStarted}>
              <WaitingDom />
            </div>
            <div hidden={!draft.draft.state.IsStarted}>
              <PlayingDom />
            </div>
          </>
          :
          <Switch>
            <Route path="/draft/create" component={CreatePage} />
            <Route path="/draft/join" component={JoinPage} />
            <Route path="/draft/singleplay" component={CreateSinglePlayPage} />
            <Route path="/draft/" component={TopPage} />
          </Switch>
        }

      </div>
    </DraftContext.Provider>
  )
}
