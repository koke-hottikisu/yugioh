import { useJsonrpc } from '../../useJsonrpc'

interface DraftExitArg {
  DraftID: string
  PlayerID: string
}

interface DraftExitRes {
}

export const useJsonrpcDraftExit = () => {
  return useJsonrpc<DraftExitArg, DraftExitRes>("/api/draft/rpc", "DraftRPC.Exit")
}
