import { useJsonrpc } from '../../useJsonrpc'

interface DraftJoinArg {
  DraftID: string
  PlayerName: string
}

interface DraftJoinRes {
  DraftID: string
  PlayerID: string
}

export const useJsonrpcDraftJoin = () => {
  return useJsonrpc<DraftJoinArg, DraftJoinRes>("/api/draft/rpc", "DraftRPC.Join")
}
