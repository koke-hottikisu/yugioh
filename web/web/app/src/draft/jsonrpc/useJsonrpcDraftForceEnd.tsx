import { useJsonrpc } from '../../useJsonrpc'

interface DraftForceEndArg {
  DraftID: string
  PlayerID: string
}

interface DraftForceEndRes {
}

export const useJsonrpcDraftForceEnd = () => {
  return useJsonrpc<DraftForceEndArg, DraftForceEndRes>("/api/draft/rpc", "DraftRPC.ForceEnd")
}
