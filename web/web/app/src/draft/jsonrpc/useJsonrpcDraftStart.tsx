import { useJsonrpc } from '../../useJsonrpc'

interface DraftStartArg {
  DraftID: string
  PlayerID: string
}

interface DraftStartRes {
}

export const useJsonrpcDraftStart = () => {
  return useJsonrpc<DraftStartArg, DraftStartRes>("/api/draft/rpc", "DraftRPC.Start")
}
