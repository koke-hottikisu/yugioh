import { useJsonrpc } from '../../useJsonrpc'

interface DraftPickArg {
  DraftID: string
  PlayerID: string
  Card: string
}

interface DraftPickRes {}

export const useJsonrpcDraftPick = () => {
  return useJsonrpc<DraftPickArg, DraftPickRes>("/api/draft/rpc", "DraftRPC.Pick")
}
