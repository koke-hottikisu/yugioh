import { useJsonrpc } from '../../useJsonrpc'

interface DraftCreateSinglePlayArg {
  Card: {
    Name: string
    EncloseNum: number
  }[]
}

interface DraftCreateSinglePlayRes {
  DraftID: string
  PlayerID: string
}

export const useJsonrpcDraftCreateSinglePlay = () => {
  return useJsonrpc<DraftCreateSinglePlayArg, DraftCreateSinglePlayRes>("/api/draft/rpc", "DraftRPC.CreateSinglePlay")
}
