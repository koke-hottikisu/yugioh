import { useJsonrpc } from '../../useJsonrpc'

interface DraftStateArg {
  DraftID: string
  PlayerID: string
}

export interface Player {
  Name: string
  PickPack: string[]
  OwnPackNum: number
  PickedCard: string[]
  PickDeadline: string
}
const PlayerInit: Player = {
  Name: "",
  PickPack: [],
  OwnPackNum: 0,
  PickedCard: [],
  PickDeadline: "",
}

export interface DraftStateRes {
	IsStarted: boolean
	IsEnd: boolean
	OwnerName: string
  OwnerID: string
	Me: Player
	AllPlayer: {
		Name: string
    ID: string
    OwnPackNum: number
	}[]
}
export const DraftStateResInit: DraftStateRes = {
	IsStarted: false,
	IsEnd: false,
	OwnerName: "",
  OwnerID: "",
	Me: PlayerInit,
	AllPlayer: []
}


export const useJsonrpcDraftState = () => {
  return useJsonrpc<DraftStateArg, DraftStateRes>("/api/draft/rpc", "DraftRPC.State")
}
