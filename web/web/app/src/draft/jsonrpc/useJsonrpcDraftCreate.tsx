import { useJsonrpc } from '../../useJsonrpc'

interface DraftCreateArg {
  PlayerName: string
  Card: {
    Name: string
    EncloseNum: number
  }[]
	Rotation: number
	CardNumInPack: number
}

interface DraftCreateRes {
  DraftID: string
  PlayerID: string
}

export const useJsonrpcDraftCreate = () => {
  return useJsonrpc<DraftCreateArg, DraftCreateRes>("/api/draft/rpc", "DraftRPC.Create")
}
