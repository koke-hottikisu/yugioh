import * as React from 'react'
import useAxios from 'axios-hooks'

export const useCardnameComplete = (name: string) => {
  const [res, setRes] = React.useState<string[]>([])

  const [resp, _] = useAxios({
    url: "/api/cardname-complete/",
    method: "get",
    params: {
      q: name,
      size: 30,
    }
  }, {
      manual: false,
  })

  React.useEffect(() => {
    if (resp.loading || resp.error) {}
    else {
      setRes(resp.data.res)
    }
  }, [resp])

  return res
}
