import * as React from 'react'
import useAxios from 'axios-hooks'
import { Badge, OverlayTrigger, Tooltip } from 'react-bootstrap'

type Props = {
  cardname: string
}

export const CardValidateBadge = (props: Props) => {
  const [req, reqDo] = useAxios({
    url: "/api/carddb-app/card",
    method: "get",
    params: {name: props.cardname}
  }, {
    manual: true
  })

  React.useEffect(() => {
    reqDo()
  }, [props.cardname])

  return (
    <>
      <OverlayTrigger
        overlay={<Tooltip id="button-tooltip">
          カード名が正しくありません
        </Tooltip>}>
        <Badge bg="danger" hidden={!req.error}>!</Badge>
      </OverlayTrigger>
    </>
  )

}
