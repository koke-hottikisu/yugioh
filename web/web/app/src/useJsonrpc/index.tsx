import useAxios, { ResponseValues } from 'axios-hooks'
import { AxiosPromise } from 'axios'

interface JsonrpcError {
  code: number
  message: string
}

export interface JsonrpcResponse<Res> {
  jsonrpc: string
  id: string
  result: Res
  error: JsonrpcError
}

export interface Options<Arg> {
  manual: boolean
  param?: Arg
}

export interface ReqOptions<Arg> {
  manual?: boolean
  param?: Arg
}

export const useJsonrpc = <Arg, Res>(
  url: string, method: string, reqOptions?: ReqOptions<Arg>): [{
    data: Res, loading: boolean, error: boolean, response: ResponseValues<JsonrpcResponse<Res>, any>
  }, (arg: Arg) => AxiosPromise<JsonrpcResponse<Res>>] => {

  const options: Options<Arg> = {
    manual: true,
    ...reqOptions
  }

  const getRequestBody = (arg: Arg) => {
    return JSON.stringify({
        jsonrpc: "2.0",
        method: method,
        params: arg,
        id: 0,
      })
  }

  const [resAxios, exeAxios] = useAxios<JsonrpcResponse<Res>>({
    url: url,
    method: "post",
    headers: {'Content-Type': 'application/json'},
    data: getRequestBody(options.param)
  }, {
    manual: options.manual
  })

  const executeJ = (arg: Arg): AxiosPromise<JsonrpcResponse<Res>> => {
    return exeAxios({
      data: getRequestBody(arg)
    })
  }

  return [{
    data: resAxios.data === undefined ? undefined : resAxios.data.result,
    loading: resAxios.loading,
    error: Boolean(resAxios.error) || (resAxios.data === undefined ? false : Boolean(resAxios.data.error)),
    response: resAxios,
  }, executeJ]
}
