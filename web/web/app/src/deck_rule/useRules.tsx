import useAxios from 'axios-hooks'
import * as React from 'react'
import { useEffectWithoutInit } from '../common/useEffectWithoutInit'
import { AuthContext } from '../context'

import { Rule } from './rule/useRule'

export type DispatchRule = React.Dispatch<((prev: Rule) => Rule)>

export interface RulesHooks {
  setRules: (rules: Rule[]) => void
  addRule: (owner: string) => void
  getDeleteRule: (index: number) => (() => void)
  getRuleDispatch: (index: number) => DispatchRule
}

export const useRules = (initRules: Rule[]): [Rule[], RulesHooks] => {
  const [rules, setRules] = React.useState<Rule[]>(initRules)
  const [auth, authHooks] = React.useContext(AuthContext)

  const [reqGetRules, reqGetRulesDo] = useAxios("/api/deckrule/rule")
  const [reqDeleteRule, reqDeleteRuleDo] = useAxios({
    url: "/api/deckrule/rule",
    method: "delete"
  }, {
    manual: true
  })

  useEffectWithoutInit(() => {
    if(reqGetRules.loading) {
      setRules([])
    } else if(reqGetRules.error) {
      setRules([])
    } else {
      setRules(reqGetRules.data)
    }
  }, [reqGetRules])

  const addRule = (owner: string) => {
    const newRule: Rule = {
      id: "",
      name: "新しいルール名",
      costcards: [],
      owners: [owner],
      ownercandidates: [],
    }
    setRules((prev) => {
      return [newRule, ...prev]
    })
  }

  useEffectWithoutInit(() => {
    if (!reqDeleteRule.loading && !reqDeleteRule.error) {
      reqGetRulesDo()
    }
  }, [reqDeleteRule])
  const getDeleteRule = (index: number) => {
    return () => {
      reqDeleteRuleDo({
        headers: {"Authorization": "userKey " + auth.userKey},
        data: rules[index],
      })
    }
  }

  const getRuleDispatch = (index: number) => {
    return (action: (prevRule: Rule) => Rule) => {
      setRules((prevRules: Rule[]) => {
        const newRules = [...prevRules]
        newRules[index] = action(prevRules[index])
        return newRules
      })
    }
  }

  return [rules, {setRules, addRule, getDeleteRule, getRuleDispatch}]
}
