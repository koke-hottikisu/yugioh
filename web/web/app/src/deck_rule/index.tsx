import * as React from 'react'
import { RuleDom } from './rule'
import { Rule } from './rule/useRule'
import { useRules } from './useRules'

import { Accordion } from 'react-bootstrap'
import { AddRuleButton } from './AddRuleButton'

export const DeckRulePage = () => {
  const [rules, rulesHooks] = useRules([])

  return (
    <>
      <AddRuleButton rulesHooks={rulesHooks} />

      <Accordion>
        {rules.map((rule: Rule, i: number) => (
          <Accordion.Item key={i} eventKey={String(i)}>
            <Accordion.Header>{rule.name}</Accordion.Header>

            <Accordion.Body>
              <RuleDom rule={rule}
                       setRule={rulesHooks.getRuleDispatch(i)}
                       deleteRule={rulesHooks.getDeleteRule(i)} />
            </Accordion.Body>
          </Accordion.Item>
        ))}
      </Accordion>
    </>
  )
}
