import * as React from 'react'

export interface Costcard {
  name: string
  cost: number
}

export interface CostcardHooks {
  setName: (name: string) => void
  setCost: (cost: number) => void
}

export const useCostcard = (costcard: Costcard, setCostcard: React.Dispatch<React.SetStateAction<Costcard>>): [Costcard, CostcardHooks] => {
  const setName = (name: string) => {
    setCostcard((prev: Costcard) => ({...prev, name}))
  }
  const setCost = (cost: number) => {
    setCostcard((prev: Costcard) => ({...prev, cost}))
  }

  return [costcard, {setName, setCost}]
}
