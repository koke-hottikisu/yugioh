import * as React from 'react'
import { Button, Form } from 'react-bootstrap'
import { RiDeleteBin6Line } from 'react-icons/ri';
import { Costcard, useCostcard } from './useCostcard'
import { CardSelector } from '../../../../card_selector'
import { CardValidateBadge } from '../../../../card_validate_badge'

type Props = {
  costcard: Costcard
  setCostcard: React.Dispatch<React.SetStateAction<Costcard>>
  deleteCostcard: () => void
}

export const CostcardDom: React.FC<Props> = (props) => {
  const [costcard, costcardHooks] = useCostcard(props.costcard, props.setCostcard)

  return (
    <>
      <CardSelector placeholder="カード名"
                    text={costcard.name}
                    onChange={costcardHooks.setName}
                    requestParam={{}}
      />
      <CardValidateBadge cardname={costcard.name} />
      <Form.Control type="number"
                    value={costcard.cost}
                    placeholder="コスト"
                    onChange={(e) => {
                      costcardHooks.setCost(Number(e.target.value))
                    }} />
      <Button variant=""
              onClick={props.deleteCostcard}>
        <RiDeleteBin6Line />
      </Button>
    </>
  )
}
