import * as React from 'react'
import { DispatchCostcards } from '../useRule'
import { Costcard } from './costcard/useCostcard'

export type DispatchCostcard = React.Dispatch<((prev: Costcard) => Costcard)>

export interface CostcardsHooks {
  addCostcard: () => void
  moveCostcard: (oldIndex: number, newIndex: number) => void
  getDeleteCostcard: (index: number) => (() => void)
  getCostcardDispatch: (index: number) => DispatchCostcard
  exportcsv: () => string
  importcsv: (data: string) => void
}

export const useCostcards = (costcards: Costcard[], setCostcards: DispatchCostcards): [Costcard[], CostcardsHooks] => {
  const addCostcard = () => {
    setCostcards((prev) => {
      const newCostcard = {name: "", cost: 0}
      return [...prev, newCostcard]
    })
  }

  const getDeleteCostcard = (index: number) => {
    return () => {
      setCostcards((prev: Costcard[]) => {
        const newCostcards = [...prev]
        newCostcards.splice(index, 1)
        return newCostcards
      })
    }
  }

  const moveCostcard = (oldIndex: number, newIndex: number) => {
    setCostcards((prev: Costcard[]) => {
      const target = costcards[oldIndex]
      const newCostcards = [...prev]
      newCostcards.splice(oldIndex, 1)
      newCostcards.splice(newIndex, 0, target)
      return newCostcards
    })
  }

  const getCostcardDispatch = (index: number) => {
    return (action: (prevCostcard: Costcard) => Costcard) => {
      setCostcards((prevCostcards: Costcard[]) => {
        const newCostcards = [...prevCostcards]
        newCostcards[index] = action(prevCostcards[index])
        return newCostcards
      })
    }
  }

  const exportcsv = (): string => {
    var data: string = ""
    costcards.map((costcard: Costcard) => {
      data = data + costcard.name + "," + costcard.cost + "\n"
    })
    return data
  }

  const importcsv = (data: string) => {
    const res: Costcard[] = []
    data.split("\n").map((row: string) => {
      const elem = row.split(",")
      const costcard = {
        name: elem[0],
        cost: Number(elem[1]),
      }
      if (costcard.name != "" && costcard.cost != Number.NaN ) {
        res.push(costcard)
      }
    })
    setCostcards((prev) => (res))
  }

  return [costcards, {addCostcard, getDeleteCostcard, moveCostcard, getCostcardDispatch, exportcsv, importcsv}]
}
