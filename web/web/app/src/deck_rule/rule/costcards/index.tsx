import * as React from 'react'
import { Button, Card, Form } from 'react-bootstrap'
import { SortableContainer, SortableElement, SortableHandle } from 'react-sortable-hoc'
import { BsGripVertical } from 'react-icons/bs'
import { BiAddToQueue } from 'react-icons/bi'

import { DispatchCostcards } from '../useRule'
import { CostcardDom } from './costcard'
import { Costcard } from './costcard/useCostcard'
import { CostcardsHooks, useCostcards } from './useCosts'

const DragHandle = SortableHandle(() => (<span><BsGripVertical/></span>))

type SortableItemProps = {
  itemIndex: number
  value: Costcard
  costcardsHooks: CostcardsHooks
}
const SortableItem = SortableElement((props: SortableItemProps) => (
  <li key={props.itemIndex}>
    <Card style={{flexDirection: "row"}}>
      <DragHandle />
      <CostcardDom key={props.itemIndex}
                   costcard={props.value}
                   setCostcard={props.costcardsHooks.getCostcardDispatch(props.itemIndex)}
                   deleteCostcard={props.costcardsHooks.getDeleteCostcard(props.itemIndex)} />
    </Card>
  </li>
))

type SortableListProps = {
  items: Costcard[]
  costcardsHooks: CostcardsHooks
}
const SortableList = SortableContainer((props: SortableListProps) => (
  <ul style={{listStyleType: "none", paddingLeft: 0}}>
    {props.items.map((value: Costcard, index: number) => (
      <SortableItem key={index}
                    index={index}
                    itemIndex={index}
                    value={value}
                    costcardsHooks={props.costcardsHooks} />
    ))}
  </ul>
))

type Props = {
  costcards: Costcard[]
  setCostcards: DispatchCostcards
}
export const CostcardsDom: React.FC<Props> = (props) => {
  const [costcards, costcardsHooks] = useCostcards(props.costcards, props.setCostcards)

  return (
    <>
      <Form.Group>
        <Form.Label>
          カードリスト
          <Button variant=""
                  onClick={() => {costcardsHooks.addCostcard()}}>
            <BiAddToQueue />
          </Button>
        </Form.Label>

        <SortableList items={costcards}
                      costcardsHooks={costcardsHooks}
                      lockAxis="y"
                      useDragHandle
                      onSortEnd={({oldIndex, newIndex, collection, isKeySorting}, e) => {
                        costcardsHooks.moveCostcard(oldIndex, newIndex)
                      }} />

        <Form.Control as="textarea"
                      value={costcardsHooks.exportcsv()}
                      onChange={(e) => {costcardsHooks.importcsv(e.target.value)} } />
      </Form.Group> 
    </>
  )
}
