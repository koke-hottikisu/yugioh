import * as React from 'react'
import { Button, OverlayTrigger, Tooltip } from 'react-bootstrap'
import { Rule, RuleHooks } from './useRule'
import { ConfirmModal } from '../../confirm_modal'
import { useConfirmModal } from '../../confirm_modal/useConfirmModal'

type Props = {
  rule: Rule
  ruleHooks: RuleHooks
  deleteRule: () => void
}
const DeleteRuleButtonEnable = (props: Props) => {
  const confirmModalHooks = useConfirmModal()

  return (
    <>
      <Button onClick={confirmModalHooks.show}
              variant="danger">
        ルール削除
      </Button>
      <ConfirmModal
        title="本当に削除しますか?"
        confirmModalHooks={confirmModalHooks}
        onConfirm={props.deleteRule}
        onCancel={() => { }}
      >
        {props.rule.name}
      </ConfirmModal>
    </>
  )
}

const DeleteRuleButtonDisable = () => {
  return (
    <>
      <OverlayTrigger
        placement="right"
        overlay={<Tooltip id="tooltip-disabled">
          ログインしていないか、編集権がありません
        </Tooltip>}>
        <span className="d-inline-block">
          <Button disabled
                  variant="danger">
            ルール削除
          </Button>
        </span>
      </OverlayTrigger>
    </>
  )
}

export const DeleteRuleButton = (props: Props) => {
  return (
    <>
      {props.ruleHooks.hasEditPermission()
      ?(<DeleteRuleButtonEnable {...props}/>)
       :(<DeleteRuleButtonDisable/>)
      }
    </>
  )
}

