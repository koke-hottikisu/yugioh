import * as React from 'react'
import { Form, Spinner } from 'react-bootstrap'
import { CostcardsDom } from './costcards'

import { DispatchRule } from '../useRules'
import { Rule, useRule } from './useRule'
import { DeleteRuleButton } from './DeleteRuleButton'
import { ApplyRuleButton } from './ApplyRuleButton'

type Props = {
  rule: Rule
  setRule: DispatchRule
  deleteRule: () => void
}

export const RuleDom: React.FC<Props> = (props) => {
  const [rule, ruleHooks] = useRule(props.rule, props.setRule)

  return (
    <>
      <DeleteRuleButton
        rule={rule}
        ruleHooks={ruleHooks}
        deleteRule={props.deleteRule} />

      <ApplyRuleButton
        rule={rule}
        ruleHooks={ruleHooks} />
      <Spinner animation="border"
               hidden={!ruleHooks.postRule.res.loading}
      />

      <Form.Group>
        <Form.Label>ルール名</Form.Label>
        <Form.Control type="text"
                      value={rule.name}
                      onChange={(e) => {ruleHooks.setName(e.target.value)}} />
      </Form.Group>

      <CostcardsDom costcards={rule.costcards}
                    setCostcards={ruleHooks.setCostcards}
      />

    </>
  )
}
