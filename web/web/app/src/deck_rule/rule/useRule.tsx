import useAxios, { ResponseValues } from 'axios-hooks'
import * as React from 'react'
import { useEffectWithoutInit } from '../../common/useEffectWithoutInit'
import { AuthContext } from '../../context'
import { DispatchRule } from '../useRules'
import { Costcard } from './costcards/costcard/useCostcard'

export interface Rule {
  id: string
  name: string
  costcards: Costcard[]
  owners: string[]
  ownercandidates: string[]
}

export type DispatchCostcards = React.Dispatch<(prev: Costcard[]) => Costcard[]>

export interface RuleHooks {
  setName: (name: string) => void
  setCostcards: DispatchCostcards
  postRule: {
    do: () => void
    res: ResponseValues<any, any>
  }
  hasEditPermission: () => boolean
}

export const useRule = (rule: Rule, setRule: DispatchRule): [Rule, RuleHooks] => {
  const [auth, authHooks] = React.useContext(AuthContext)
  const [reqPostRule, reqPostRuleDo] = useAxios({
    url: "/api/deckrule/rule",
    method: "post"
  }, {
    manual: true
  })
  useEffectWithoutInit(() => {
    if (!reqPostRule.error && !reqPostRule.loading) {
      setRule((prev: Rule) => (reqPostRule.data))
    }
  }, [reqPostRule])

  const setName = (name: string) => {
    setRule((prev: Rule) => {
      return {...prev, name}
    })
  }

  const setCostcards = (action: (prevCostcards: Costcard[]) => Costcard[]) => {
    setRule((prevRule: Rule) => {
      return {...prevRule, costcards: action(prevRule.costcards)}
    })
  }

  const postRule = () => {
    reqPostRuleDo({
      headers: {"Authorization": "userKey " + auth.userKey},
      data: rule,
    })
  }

  const hasEditPermission = (): boolean => {
    if(!auth.exists) {
      return false
    }
    return rule.owners.includes(auth.userId)
  }

  return [rule, {setName, postRule: {do: postRule, res: reqPostRule}, setCostcards, hasEditPermission}]
}
