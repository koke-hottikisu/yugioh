import * as React from 'react'
import { Button, OverlayTrigger, Tooltip } from 'react-bootstrap'
import { Rule, RuleHooks } from './useRule'

type Props = {
  rule: Rule
  ruleHooks: RuleHooks
}
const ApplyRuleButtonEnable = (props: Props) => {
  return (
    <>
      <Button onClick={props.ruleHooks.postRule.do}
              variant="primary">
        保存
      </Button>
    </>
  )
}

const ApplyRuleButtonDisable = () => {
  return (
    <>
      <OverlayTrigger
        placement="right"
        overlay={<Tooltip id="tooltip-disabled">
          ログインしていないか、編集権がありません
        </Tooltip>}>
        <span className="d-inline-block">
          <Button disabled
                  variant="primary">
            保存
          </Button>
        </span>
      </OverlayTrigger>
    </>
  )
}

export const ApplyRuleButton = (props: Props) => {
  return (
    <>
      {props.ruleHooks.hasEditPermission()
      ?(<ApplyRuleButtonEnable {...props}/>)
       :(<ApplyRuleButtonDisable/>)
      }
    </>
  )
}

