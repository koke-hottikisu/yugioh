import * as React from 'react'
import { Button, OverlayTrigger, Tooltip } from 'react-bootstrap'
import { AuthContext } from '../context'
import { RulesHooks } from './useRules'



type EnableProps = {
  rulesHooks: RulesHooks
  userId: string
}
const AddRuleButtonEnable: React.FC<EnableProps> = (props) => {
  return (
    <>
      <Button
        variant="secondary"
        onClick={() => { props.rulesHooks.addRule(props.userId) }}
      >新規ルール追加</Button>
    </>
  )
}

const AddRuleButtonDisable: React.FC = () => {
  return (
    <>
      <OverlayTrigger
        placement="right"
        overlay={<Tooltip id="tooltip-disabled">
          ログインしてください
        </Tooltip>}>
        <span className="d-inline-block">
          <Button
            variant="secondary"
            disabled
          >新規ルール追加</Button>
        </span>
      </OverlayTrigger>
    </>
  )
}

type Props = {
  rulesHooks: RulesHooks
}
export const AddRuleButton: React.FC<Props> = (props) => {
  const [auth, authHooks] = React.useContext(AuthContext)

  return (
    <>
      {auth.exists
      ?(<AddRuleButtonEnable {...props}
                             userId={auth.userId} />)
       :(<AddRuleButtonDisable/>)
      }
    </>
  )
}
