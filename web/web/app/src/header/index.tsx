import * as React from 'react'
import { Navbar, Nav, Container, NavDropdown, Image } from 'react-bootstrap'
import Favicon from 'react-favicon'
import { TwitterLogin } from '../twitter_login'

import IconImage from './icon2.png'

export const Header = () => {
  return (
    <>
      <Favicon url={IconImage} />

      <Navbar bg="dark" variant="dark">
        <Container>
          <Navbar.Brand href="/"><Image src={IconImage} height="30" className="d-inline-block align-top"/></Navbar.Brand>
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="me-auto">
              <NavDropdown title="02環境">
                <NavDropdown.Item href="/search02">カード検索</NavDropdown.Item>
                <NavDropdown.Item href="/checker/deck">デッキチェッカー</NavDropdown.Item>
                <NavDropdown.Item href="/checker/cardname">カード名から02チェック</NavDropdown.Item>
              </NavDropdown>
              <NavDropdown title="ドラフト">
                <NavDropdown.Item href="/draft/top">Top</NavDropdown.Item>
                <NavDropdown.Item href="/cardpool/top">カードプール</NavDropdown.Item>
              </NavDropdown>
            </Nav>
          </Navbar.Collapse>
	        <TwitterLogin/>
        </Container>
      </Navbar>
    </>
  )
}
