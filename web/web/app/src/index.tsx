import * as React from 'react'
import * as ReactDOM from 'react-dom'
import { Route, BrowserRouter, Switch, Redirect } from 'react-router-dom'
import { Context } from './context'

import { TopPage } from './top'
import { CardNamePage } from './checker/cardname'
import { DeckRulePage } from './deck_rule'
import { DraftRoute } from './draft'
import { CardpoolRoute } from './draft_cardpool'
import { CardSearch02Page } from './card_search/CardSearch02'
import { LinksDeckPage } from './checker/linksdeck'
import { Header } from './header'

ReactDOM.render(
  <>
    <Context>
      <BrowserRouter>
        <Header/>
        <Switch>
          <Route exact path='/' component={TopPage} />
          <Route path='/checker/cardname' component={CardNamePage} />
          <Route path='/checker/deck' component={LinksDeckPage} />
          <Route path='/search02' component={CardSearch02Page}/>
          <Route path='/deck_rule' component={DeckRulePage} />
          <Route path='/draft' component={DraftRoute} />
          <Route path='/cardpool' component={CardpoolRoute} />
        </Switch>
      </BrowserRouter>
    </Context>
  </>,
  document.getElementById('app')
)
