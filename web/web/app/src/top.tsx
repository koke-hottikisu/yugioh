import * as React from 'react'
import { Button, Row, Container, Col } from 'react-bootstrap'

import BackGround from './draft/bg.png'

export const TopPage = () => {
  React.useEffect(() => {
    document.title = "トップページ | 02環境ツール | slimemoss"
  }, []);

  return (
    <>
      <div style={{backgroundImage: `url(${BackGround})`,
                   flex: "1 1 100%",
                   backgroundSize: "contain",
                   backgroundColor: "#1f1f1e",
                   backgroundRepeat: "no-repeat",
                   backgroundPosition: "center"}}>
        <Container>
          <Row style={{ maxWidth: "45rem", marginLeft: 'auto', marginRight: 'auto', marginTop: '8rem' }}>
            <Col>
              <Row>
                <Col>
                  <h1 style={{fontSize: "3rem", fontFamily: "Hannotate SC", fontWeight: "bold"}}>遊戯王 02環境ツール</h1>
                </Col>
              </Row>
              <Row>
                <Col>
                  <Button href="/search02" className="m-2 p-2"
                          variant="outline-light" style={{minWidth: "10rem"}}>
                    カード検索</Button>
                  <Button href="/checker/deck" className="m-2 p-2"
                          variant="outline-light" style={{minWidth: "10rem"}}>
                    マスターデュエル デッキチェッカー</Button>
                  <Button href="/checker/cardname" className="m-2 p-2"
                          variant="outline-light" style={{minWidth: "10rem"}}>
                    カード名から02チェック</Button>
                </Col>
              </Row>
            </Col>
          </Row>

          <Row className="py-4">
            <Col sm="4">
              <div className="p-2" style={{borderTop: '4px solid', borderTopColor: '#333333'}}>
                <h3>02環境とは?</h3>
                <p>2002ごろ(2期)の環境で遊ぶ独自ルールです。</p>
                <p>もっと知りたい方はニコツさんの
                  <a href="https://www.youtube.com/c/%E3%83%8B%E3%82%B3%E3%83%84" target="_blank">YouTube</a>や
                  <a href="http://blog.livedoor.jp/nikotusanko/" target="_blank">ブログ</a>をご覧ください
                </p>
              </div>
            </Col>

            <Col sm="4">
              <div className="p-2" style={{borderTop: '4px solid', borderTopColor: '#333333'}}>
                <h3>使えるカードは?</h3>
                <p>2002年03月21日の
                  <a href="https://yugioh-wiki.net/index.php?%A5%AB%A1%BC%A5%C9%A5%EA%A5%B9%A5%C8#zd031dfd" target="_blank">STRUCTURE DECK－ペガサス・Ｊ・クロフォード編－</a>
                  までに発売されたカード と
                  <a href="https://yugioh-wiki.net/index.php?%BB%B0%B8%B8%BF%C0" target="_blank">三幻神</a>
                  です。
                </p>
                <p>
                  <a href="/search02" target="_blank">02環境カード検索</a>
                  から使えるカードを検索してみてください！
                </p>
              </div>
            </Col>

            <Col sm="4">
              <div className="p-2" style={{borderTop: '4px solid', borderTopColor: '#333333'}}>
                <h3>やってみたい</h3>
                <p>デュエルリンクス・マスターデュエルで気軽に遊ぶことができます。</p>
                <p>公開されたルームを
                  <a href="https://twitter.com/search?q=02%E7%92%B0%E5%A2%83%20%E3%83%AB%E3%83%BC%E3%83%A0&src=typed_query&f=live" target="_blank">Twitterで探してみて</a>
                  ください。</p>
                <p>プレイする前に
                  <a href="/checker/cardname" target="_blank">使えないカードが混ざっていないか確認する</a>
                  のを忘れずに！</p>
              </div>
            </Col>

          </Row>
        </Container>
      </div>
    </>
  )
}
