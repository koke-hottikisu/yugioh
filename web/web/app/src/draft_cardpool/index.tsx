import * as React from 'react'

import { Route, Switch } from 'react-router-dom'
import { CardpoolPage } from './top/CardpoolPage'
import { AddCardpoolPage } from './AddCardpoolPage'
import { DetailPage } from './DetailPage'
import { EditCardpoolPage } from './EditCardpoolPage'

export const CardpoolRoute = () => {
  React.useEffect(() => {
    document.title = "ブースター・ドラフト カードプール | slimemoss"
  }, []);

  return (
    <Switch>
      <Route path="/cardpool/top" component={CardpoolPage}/>
      <Route path="/cardpool/add" component={AddCardpoolPage}/>
      <Route path="/cardpool/detail/:id" component={DetailPage} />
      <Route path="/cardpool/edit/:id" component={EditCardpoolPage} />
    </Switch>
  )
}
