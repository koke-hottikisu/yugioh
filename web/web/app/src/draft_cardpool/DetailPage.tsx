import * as React from 'react'
import { Button, Card, Container, Image, Row, Col } from 'react-bootstrap'
import { useParams } from 'react-router'

import { AuthContext } from '../context'
import { ImageUrl } from './Image'
import { GroupByEncloseNum, Sum } from './DraftSet'
import { useGet } from './jsonrpc/useGet'
import { Cardpool } from './jsonrpc/useCreate'
import { useUserInfo } from '../twitter_login/useUserInfo'
import { useYugiohCard } from '../card_info/useYugiohCard'

const DetailCardDom = (props: {name: string}) => {
  const yugiohCard = useYugiohCard(props.name)

  return (
    <a href={yugiohCard.url} target="_blank" style={{color: 'inherit', textDecoration: 'inherit'}}>
      <Card className="p-0" style={{width: '7rem', fontSize: '0.75rem'}}>
        <Card.Header className="p-0" style={{overflow: 'hidden', textOverflow: 'ellipsis', whiteSpace: 'nowrap'}}>
          {props.name}</Card.Header>
        <Card.Img src={"https://" + yugiohCard.imageurl[0]} alt={props.name} />
      </Card>
    </a>
  )
}

const DetailDom = (props: {cardpool: Cardpool}) => {
  const [auth, _] = React.useContext(AuthContext)
  const userInfo = useUserInfo(props.cardpool.UserID)

  return (
    <>
      <Container>
        <Row className="justify-content-md-center pt-3">
          <Col xs={12} sm={6}>
            <Image alt="icon" src={ImageUrl()} fluid={true} />

            <div>
              <div>
                by  <a href={userInfo.url} target="_blank">{userInfo.name}</a>
              </div>
            </div>
          </Col>

          <Col sm={1}>
            <Button disabled={ !auth.exists && props.cardpool.UserID != auth.userId }
                    onClick={() => {
                      location.href = "/cardpool/edit/" + props.cardpool.ID
                    }}>編集</Button>
          </Col>
        </Row>

        <Row>
          <Col>
            <Button href={"/draft/singleplay?cardpoolid=" + props.cardpool.ID} className="m-2 p-2" variant="outline-light" style={{minWidth: "10rem"}}>対CPUドラフト</Button>
            <Button href={"/draft/create?cardpoolid=" + props.cardpool.ID} className="m-2 p-2" variant="outline-light" style={{minWidth: "10rem"}}>対人ドラフト</Button>
          </Col>
        </Row>

        <Row className="justify-content-md-center pt-3">
          {GroupByEncloseNum(props.cardpool.Set.concat()).map((cards, index) => (
            <div key={index}>
              <div>
                封入率 {cards[0].EncloseNum} / {Sum(props.cardpool.Set)}
              </div>
              <div>
                全{cards.length}種類
              </div>

              <div style={{display: 'flex', flexWrap: 'wrap'}}>
                {cards.concat().map((card, index) => (
                  <div key={index}>
                    <DetailCardDom name={card.Name} />
                  </div>
                ))}
              </div>
            </div>
          ))}
        </Row>
      </Container>
    </>
  )
}

interface Param {
  id: string
}
export const DetailPage = () => {
  const { id } = useParams<Param>()
  const [respGet, _] = useGet(id)

  if (respGet.loading) {
    return <p>loading</p>
  }
  if (respGet.error) {
    return <p>error</p>
  }

  return (
    <>
      <DetailDom cardpool={respGet.data}/>
    </>
  )
}
