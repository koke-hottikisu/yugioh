import * as React from 'react'
import { Form, ListGroup } from 'react-bootstrap'
import { CardValidateBadge } from '../../card_validate_badge'
import { DraftSet, DraftSetHooks } from './useDraftSet'

interface Props {
  draftSet: DraftSet[]
  draftSetHooks: DraftSetHooks
}

export const DraftSetForm = (props: Props) => {
  const cardsetPlaceholder = "ドラフトで使うカードセット\n\
csv形式で、[カード名, 封入数]\n\
ex)\n\
万能地雷グレイモヤ,1\n\
ハネハネ,3\n\
ワイト,11\n\
\n\
mtgでは、超強,1(80種)  強い,3(80種)  普通,11(80種)の割合です。"

  return (
    <>
      <Form.Group className="pt-3">
        <Form.Label>カードプール</Form.Label>
        <Form.Control as="textarea" required
                      placeholder={cardsetPlaceholder}
                      style={{ height: '20rem' }}
                      value={props.draftSetHooks.exportcsv()}
                      onChange={(e) => {props.draftSetHooks.importcsv(e.target.value)} }
        />
      </Form.Group>

      <Form.Group className="pt-3">
        <ListGroup>
          {props.draftSet.map((d: DraftSet, index) => (
            <ListGroup.Item className="p-0 bg-transparent" key={index}>
              {d.Name} : {d.EncloseNum}
              <CardValidateBadge cardname={d.Name}/>
            </ListGroup.Item>
          ))}
        </ListGroup>
      </Form.Group>
    </>
  )
}
