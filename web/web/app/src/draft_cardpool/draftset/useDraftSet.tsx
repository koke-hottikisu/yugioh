import * as React from 'react'
import { useGet } from '../jsonrpc/useGet'

export interface DraftSet {
  Name: string
  EncloseNum: number
}

export interface DraftSetHooks {
  exportcsv: () => string
  importcsv: (data: string) => void
  load: (cardpoolid: string) => void
}

export const useDraftSet = (init: DraftSet[]): [DraftSet[], DraftSetHooks] => {
  const [draftSet, setDraftSet] = React.useState<DraftSet[]>(init)
  const [_a, reqGet] = useGet("")

  const exportcsv = (): string => {
    var data: string = ""
    draftSet.map((draftset: DraftSet) => {
      var encloseNum = 1
      if(!Number.isNaN(draftset.EncloseNum)) {
        encloseNum = draftset.EncloseNum
      }
      data = data + draftset.Name + ", " + encloseNum + "\n"
    })
    return data
  }

  const importcsv = (data: string) => {
    const res: DraftSet[] = []
    data.split("\n").map((row: string) => {
      const elem = row.split(/[,\t] */)
      const draftset: DraftSet = {
        Name: elem[0],
        EncloseNum: Number(elem[1]),
      }
      if (draftset.Name != "" && draftset.EncloseNum != Number.NaN ) {
        res.push(draftset)
      }
    })
    setDraftSet(res)
  }

  const load = (cardpoolid: string) => {
    reqGet({ID: cardpoolid})
      .then((resp) => {
        setDraftSet(resp.data.result.Set)
      })
  }

  return [draftSet, {exportcsv, importcsv, load}]
}
