export const ImageUrl = () => {
  const width = 1280
  const height = 720

  const canvasElem = document.createElement('canvas')
  canvasElem.width = width
  canvasElem.height = height
  const ctx = canvasElem.getContext('2d')

  // draw
  ctx.fillRect(0, 0, width, height)
  ctx.fillStyle = '#888888'

  return canvasElem.toDataURL()
}
