import * as React from 'react'
import { Button, Container, Form, Toast } from 'react-bootstrap'
import { useParams } from 'react-router'

import { AuthContext } from '../context'
import { CardpoolForm } from './CardpoolForm'
import { useDraftSet } from './draftset/useDraftSet'
import { useUpdate } from './jsonrpc/useUpdate'
import { useGet } from './jsonrpc/useGet'
import { useDelete } from './jsonrpc/useDelete'
import { Cardpool } from './jsonrpc/useCreate'
import { ConfirmModal } from '../confirm_modal2'

export const EditCardpoolDom = (props: {cardpool: Cardpool}) => {
  const [auth, _] = React.useContext(AuthContext)
  const [name, setName] = React.useState(props.cardpool.Name)
  const [draftSet, draftSetHooks] = useDraftSet(props.cardpool.Set)
  const [_a, reqUpdate] = useUpdate()
  const [_b, reqDelete] = useDelete()

  const [show, setShow] = React.useState(false)

  if(auth.userId != props.cardpool.UserID) {
    return (
      <>
        <p>wrong user</p>
        <p>{auth.userId}</p>
        <p>{props.cardpool.UserID}</p>
      </>
    )
  }

  return (
    <>
      <Container>
        <Form>
          <div className="m-1">
            <Button className="m-1"
                    onClick={() => {
                      reqUpdate({
                        ID: props.cardpool.ID,
                        Name: name,
                        Set: draftSet,
                        UserKey: auth.userKey
                      }).then(() => {
                        location.href = "/cardpool/top"
                      }).catch(() => {
                        setShow(true)
                      })
                    }}>反映</Button>

            <ConfirmModal buttonProps={{className: "m-1", variant:"danger"}}
                          buttonElem={<div>削除</div>}
                          modalTitle="削除しますか"
                          onConfirm={() => {
                            reqDelete({ID: props.cardpool.ID, UserKey: auth.userKey})
                              .then(() => {location.href = "/cardpool/top"})
                          }}
            />
          </div>

          <CardpoolForm props={{name, setName, draftSet, draftSetHooks}} />

        </Form>
      </Container>

      <Toast show={show} onClose={() => {setShow(false)}}>
        <Toast.Header>
          <strong className="me-auto">反映できませんでした</strong>
        </Toast.Header>
      </Toast>
    </>
  )
}


interface Param {
  id: string
}
export const EditCardpoolPage = () => {
  const { id } = useParams<Param>()
  const [respGet, _] = useGet(id)

  if (respGet.loading) {
    return <p>loading</p>
  }
  if (respGet.error) {
    return <p>error</p>
  }

  return (
    <>
      <EditCardpoolDom cardpool={respGet.data}/>
    </>
  )
}
