export interface DraftSet {
  Name: string
  EncloseNum: number
}

const groupBy = <K extends PropertyKey, V>(
  array: readonly V[],
  getKey: (cur: V, idx: number, src: readonly V[]) => K
  ) =>
  array.reduce((obj, cur, idx, src) => {
    const key = getKey(cur, idx, src);
    (obj[key] || (obj[key] = []))!.push(cur);
    return obj;
  }, {} as Record<K, V[]>);

export const GroupByEncloseNum = (set: DraftSet[]): DraftSet[][] => {
  const group = groupBy<number, DraftSet>(set.concat(), (s) => (s.EncloseNum))

  const res: DraftSet[][] = []
  Object.entries(group).forEach(([_, value]) => {
    res.push(value)
  })

  res.sort((a, b) => (a[0].EncloseNum - b[0].EncloseNum))

  return res
}

export const Sum = (set: DraftSet[]) => {
  return set.reduce((prev, cur) => (prev + cur.EncloseNum), 0)
}
