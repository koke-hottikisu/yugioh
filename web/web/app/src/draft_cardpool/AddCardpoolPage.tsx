import * as React from 'react'
import { Button, Container, Form, Toast } from 'react-bootstrap'
import { AuthContext } from '../context'

import { CardpoolForm } from './CardpoolForm'
import { useDraftSet } from './draftset/useDraftSet'
import { useCreate } from './jsonrpc/useCreate'

export const AddCardpoolPage = () => {
  const [auth, _] = React.useContext(AuthContext)
  const [name, setName] = React.useState("")
  const [draftSet, draftSetHooks] = useDraftSet([])
  const [__, reqCreate] = useCreate()

  const [show, setShow] = React.useState(false)

  return (
    <>
      <Container>
        <h1 className="p-3">カードプール作成</h1>
        <Form>

          <CardpoolForm props={{name, setName, draftSet, draftSetHooks}} />

          <Button onClick={() => {
            reqCreate({
              Name: name,
              Set: draftSet,
              UserKey: auth.userKey
            }).then(() => {
              location.href = "/cardpool/top"
            }).catch(() => {
              setShow(true)
            })
          }}>作成</Button>
        </Form>
      </Container>

      <Toast show={show} onClose={() => {setShow(false)}}>
        <Toast.Header>
          <strong className="me-auto">カードプールを作成できませんでした</strong>
        </Toast.Header>
      </Toast>
    </>
  )
}
