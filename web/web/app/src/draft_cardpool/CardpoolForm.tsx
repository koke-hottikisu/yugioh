import * as React from 'react'
import { Button, Container, Form, Toast } from 'react-bootstrap'

import { DraftSet, DraftSetHooks } from './draftset/useDraftSet'
import { DraftSetForm } from './draftset/DraftSetForm'

interface Props {
  name: string
  setName: (arg: string) => void
  draftSet: DraftSet[]
  draftSetHooks: DraftSetHooks
}

export const CardpoolForm = (props: {props: Props}) => {
  const {name, setName, draftSet, draftSetHooks} = props.props

  return (
    <>
      <Form.Group>
        <Form.Label>名前</Form.Label>
        <Form.Control type="text" placeholder="名前"
                      value={name} onChange={(e) => {setName(e.target.value)}} />
      </Form.Group>

      <DraftSetForm draftSet={draftSet} draftSetHooks={draftSetHooks} />
    </>
  )
}
