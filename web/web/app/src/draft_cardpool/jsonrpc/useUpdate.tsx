import { useJsonrpc } from '../../useJsonrpc'
import { DraftSet } from '../draftset/useDraftSet'

interface UpdateArg {
	ID: string
	Name: string
	Set: DraftSet[]
	UserKey: string
}
interface UpdateRes {}

export const useUpdate = () => {
  return useJsonrpc<UpdateArg, UpdateRes>("/api/draft/cardpool", "CardpoolRPC.Update")
}
