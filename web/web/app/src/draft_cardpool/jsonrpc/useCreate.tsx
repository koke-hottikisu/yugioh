import { useJsonrpc } from '../../useJsonrpc'
import { DraftSet } from '../DraftSet'

interface CreateArg {
  Name: string
  Set: DraftSet[]
  UserKey: string
}

export interface Cardpool {
	ID: string
	Name: string
	UserID: string
	Set: DraftSet[]
	History: DraftSet[][]
}
type CreateRes = Cardpool

export const useCreate = () => {
  return useJsonrpc<CreateArg, CreateRes>("/api/draft/cardpool", "CardpoolRPC.Create")
}
