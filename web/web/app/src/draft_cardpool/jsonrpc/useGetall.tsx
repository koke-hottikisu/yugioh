import { useJsonrpc, ReqOptions } from '../../useJsonrpc'
import { Cardpool } from './useCreate'

interface GetallArg {
}
interface GetallRes {
  Cardpools: Cardpool[]
}

export const useGetall = () => {
  const options: ReqOptions<GetallArg> = {
    manual: false,
    param: {}
  }

  return useJsonrpc<GetallArg, GetallRes>("/api/draft/cardpool", "CardpoolRPC.Getall", options)
}
