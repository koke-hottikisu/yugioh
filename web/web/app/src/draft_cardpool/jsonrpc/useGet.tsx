import { useJsonrpc, ReqOptions } from '../../useJsonrpc'
import { Cardpool } from './useCreate'

interface GetArg {
  ID: string
}
type GetRes = Cardpool

export const useGet = (id: string) => {
  const options: ReqOptions<GetArg> = {
    manual: false,
    param: {ID: id}
  }
  return useJsonrpc<GetArg, GetRes>("/api/draft/cardpool", "CardpoolRPC.Get", options)
}
