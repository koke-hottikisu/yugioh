import { useJsonrpc } from '../../useJsonrpc'

interface DeleteArg {
  ID: string
  UserKey: string
}
interface DeleteRes {}

export const useDelete = () => {
  return useJsonrpc<DeleteArg, DeleteRes>("/api/draft/cardpool", "CardpoolRPC.Delete")
}
