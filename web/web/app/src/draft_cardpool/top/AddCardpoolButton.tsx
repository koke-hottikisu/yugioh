import * as React from 'react'
import { Button, OverlayTrigger, Tooltip } from 'react-bootstrap'

import { AuthContext } from '../../context'

export const AddCardpoolButton = () => {
  const [auth, _] = React.useContext(AuthContext)

  return (
    <>
      <OverlayTrigger
        placement="right"
        overlay={
          <Tooltip id="tooltip-disabled" hidden={auth.exists}>
            ログインしてください
          </Tooltip>
        }>
        <span className="d-inline-block">
          <Button disabled={!auth.exists}
                  onClick={()=>{location.href="/cardpool/add" }}>
            カードプール作成
          </Button>
        </span>
      </OverlayTrigger>
    </>
  )
}
