import * as React from 'react'
import { Col, Container, Row } from 'react-bootstrap'

import { useGetall } from '../jsonrpc/useGetall'
import { AddCardpoolButton } from './AddCardpoolButton'
import { CardpoolThumbnails } from './CardpoolThumbnails'

export const CardpoolPage = () => {
  const [respGetall, reqGetall] = useGetall()

  React.useEffect(() => {
    reqGetall({})
  }, [])

  if (respGetall.loading) {
    return <p>loading</p>
  }
  if (respGetall.error) {
    return <p>error</p>
  }

  return (
    <>
      <Container>
        <Row className="p-2">
          <Col>
            <AddCardpoolButton/>
          </Col>
        </Row>
        <Row>
          {respGetall.data.Cardpools.map((cardpool, index) => (
            <Col key={index} className="p-1" xs={6} sm={3}>
              <CardpoolThumbnails cardpool={cardpool} />
            </Col>
          ))}
        </Row>
      </Container>
    </>
  )
}
