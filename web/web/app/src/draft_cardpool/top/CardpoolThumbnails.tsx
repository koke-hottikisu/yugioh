import React, { useEffect, useState } from 'react'
import { Card, Image } from 'react-bootstrap'
import { Cardpool } from '../jsonrpc/useCreate'
import { useUserInfo } from '../../twitter_login/useUserInfo'
import { ImageUrl } from '../Image'

interface Props {
  cardpool: Cardpool
}

export const CardpoolThumbnails = (props: Props) => {
  const userInfo = useUserInfo(props.cardpool.UserID)

  const aStyle = {color: 'inherit', textDecoration: 'inherit'}
  return (
    <a href={"/cardpool/detail/" + props.cardpool.ID} style={aStyle}>
      <Card>
        <Card.Img alt="icon" src={ImageUrl()} className="p-2" />
        <Card.Body>
          <Card.Text>
            <div className="text-white"
                 style={{overflow: 'hidden', textOverflow: 'ellipsis', whiteSpace: 'nowrap'}}>
              {props.cardpool.Name}
            </div>

            <div>
              <Image src={userInfo.profile_image_url} roundedCircle />
              <div>{userInfo.name}</div>
            </div>
          </Card.Text>
        </Card.Body>
      </Card>
    </a>    
  )
}
