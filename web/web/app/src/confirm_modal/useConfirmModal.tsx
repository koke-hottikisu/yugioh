import * as React from 'react'

export type ConfirmModalHooks = {
  showing: boolean
  show: () => void
  hide: () => void
}

export const useConfirmModal = (): ConfirmModalHooks => {
  const [showing, setShowing] = React.useState(false)

  const show = () => {
    setShowing(true)
  }

  const hide = () => {
    setShowing(false)
  }

  return { showing, show, hide }
}
