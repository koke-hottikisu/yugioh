import * as React from 'react'

import { Button, Modal } from 'react-bootstrap'

import { ConfirmModalHooks } from './useConfirmModal'

type Props = {
  title: string
  confirmModalHooks: ConfirmModalHooks
  onConfirm: () => void
  onCancel: () => void
}

export const ConfirmModal: React.FC<Props> = (props) => {
  return (
    <Modal show={props.confirmModalHooks.showing} onHide={props.confirmModalHooks.hide}>
      <Modal.Header closeButton>{props.title}</Modal.Header>
      <Modal.Body>{props.children}</Modal.Body>
      <Modal.Footer>
        <Button variant="secondary" onClick={() => {
          props.onCancel()
          props.confirmModalHooks.hide()
        }}>
          Cancel
        </Button>
        <Button variant="primary" onClick={() => {
          props.onConfirm()
          props.confirmModalHooks.hide()
        }}>
          Confirm
        </Button>
      </Modal.Footer>
    </Modal>
  )
}
