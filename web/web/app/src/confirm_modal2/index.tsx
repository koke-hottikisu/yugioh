import * as React from 'react'

import { Button, ButtonProps, Modal } from 'react-bootstrap'

type Props = {
  buttonProps: ButtonProps
  buttonElem: React.ReactChild

  modalTitle: string

  onConfirm: () => void
}

export const ConfirmModal: React.FC<Props> = (props) => {
  const [show, setShow] = React.useState(false)

  return (
    <>
      <Button {...props.buttonProps}
              onClick={() => {
                setShow(true)
              }}>
        {props.buttonElem}
      </Button>

      <Modal show={show} onHide={() => setShow(false)}>
        <Modal.Header closeButton>{props.modalTitle}</Modal.Header>
        <Modal.Footer>
          <Button variant="secondary" onClick={() => {
            setShow(false)
          }}>
            Cancel
          </Button>
          <Button variant="primary" onClick={() => {
            setShow(false)
            props.onConfirm()
          }}>
            Confirm
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  )
}
