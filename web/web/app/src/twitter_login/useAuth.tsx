import * as React from 'react'
import axios from 'axios'

export interface Auth {
  exists: boolean
  userKey: string
  userId: string
}

export interface AuthHooks {
  update: () => void
  logout: () => void
}

export const useAuth = (): [Auth, AuthHooks] => {
  const [auth, setAuth] = React.useState<Auth>({
    exists: false,
    userKey: "",
    userId: "",
  })

  const update = () => {
    axios.get('/api/auth/get_userKey').then((res) => {
      const auth: Auth = {
	      exists: res.data['exists'],
	      userKey: res.data['userKey'],
        userId: res.data['userId'],
      }
      setAuth(auth)
    }).catch(() => {
      setAuth({...auth, exists: false})
    })
  }

  const logout = () => {
    axios.get('/api/auth/logout').then(() => {
      setAuth({...auth, exists: false})
      location.reload()
    })
  }

  return [auth, {update, logout}]
}
