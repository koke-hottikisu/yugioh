import * as React from 'react'
import axios from 'axios'

export interface UserInfo {
  id: string
  name: string
  username: string
  url: string
  profile_image_url: string
}

export const useUserInfo = (id: string): UserInfo => {
  const initUserInfo: UserInfo = {
    id: "",
    name: "",
    username: "",
    url: "",
    profile_image_url: "",
  }
  const [userInfo, setUserInfo] = React.useState<UserInfo>(initUserInfo)

  React.useEffect(() => {
    var url = '/api/auth/get_userInfo?userId=' + id + '&user.fields=url,profile_image_url'
    axios.get(url).then((res) => {
      setUserInfo(res.data.data)
    }).catch(() => {
      setUserInfo(initUserInfo)
    })
  }, [id])

  return userInfo
}
