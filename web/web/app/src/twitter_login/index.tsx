import * as React from 'react'
import { Image, Dropdown } from 'react-bootstrap'
import { AuthContext } from '../context'
import { useUserInfo } from './useUserInfo'

export const TwitterLogin = () => {
  const [auth, authHooks] = React.useContext(AuthContext)
  const userInfo = useUserInfo(auth.userId)

  React.useEffect(() => {
    authHooks.update()
  }, [])

  return (
    <>
    {!auth.exists && <a href="/api/auth/twitter_login">twitterでログイン</a>}
    {auth.exists &&
     <Dropdown className="p-0">
       <Dropdown.Toggle className="p-0 bg-transparent border-0">
         <Image src={userInfo.profile_image_url} roundedCircle />
       </Dropdown.Toggle>

       <Dropdown.Menu>
         <Dropdown.Item disabled={true} className="border-bottom border-3">
           <div className="small">@{userInfo.username}</div>
           <div className="text-white">{userInfo.name}</div>
         </Dropdown.Item>
         <Dropdown.Item onClick={authHooks.logout}>ログアウト</Dropdown.Item>
       </Dropdown.Menu>
     </Dropdown>
    }
    </>
  )
}
