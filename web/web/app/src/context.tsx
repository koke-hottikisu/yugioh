import * as React from 'react'

import { Auth, AuthHooks, useAuth } from './twitter_login/useAuth'

export const AuthContext = React.createContext<[Auth, AuthHooks]>(undefined)

export const Context = (props) => {
  return (
    <AuthContext.Provider value={useAuth()}>
      {props.children}
    </AuthContext.Provider>
  )
}
