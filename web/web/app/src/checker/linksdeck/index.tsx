import * as React from 'react'

import { Alert, Button, Col, Container, Form, Row, Spinner } from 'react-bootstrap'
import ReactCrop, { Crop } from 'react-image-crop'
import 'react-image-crop/dist/ReactCrop.css'
import { makeCroppedImg } from './crop'
import { useDeckToCards } from '../useDeckToCards'
import { CardsDom } from '../cards'

export const LinksDeckPage = () => {
  React.useEffect(() => {
    document.title = "02環境デッキチェッカー | slimemoss"
  }, []);

  const [selectFile, setSelectFile] = React.useState<string>("")
  const [selectFileImg, setSelectFileImg] = React.useState<HTMLImageElement>()
  const [crop, setCrop] = React.useState<Crop>()

  const {cardNames, setCardNames, whileDeckToCards, deckToCards} = useDeckToCards()

  return (
    <>
      <Container>
        <h1 className="p-3">マスターデュエル　02環境　デッキチェッカー</h1>

        <Alert variant="danger">誤検出することがあります。参考程度にご利用ください。</Alert>

        <Form>
          <Form.Group>
            <Form.Label>デッキのスクショをアップロード</Form.Label>
            <Form.Control type="file"
                          accept="image/*" 
                          onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
                            setCardNames([])
                            window.URL.revokeObjectURL(selectFile)
                            setSelectFile(window.URL.createObjectURL(e.target.files[0]))
                          }}/>
          </Form.Group>
        </Form>

        <Row>
          <Col>
            <div style={{fontSize: '0.9em'}}>
              <CardsDom initNames={cardNames}/>
            </div>
            <Spinner className="m-3" animation="border" role="status"
                     hidden={!whileDeckToCards}/>
          </Col> 

          <Col xs="12" sm>
            <Button className="m-1" disabled={selectFile == ""}
                    onClick={() => {
                      setCardNames([])
                      deckToCards(makeCroppedImg(selectFileImg, crop))
                    }}>
              チェック</Button>
            <div>
              <ReactCrop src={selectFile}
                         onChange={c => { setCrop(c) }}
                         crop={crop}
                         onImageLoaded = {image => {
                           setSelectFileImg(image)
                           setCrop({
                             x: image.width * 0.1, y: image.height * 0.1,
                             width: image.width * 0.8, height: image.height * 0.8,
                             unit: 'px',
                           })
                           return false
                         }}/>
            </div>
          </Col>
        </Row>
      </Container>
    </>
  )
}
