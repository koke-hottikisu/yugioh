import { Crop } from 'react-image-crop'

export const makeCroppedImg = (image: HTMLImageElement, crop: Crop) => {
  const canvas = document.createElement('canvas');
  const ctx = canvas.getContext('2d');

  if (crop.width == 0 || crop.height == 0) {
    canvas.width  = image.naturalWidth
    canvas.height = image.naturalHeight
    ctx.drawImage(image, 0, 0)
  } else {
    const pixelRatio = window.devicePixelRatio;
    const scaleX = image.naturalWidth / image.width;
    const scaleY = image.naturalHeight / image.height;

    canvas.width = crop.width * pixelRatio * scaleX;
    canvas.height = crop.height * pixelRatio * scaleY;

    ctx.setTransform(pixelRatio, 0, 0, pixelRatio, 0, 0);
    ctx.imageSmoothingQuality = 'high';

    ctx.drawImage(
      image,
      crop.x * scaleX,
      crop.y * scaleY,
      crop.width * scaleX,
      crop.height * scaleY,
      0,
      0,
      crop.width * scaleX,
      crop.height * scaleY
    )
  }

  const data = canvas.toDataURL("image/png")
  return data
}

