import * as React from 'react'

import { useCards } from './useCards'
import { CardDom } from '../CardDom'
import { Col, Row } from 'react-bootstrap'
import { CopyClip } from './CopyClip'

interface Props {
  initNames: string[]
}

export const CardsDom = (props: Props) => {
  const [cards, cardsHooks] = useCards(props.initNames)

  return (
    <>
      <div className="m-2" style={{display: 'flex', alignItems: 'center'}}>
        <CopyClip names={cards.map(c => c.name)} />
      </div>

      <Row>
        <Col xs="6">
          {cards.slice(0, cards.length / 2).map((card, index) => (
            <div key={index} style={{display: 'flex', alignItems: 'center'}}>
              <div style={{width: '2em'}}>#{index + 1}</div>
              <CardDom name={card.name}/>
            </div>
          ))}
        </Col>
        <Col xs="6">
          {cards.slice(cards.length / 2).map((card, index) => (
            <div key={index} style={{display: 'flex', alignItems: 'center'}}>
              <div style={{width: '2em'}}>#{Math.floor(cards.length / 2) + index + 1}</div>
              <CardDom name={card.name}/>
            </div>
          ))}
        </Col>
      </Row>

    </>
  )
}
