import * as React from 'react'

import { HiOutlineClipboardCopy } from 'react-icons/hi'
import { CopyToClipboard } from 'react-copy-to-clipboard'
import { Tooltip, Overlay } from 'react-bootstrap'

interface Props {
  names: string[]
}
export const CopyClip = (props: Props) => {
  const [showCopyed, setShowCopyed] = React.useState(false)
  const showCopyedTarget = React.useRef(null);

  React.useEffect(() => {
    const f = async () => {
      await new Promise(r => setTimeout(r, 1000));
      setShowCopyed(false)
    }
    f()
  }, [showCopyed])

  return (
    <div>
      <CopyToClipboard text={props.names.reduce(
        (prev, curr, index) => (prev + (index == 0 ? "" : "\n") + curr),
        "")}
                       onCopy={() => {setShowCopyed(true)}}>
        <span className="m-1" ref={showCopyedTarget}>
          結果をクリップボードにコピー<HiOutlineClipboardCopy/>
        </span>
      </CopyToClipboard>
      <Overlay target={showCopyedTarget.current} show={showCopyed} placement="right">
        <Tooltip hidden={!showCopyed}>
          コピーしました!
        </Tooltip>
      </Overlay>
    </div>
  )
}
