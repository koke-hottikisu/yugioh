import React from "react"

export interface Card {
  id: number
  name: string
}

export type DispatchCard = React.Dispatch<((prev: Card) => Card)>

interface Props {
  card: Card
  setCard: DispatchCard
}

interface Hooks {
  setCard: DispatchCard
}

export const useCard = (props: Props): [Card, Hooks] => {
  const {card, setCard} = props

  return [card, {setCard}]
}
