import * as React from 'react'
import { Badge } from 'react-bootstrap'

import { Card, DispatchCard, useCard } from './useCard'
import { is02, useYugiohCard } from '../../../card_info/useYugiohCard'

interface Props {
  card: Card
  setCard: DispatchCard
  deleteCard: () => void
}

export const CardDom = (props: Props) => {
  const [card, cardHooks] = useCard(props)
  const cardInfo = useYugiohCard(card.name)

  return (
    <>
      <div style={{display: 'flex', alignItems: 'center'}}>
        <div className="mx-1">
          <Badge bg="success" hidden={!is02(cardInfo.releasedate, cardInfo.name)}>02</Badge>
          <Badge bg="danger" hidden={is02(cardInfo.releasedate, cardInfo.name)}>02</Badge>
        </div>
        <a href={cardInfo.url} target="_blank"> {card.name} </a>
      </div>
    </>
  )
}
