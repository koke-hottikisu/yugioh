import * as React from 'react'

import { Card, DispatchCard } from './card/useCard'

interface Hooks {
  getCardDispatch: (id: number) => DispatchCard
  getDeleteCard: (id: number) => (() => void)
  addCard: () => void
}

export const useCards = (initNames: string[]): [Card[], Hooks] => {
  const [cards, setCards] = React.useState<Card[]>([])

  React.useEffect(() => {
    setCards(initNames.map((name, index) => ({
      name: name,
      id: index,
    })))
  }, [JSON.stringify(initNames)])

  const getCardDispatch = (id: number) => {
    return (action: (prev: Card) => Card) => {
      setCards((prevCards) => (
        prevCards.map((card, _) => (
          card.id != id ? card : action(card)
        ))
      ))
    }
  }

  const addCard = () => {
    const newCard: Card = {
      name: '新しいカード',
      id: cards.reduce((prev, cur) => (prev > cur.id ? prev : cur.id), 0) + 1,
    }
    setCards((prev) => ([
      ...prev,
      newCard
    ]))
  }

  const getDeleteCard = (id: number) => {
    return () => {
      setCards((prev) => (
        prev.filter((card) => (id != card.id))
      ))
    }
  }

  return [cards, {getCardDispatch, addCard, getDeleteCard}]
}
