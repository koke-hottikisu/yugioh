import * as React from 'react'
import { Container, Button, InputGroup, FormControl } from 'react-bootstrap'

import { useCardnameComplete } from '../../common/useCardnameComplete'
import { CardDom } from '../CardDom'

export const CardNamePage = () => {
  const [name, setName] = React.useState<string>("")
  const candidateCard = useCardnameComplete(name)

  React.useEffect(() => {
    document.title = "02環境 / 4期かどうかをいい感じに検索 | slimemoss"
    document.title = "02環境かどうかをいい感じに検索 | slimemoss"
  }, []);

  return (
    <Container>
      <InputGroup className="py-3">
        <FormControl type="text" value={name}
                     placeholder="カード名を入力してください"
                     onChange={(e) => {setName(e.target.value)} } 
                     aria-describedby="basic-addon2" />
        <Button variant="outline-secondary" id="button-addon2"
                onClick={() => {setName("")}}>
          x
        </Button>
      </InputGroup>

      <div>
        {candidateCard.map((e, i) => (
          <div key={i} className="m-1">
            <CardDom name={e} />
          </div>
        ))}
      </div>

    </Container>
  )
}
