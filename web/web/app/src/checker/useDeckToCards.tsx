import * as React from 'react'
import axios from 'axios'

export interface DeckToCardsHooks {
  cardNames: string[]
  setCardNames: (names: string[]) => void
  whileDeckToCards: boolean
  deckToCards: (imageUrl: string) => void
}

export const useDeckToCards = (): DeckToCardsHooks => {
  const [cardNames, setCardNames] = React.useState<string[]>([])
  const [whileDeckToCards, setWhileDeckToCards] = React.useState(false)

  const deckToCards = (imageUrl: string) => {
    setWhileDeckToCards(true)
    axios.post('/api/links-deck-parse/', { q: imageUrl }, {
      headers: {
        'Content-Type': 'application/json'
      }
    }).then((res) => {
      setCardNames(res.data)
    }).catch(() => {
      setCardNames([])
    }).finally(() => {
      setWhileDeckToCards(false)
    })
  }

  return { cardNames, setCardNames ,whileDeckToCards, deckToCards }
}
