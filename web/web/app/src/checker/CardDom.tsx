import * as React from 'react'
import { Badge } from 'react-bootstrap'

import { useYugiohCard, is02, is04 } from '../card_info/useYugiohCard'
import { limit02 } from '../card_info/env02'

interface Props {
  name: string
}

const badgeText02 = (name: string): string => {
  if (limit02(name) == 1) {
    return "制限"
  }
  if (limit02(name) == 2) {
    return "準制限"
  }
  return "02"
}

const badgeBg02 = (date: string, name: string): string => {
  if (is02(date, name)) {
    return "success"
  }
  return "danger"
}

export const CardDom = (props: Props) => {
  const cardInfo = useYugiohCard(props.name)

  return (
    <div style={{display: 'flex', alignItems: 'center'}}>
      <Badge style={{width: '4.0em'}} className="p-1" bg={badgeBg02(cardInfo.releasedate, cardInfo.name)}>
        {badgeText02(cardInfo.name)}
      </Badge>

      <div hidden={true}>
        <Badge bg="primary" hidden={!is04(cardInfo.releasedate)}>4期</Badge>
        <Badge bg="danger" hidden={is04(cardInfo.releasedate)}>4期</Badge>
      </div>

      <div className="mx-1">
        <a href={cardInfo.url} target="_blank">{props.name}</a>
      </div>
    </div>
  )
}
