package handler

import (
	"errors"
	"main/auth"
	"main/cardpool"
	"main/db"
	"main/draft"
	"net/http"

	"github.com/gorilla/rpc/v2"
	"github.com/gorilla/rpc/v2/json2"
)

type CardpoolRPC struct {}

type CreateCardpoolArg struct {
	Name string
	Set []draft.Card
	UserKey string
}
type CreateCardpoolRes cardpool.CardPool
func (rpc *CardpoolRPC) Create(r *http.Request,
	args *CreateCardpoolArg,
	result *CreateCardpoolRes) error {

	// check auth
	ok, userId, err := auth.GetUserId(args.UserKey)
	if err != nil {	return err }
	if !ok { return errors.New("user not exists") }

	id := db.GenID()
	err = db.UpsertCardpool(cardpool.CardPool{
		ID:      id,
		Name:    args.Name,
		UserID:  userId,
		Set:     args.Set,
		History: [][]draft.Card{},
	})
	if err != nil {	return err }

	targetCardpool, err := db.FindCardpool(id)
	if err != nil {	return err }

	*result = CreateCardpoolRes(targetCardpool)
	return nil
}

type UpdateArg struct {
	ID string
	Name string
	Set []draft.Card
	UserKey string
}
type UpdateRes struct {}
func (rpc *CardpoolRPC) Update(r *http.Request,
	args *UpdateArg,
	result *UpdateRes) error {

	targetCardpool, err := db.FindCardpool(args.ID)

	// check auth
	ok, userId, err := auth.GetUserId(args.UserKey)
	if err != nil {	return err }
	if !ok { return errors.New("user not exists") }
	if targetCardpool.UserID != userId { return errors.New("not permitted") }

	targetCardpool.Name = args.Name
	targetCardpool.UpdateSet(args.Set)

	err = db.UpsertCardpool(targetCardpool)
	if err != nil {	return err }
	
	return nil
}

type GetallArg struct {}
type GetallRes struct {
	Cardpools []cardpool.CardPool
}
func (rpc *CardpoolRPC) Getall(r *http.Request,
	args *GetallArg,
  result *GetallRes) error {

	pools, err := db.FindAllCardpool()
	if err != nil {	return err }

	*result = GetallRes{
		Cardpools: pools,
	}

	return nil
}

type GetArg struct {
	ID string
}
type GetRes cardpool.CardPool
func (rpc *CardpoolRPC) Get(r *http.Request,
	args *GetArg,
  result *GetRes) error {

	pool, err := db.FindCardpool(args.ID)
	if err != nil {	return err }

	*result = GetRes(pool)

	return nil
}

type DeleteArg struct {
	ID string
	UserKey string
}
type DeleteRes struct {}
func (rpc *CardpoolRPC) Delete(r *http.Request,
	args *DeleteArg,
  result *DeleteRes) error {

	targetCardpool, err := db.FindCardpool(args.ID)

	ok, userId, err := auth.GetUserId(args.UserKey)
	if err != nil {	return err }
	if !ok { return errors.New("user not exists") }
	if targetCardpool.UserID != userId { return errors.New("not permitted") }
	
	err = db.DeleteCardpool(args.ID)
	if err != nil {	return err }

	return nil
}

func CardpoolHandler() (http.Handler){
	s := rpc.NewServer()
	s.RegisterCodec(json2.NewCodec(), "application/json")
	s.RegisterService(&CardpoolRPC{}, "")

	return NewServer(s)
}
