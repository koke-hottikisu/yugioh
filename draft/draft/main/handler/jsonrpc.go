package handler

import (
	"bytes"
	"io"
	"io/ioutil"
	"log"
	"net/http"
)

type Server struct {
	inner http.Handler
}

type rwWrapper struct {
	rw http.ResponseWriter
	mw io.Writer
}

func NewRwWrapper(rw http.ResponseWriter, buf io.Writer) *rwWrapper {
	return &rwWrapper{
		rw: rw,
		mw: io.MultiWriter(rw, buf),
	}
}

func (r *rwWrapper) Header() http.Header {
	return r.rw.Header()
}

func (r *rwWrapper) Write(i []byte) (int, error) {
	return r.mw.Write(i)
}

func (r *rwWrapper) WriteHeader(statusCode int) {
	r.rw.WriteHeader(statusCode)
}

func NewServer(next http.Handler) http.Handler {
	return http.HandlerFunc(func(rw http.ResponseWriter, r *http.Request) {
		body, err := ioutil.ReadAll(r.Body)
		if err == nil {
			log.Printf("[req] %s", body)
			r.Body = ioutil.NopCloser(bytes.NewBuffer(body))
		}
		
		buf := &bytes.Buffer{}
		rww := NewRwWrapper(rw, buf)
		next.ServeHTTP(rww, r)
		log.Printf("[res] %s", buf)
	})
}
