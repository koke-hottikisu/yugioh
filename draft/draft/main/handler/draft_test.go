package handler

import (
	"bytes"
	"encoding/json"
	"log"
	"main/draft"
	"net/http/httptest"
	"testing"

	"github.com/ybbus/jsonrpc/v2"
)

func TestAll(t *testing.T) {
	ts := httptest.NewServer(DraftHandler())
	rpcClient := jsonrpc.NewClient(ts.URL)

	resp, err := rpcClient.Call("DraftRPC.Create", &CreateArg{
		PlayerName: "ownername",
		Card: []draft.Card{
			{Name: "c01", EncloseNum: 1},
			{Name: "c02", EncloseNum: 1},
			{Name: "c03", EncloseNum: 1},
			{Name: "c04", EncloseNum: 1},
			{Name: "c05", EncloseNum: 1},
			{Name: "c06", EncloseNum: 1},
			{Name: "c07", EncloseNum: 1},
			{Name: "c08", EncloseNum: 1},
			{Name: "c09", EncloseNum: 1},
			{Name: "c10", EncloseNum: 1},
			{Name: "c11", EncloseNum: 1},
			{Name: "c12", EncloseNum: 1},
			{Name: "c13", EncloseNum: 1},
			{Name: "c14", EncloseNum: 1},
			{Name: "c15", EncloseNum: 1},
			{Name: "c16", EncloseNum: 1},
		},
	})
	if err != nil {t.Fatal(err)}

	var res CreateRes
	err = resp.GetObject(&res)
	if err != nil {t.Fatal(err)}
	draft := res.DraftID
	owner := res.PlayerID

	var resj JoinRes
	resp, err = rpcClient.Call("DraftRPC.Join", &JoinArg{
		DraftID: draft,
	})
	if err != nil {t.Fatal(err)}
	err = resp.GetObject(&resj)
	if err != nil {t.Fatal(err)}
	player := resj.PlayerID

	d, _ := getState(draft, owner, rpcClient)
	log.Println(draftString(d))
	
	resp, err = rpcClient.Call("DraftRPC.Start", &StartArg{
		PlayerID: owner,
		DraftID:  draft,
	})
	if err != nil {t.Fatal(err)}

	var resstart StartRes
	err = resp.GetObject(&resstart)
	if err != nil {t.Fatal(err)}


	for {
		log.Println("** wait start **")
		d, _ = getState(draft, owner, rpcClient)
		log.Println(draftString(d))
		if d.IsStarted {
			break
		}
	}

	for range(make([]int, 45)) {
		log.Println("** player ***")
		d, _ := getState(draft, player, rpcClient)
		log.Println(draftString(d))
		resp, err = rpcClient.Call("DraftRPC.Pick", &PickArg{
			PlayerID: player,
			DraftID:  draft,
			Card: d.Me.PickPack[0],
		})
		d, _ = getState(draft, player, rpcClient)
		log.Println(draftString(d))

		log.Println("** owner ***")
		d, _ = getState(draft, owner, rpcClient)
		log.Println(draftString(d))
		resp, err = rpcClient.Call("DraftRPC.Pick", &PickArg{
			PlayerID: owner,
			DraftID:  draft,
			Card: d.Me.PickPack[0],
		})
		d, _ = getState(draft, owner, rpcClient)
		log.Println(draftString(d))
	}

	log.Println("*************************************************************************************")
	d, _ = getState(draft, owner, rpcClient)
	log.Println(draftString(d))

	if !d.IsEnd {
		t.Fatal("should end")
	}
}

func getState(draftID string, ownerID string, rpcClient jsonrpc.RPCClient) (StateRes, error) {
	resp, err := rpcClient.Call("DraftRPC.State", &StateArg{
		DraftID:  draftID,
		PlayerID: ownerID,
	})
	if err != nil {
		return StateRes{}, err
	}

	var ress StateRes
	err = resp.GetObject(&ress)
	if err != nil {
		return StateRes{}, err
	}
	return ress, nil
}

func draftString(s StateRes) string {
	db, _ := json.Marshal(s)
	var buf bytes.Buffer
	json.Indent(&buf, db, "", "  ")
	return buf.String()
}
