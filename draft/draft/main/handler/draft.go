package handler

import (
	"errors"
	"fmt"
	"main/db"
	"main/draft"
	"net/http"
	"time"

	"github.com/gorilla/rpc/v2"
	"github.com/gorilla/rpc/v2/json2"
)

type DraftRPC struct {}

type StateArg struct{DraftID string; PlayerID string}
type StateRes struct{
	IsStarted bool
	IsEnd bool
	OwnerName string
	OwnerID string
	Me struct {
		Name string
		PickPack []string
		OwnPackNum int
		PickedCard []string
		PickDeadline time.Time
	}
	AllPlayer []struct {
		Name string
		ID string
		OwnPackNum int
	}
}
func createStateRes(targetDraft draft.Draft, playerID string) (StateRes) {
	owner, _ := targetDraft.GetPlayer(targetDraft.Owner)
	me, _ := targetDraft.GetPlayer(playerID)

	otherPlayer := []struct{
		Name string
		ID string
		OwnPackNum int
	}{}
	for _, p := range(targetDraft.Player) {
		otherPlayer = append(otherPlayer, struct{Name string; ID string; OwnPackNum int}{
			Name: p.Name,
			ID: p.ID,
			OwnPackNum:len(p.OwnPack),
		})
	}

	var pickpack []string
	if len(me.OwnPack) != 0 {
		for _, card := range me.OwnPack[0].Card {
			pickpack = append(pickpack, card.Name)
		}
	} else {
		pickpack = []string{}
	}
	
	return StateRes{
		IsStarted: targetDraft.IsStarted,
		IsEnd:     targetDraft.IsEnd,
		OwnerName: owner.Name,
		OwnerID: owner.ID,
		Me: struct {
			Name         string
			PickPack     []string
			OwnPackNum   int
			PickedCard   []string
			PickDeadline time.Time
		}{
			Name:         me.Name,
			PickPack:     pickpack,
			OwnPackNum:   len(me.OwnPack),
			PickedCard:   me.PickCard,
			PickDeadline: me.PickDeadline,
		},
		AllPlayer: otherPlayer,
	}
}

func (d *DraftRPC) State(r *http.Request,
	args *StateArg,
	result *StateRes) error {

	targetDraft, err := db.FindDraft(args.DraftID)
	if err != nil {
		return errors.New("can not found draft")
	}

	targetDraft.AutoPick()
	targetDraft.PickDeadline()

	err = db.UpsertDraft(targetDraft)
	if err != nil {	return err }

	*result = createStateRes(targetDraft, args.PlayerID)

	return nil
}

type CreateArg struct{
	PlayerName string
	Card []draft.Card
	Rotation int
	CardNumInPack int
}
type CreateRes struct{DraftID string; PlayerID string}
func (d *DraftRPC) Create(r *http.Request,
	args *CreateArg,
	result *CreateRes) error {

	rule := draft.DraftRule{
		Rotation:      args.Rotation,
		CardNumInPack: args.CardNumInPack,
		Set:           args.Card,
	}
	if rule.Rotation == 0 { rule.Rotation = 3 }
	if rule.CardNumInPack == 0 { rule.CardNumInPack = 15 }
	targetdraft, playerID := draft.CreateDraft(rule, args.PlayerName)

	err := db.UpsertDraft(targetdraft)
	if err != nil {	return err }

	result.DraftID = targetdraft.ID
	result.PlayerID = playerID

	return nil
}

type CreateSinglePlayArg struct{
	Card []draft.Card
}
type CreateSinglePlayRes struct{DraftID string; PlayerID string}
func (d *DraftRPC) CreateSinglePlay(r *http.Request,
	args *CreateSinglePlayArg,
	result *CreateSinglePlayRes) error {

	targetDraft, playerID := draft.CreateDraft(draft.DraftRule{
		Rotation:      3,
		CardNumInPack: 15,
		Set:           args.Card,
	}, "You")

	for i := 0; i < 7; i++ {
		targetDraft.Join(fmt.Sprintf("player%d", i), true)
	}
	ok := targetDraft.Start(playerID)
	if !ok { return errors.New("can not start draft") }

	err := db.UpsertDraft(targetDraft)
	if err != nil {	return err }

	*result = CreateSinglePlayRes{
		DraftID:  targetDraft.ID,
		PlayerID: playerID,
	}
	return nil
}

type StartArg struct{PlayerID string; DraftID string}
type StartRes struct{}
func (d *DraftRPC) Start(r *http.Request,
	args *StartArg,
	result *StartRes) error {

	targetdraft, err := db.FindDraft(args.DraftID)
	if err != nil {	return err }

	ok := targetdraft.Start(args.PlayerID)
	if !ok { return errors.New("can not start draft") }

	err = db.UpsertDraft(targetdraft)
	if err != nil { return err }

	*result = StartRes{}
	return nil
}

type JoinArg struct{
	DraftID string
	PlayerName string
}
type JoinRes struct{DraftID string; PlayerID string}
func (d *DraftRPC) Join(r *http.Request,
	args *JoinArg,
	result *JoinRes) error {

	targetdraft, err := db.FindDraft(args.DraftID)
	if err != nil { return err }

	if targetdraft.IsStarted { return errors.New("draft have already started") }
	playerID := targetdraft.Join(args.PlayerName, false)

	err = db.UpsertDraft(targetdraft)
	if err != nil { return err }

	*result = JoinRes{
		DraftID:  args.DraftID,
		PlayerID: playerID,
	}
	return nil
}

type PickArg struct{PlayerID string; DraftID string; Card string}
type PickRes struct{}
func (d *DraftRPC) Pick(r *http.Request,
	args *PickArg,
	result *PickRes) error {

	targetdraft, err := db.FindDraft(args.DraftID)
	if err != nil { return err }

	ok := targetdraft.Pick(args.PlayerID, args.Card)
	if !ok { return errors.New("can not pick") }

	err = db.UpsertDraft(targetdraft)
	if err != nil { return err }

	return nil
}

type ExitArg struct{PlayerID string; DraftID string}
type ExitRes struct{}
func (d *DraftRPC) Exit(r *http.Request,
	args *ExitArg,
	result *ExitRes) error {

	targetdraft, err := db.FindDraft(args.DraftID)
	if err != nil { return err }

	if targetdraft.IsStarted {
		return errors.New("draft already started")
	}
	if args.PlayerID == targetdraft.Owner {
		return errors.New("owner can not exit, use ForceEnd")
	}
	targetdraft.Exit(args.PlayerID)

	err = db.UpsertDraft(targetdraft)
	if err != nil { return err }

	*result = ExitRes{}
	return nil
}

type ForceEndArg struct{PlayerID string; DraftID string}
type ForceEndRes struct{}
func (d *DraftRPC) ForceEnd(r *http.Request,
	args *ForceEndArg,
	result *ForceEndRes) error {

	targetdraft, err := db.FindDraft(args.DraftID)
	if err != nil { return err }

	if args.PlayerID != targetdraft.Owner {
		return errors.New("owner is only force end.")
	}
	targetdraft.IsStarted = true
	targetdraft.IsEnd = true

	err = db.UpsertDraft(targetdraft)
	if err != nil { return err }

	*result = ForceEndRes{}
	return nil
}

func DraftHandler() (http.Handler){
	s := rpc.NewServer()
	s.RegisterCodec(json2.NewCodec(), "application/json")
	s.RegisterService(&DraftRPC{}, "")

	return NewServer(s)
}
