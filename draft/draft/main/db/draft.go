package db

import (
	"context"
	"main/draft"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type DraftCollection struct {
	*mongo.Collection
}

func FindDraft(draftID string) (draft.Draft, error) {
	coll, discon, err := GetDraftCollection()
	if err != nil { return draft.Draft{}, err	}
	defer discon()

	findRes := coll.FindOne(context.Background(), bson.M{"ID": draftID})
	var res draft.Draft
	err = findRes.Decode(&res)
	if err != nil { return draft.Draft{}, err	}

	return res, nil
}

func UpsertDraft(target draft.Draft) (error) {
	coll, discon, err := GetDraftCollection()
	if err != nil { return err	}
	defer discon()

	_, err = coll.UpdateOne(context.Background(),
		bson.M{"ID": target.ID},
		bson.M{"$set": target},
		options.Update().SetUpsert(true))
	if err != nil { return err	}

	return nil
}

func GetDraftCollection() (coll DraftCollection, disconnect func(), err error) {
	client, err := mongo.Connect(context.Background(), options.Client().ApplyURI("mongodb://draft-db:27017"))
	if err != nil { return DraftCollection{}, nil, err }

	return DraftCollection{client.Database("draft").Collection("draft")},
		func () {client.Disconnect(context.Background())},
		nil
}
