package db

import (
	"context"
	"main/cardpool"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

func FindAllCardpool() ([]cardpool.CardPool, error) {
	coll, discon, err := getCardpoolCollection()
	if err != nil { return []cardpool.CardPool{}, err }
	defer discon()

	cur, err := coll.Find(context.Background(), bson.M{})
	if err != nil { return []cardpool.CardPool{}, err }


	res := []cardpool.CardPool{}
	for cur.Next(context.Background()) {
		var doc cardpool.CardPool
		if err = cur.Decode(&doc); err != nil {
			return []cardpool.CardPool{}, err
		}
		res = append(res, doc)
	}
	
	return res, nil
}

func FindCardpool(cardpoolID string) (cardpool.CardPool, error) {
	coll, discon, err := getCardpoolCollection()
	if err != nil { return cardpool.CardPool{}, err }
	defer discon()

	var res cardpool.CardPool
	findRes := coll.FindOne(context.Background(), bson.M{"ID": cardpoolID})
	err = findRes.Decode(&res)
	if err != nil { return cardpool.CardPool{}, err }

	return res, nil
}

func DeleteCardpool(id string) (error) {
	coll, discon, err := getCardpoolCollection()
	if err != nil { return err }
	defer discon()

	_, err = coll.DeleteOne(context.Background(), bson.M{"ID": id})
	if err != nil { return err }

	return nil
}

func UpsertCardpool(target cardpool.CardPool) (error) {
	coll, discon, err := getCardpoolCollection()
	if err != nil { return err }
	defer discon()

	_, err = coll.UpdateOne(context.Background(),
		bson.M{"ID": target.ID},
		bson.M{"$set": target},
		options.Update().SetUpsert(true))
	if err != nil { return err	}

	return nil
}

func getCardpoolCollection() (coll *mongo.Collection, disconnect func(), err error) {
	client, err := mongo.Connect(context.Background(), options.Client().ApplyURI("mongodb://draft-db:27017"))
	if err != nil { return nil, nil, err }

	return client.Database("draft").Collection("cardpool"),
		func () {client.Disconnect(context.Background())},
		nil
	
}
