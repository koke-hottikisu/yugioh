package db

import (
	"fmt"
	"math/rand"
	"time"
)

var counter = 0

func randomString(n int) string {
	var letters = []rune("abcdefghkmnpqrstwxyzABCEFGHJKLMNPQRSTUVWXYZ23456789")
	
	s := make([]rune, n)
	for i := range s {
		s[i] = letters[rand.Intn(len(letters))]
	}
	return string(s)
}

func GenID() string {
	counter = counter + 1
	return fmt.Sprintf("%s-%d-%d", randomString(10), time.Now().Unix(), counter)
}
