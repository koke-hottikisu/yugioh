package cardpool

import "main/draft"

// postable cardpool by user
type CardPool struct {
	ID string
	Name string
	UserID string
	Set []draft.Card
	History [][]draft.Card
}

func (c *CardPool)UpdateSet(newSet []draft.Card) {
	c.Set = newSet
	c.History = append(c.History, newSet)
}
