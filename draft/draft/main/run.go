package main

import (
	"log"
	"main/handler"
	"math/rand"
	"time"

	"net/http"
)

func main() {
	rand.Seed(time.Now().UnixNano())

	http.Handle("/rpc", handler.DraftHandler())
	http.Handle("/cardpool", handler.CardpoolHandler())
	log.Fatal(http.ListenAndServe(":80", nil))
}
