module main

go 1.14

require (
	github.com/gin-gonic/gin v1.7.3 // indirect
	github.com/gorilla/rpc v1.2.0
	github.com/hashicorp/go-retryablehttp v0.7.0
	github.com/ybbus/jsonrpc/v2 v2.1.6
	go.mongodb.org/mongo-driver v1.7.3
)
