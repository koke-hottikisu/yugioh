package draft

import (
	"math/rand"
	"sort"
)

type Pack struct {
	Card []Card
	ID string
}

func getRandomCard(set []Card) (card Card) {
	allcards := []Card{}
	for _, card := range(set) {
		for range(make([]int, card.EncloseNum)) {
			allcards = append(allcards, card)
		}
	}

	card = allcards[rand.Intn(len(allcards))]
	return card
}

func (p Pack)include(card Card) bool {
	for _, c := range(p.Card) {
		if c.Name == card.Name {
			return true
		}
	}
	return false
}

func CreatePack(rule DraftRule) (pack Pack) {
	pack = Pack{
		Card: []Card{},
		ID:   GenID(),
	}
	for {
		if len(pack.Card) == rule.CardNumInPack {
			break
		}

		c := getRandomCard(rule.Set)
		// 同じカードは封入しない
		if !pack.include(c) || len(rule.Set) <= rule.CardNumInPack {
			pack.Card = append(pack.Card, c)
		}
	}

	sort.Slice(pack.Card, func(i, j int) bool {return pack.Card[i].EncloseNum < pack.Card[j].EncloseNum})

	return pack
}

func (p Pack)Empty() bool {
	return len(p.Card) == 0
}

func (pack *Pack)Pick(pickcard string) (ok bool) {
	for index, packcard := range(pack.Card) {
		if packcard.Name == pickcard {
			// パックからカードを抜く
			pack.Card = append(pack.Card[:index], pack.Card[index+1:]...)
			return true
		}
	}
	return false
}
