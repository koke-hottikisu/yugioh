package draft

import (
	"sort"
	"time"
)

type Player struct {
	ID string
	Name string
	OwnPack []Pack // ピック対象のパック
	PickCard []string
	PickDeadline time.Time
	IsAuto bool
}

func (p *Player)updateDeadline() () {
	if len(p.OwnPack) != 0 {
		deadline := time.Now().Add(time.Second * 6 * time.Duration(len(p.OwnPack[0].Card)))
		p.PickDeadline = deadline
	} else {
		p.PickDeadline = time.Now().AddDate(1, 0, 0)
	}
}

func (p *Player)DealPack(pack Pack) () {
	p.OwnPack = append(p.OwnPack, pack)
	if len(p.OwnPack) == 1 {
		p.updateDeadline()
	}
}

func (p *Player)Pick(pickcard string) (Pack, bool){
	ok := p.OwnPack[0].Pick(pickcard)
	if !ok { return Pack{}, false }

	p.PickCard = append(p.PickCard, pickcard)

	pickedPack := p.OwnPack[0]
	p.OwnPack = p.OwnPack[1:]
	p.updateDeadline()

	return pickedPack, true
}

func (p Player)AutoPickCard() (Card, bool){
	if len(p.OwnPack) <= 0 { return Card{}, false }

	pack := append(p.OwnPack[0].Card)
	
	sort.Slice(pack, func(i, j int) bool {
		return pack[i].EncloseNum < pack[j].EncloseNum && pack[i].EncloseNum < 10
	})

	pickCard := pack[0]
	return pickCard, true
}

func (p Player)HasPack() bool {
	return len(p.OwnPack) > 0
}
