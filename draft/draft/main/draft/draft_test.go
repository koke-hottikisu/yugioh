package draft

import "testing"

func TestPick(t *testing.T) {
	targetDraft, owner := CreateDraft(DraftRule{
		Rotation:      3,
		CardNumInPack: 15,
		Set:           []Card{{Name: "c01",EncloseNum: 1}},
	}, "test")

	targetDraft.Join("p1", false)
	targetDraft.Join("p2", false)

	targetDraft.Start(owner)

	if len(targetDraft.Player[0].OwnPack) != 1 { t.Fatalf("%#v", targetDraft) }
	if len(targetDraft.Player[1].OwnPack) != 1 { t.Fatalf("%#v", targetDraft) }
	if len(targetDraft.Player[2].OwnPack) != 1 { t.Fatalf("%#v", targetDraft) }

	targetDraft.Pick(owner, "c01")

	if (len(targetDraft.Player[0].OwnPack) == 1 &&
		len(targetDraft.Player[1].OwnPack) == 1 &&
		len(targetDraft.Player[1].OwnPack) == 1) {
		t.Fatalf("%#v", targetDraft)
	}
}

func TestAutoPick(t *testing.T) {
	targetDraft, owner := CreateDraft(DraftRule{
		Rotation:      3,
		CardNumInPack: 15,
		Set:           []Card{{Name: "c01",EncloseNum: 1}},
	}, "test")

	targetDraft.Join("p1", true)

	targetDraft.Start(owner)

	targetDraft.Pick(owner, "c01")
	targetDraft.AutoPick()
	if len(targetDraft.Player[0].OwnPack) != 1 { t.Fatalf("%#v", targetDraft) }
	if len(targetDraft.Player[1].OwnPack) != 1 { t.Fatalf("%#v", targetDraft) }
}

