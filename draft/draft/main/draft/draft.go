package draft

import (
	"math/rand"
	"time"
)

type Draft struct {
	ID string
	IsStarted bool
	IsEnd bool
	Owner string
	Rule DraftRule
	Player []Player
	CurrentRotation int
}
func CreateDraft(rule DraftRule, playerName string) (draft Draft, playerID string) {
	draftID := GenID()

	draft = Draft{
		ID:              draftID,
		IsStarted:       false,
		IsEnd:           false,
		Owner:           "",
		Rule:            rule,
		Player:          []Player{},
		CurrentRotation: 0,
	}

	playerID = draft.Join(playerName, false)
	draft.Owner = playerID

	return draft, playerID
}
func (d Draft)IsOwn(playerID string) bool {
	return playerID == d.Owner
}
func (d Draft)GetPlayer(PlayerID string) (*Player, bool) {
	for index, player := range d.Player {
		if player.ID == PlayerID {
			return &d.Player[index], true
		}
	}
	return &Player{}, false
}
func (d Draft)AllPlayerPackEmpty() (bool) {
	for _, player := range(d.Player) {
		if len(player.OwnPack) != 0 {
			return false
		}
	}
	return true
}
func (d Draft)WhoisNextPlayer(player Player) (*Player, bool) {
	playerList := append(d.Player, d.Player[0])
	for index, nextPlayer := range(d.Player) {
		if player.ID == nextPlayer.ID {
			return d.GetPlayer(playerList[index + 1].ID)
		}
	}
	return &Player{}, false
}
func (d *Draft)Join(playerName string, isAuto bool) (playerID string) {
	playerID = GenID()
	d.Player = append(d.Player, Player{
		ID:           playerID,
		Name:         playerName,
		OwnPack:      []Pack{},
		PickCard:     []string{},
		PickDeadline: time.Now().AddDate(1, 0, 0),
		IsAuto:       isAuto,
	})
	return playerID
}
func (d *Draft)Exit(playerID string) {
	newPlayers := []Player{}
	for _, player := range(d.Player) {
		if player.ID != playerID {
			newPlayers = append(newPlayers, player)
		}
	}
	d.Player = newPlayers
}
func (d *Draft)PickDeadline() {
		for _, player := range(d.Player) {
			if(time.Now().After(player.PickDeadline.Add(time.Second * 5))) {
				if pickCard, ok := player.AutoPickCard(); ok {
					d.Pick(player.ID, pickCard.Name)
				}
			}
		}
}
func (d *Draft)AutoPick() {
	for _, player := range(d.Player) {
		if player.IsAuto {
			if pickCard, ok := player.AutoPickCard(); ok {
				d.Pick(player.ID, pickCard.Name)
			}
		}
	}
}
func (d *Draft)Start(playerID string) (ok bool) {
	if d.Owner != playerID {
	return false
	}

	if d.IsStarted {
		return true
	}

	// 席順を決める
	d.Player = shuffle(d.Player)

	// パック作って、配る
	d.dealNewPack()

	d.IsStarted = true
	return true
}

func (d *Draft)dealNewPack() {
	newPlayers := []Player{}
	for _, player := range(d.Player) {
		pack := CreatePack(d.Rule)
		player.DealPack(pack)
		newPlayers = append(newPlayers, player)
	}

	d.Player = newPlayers
	d.CurrentRotation = d.CurrentRotation + 1
}

func (d *Draft)Pick(playerID string, pickcard string) (ok bool) {
	player, playerExists := d.GetPlayer(playerID)
	if !playerExists { return false }

	pickedPack, ok := player.Pick(pickcard)
	if !ok { return false }

	// カードが残ってれば、パックを次に回す
	if len(pickedPack.Card) != 0 {
		nextPlayer, ok := d.WhoisNextPlayer(*player)
		if !ok { return false }
		nextPlayer.DealPack(pickedPack)
	}

	// 全プレーヤーがパックを開けきったら、もう一週
	if d.AllPlayerPackEmpty() {
		if d.CurrentRotation < d.Rule.Rotation {
			d.dealNewPack()
		} else {
			d.IsEnd = true
		}
	}
	return true
}

func shuffle(data []Player) ([]Player) {
	n := len(data)
	for i := n - 1; i >= 0; i-- {
		j := rand.Intn(i + 1)
		data[i], data[j] = data[j], data[i]
	}
	return data
}
