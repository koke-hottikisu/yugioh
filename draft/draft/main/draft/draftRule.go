package draft

type Card struct {
	Name string
	EncloseNum int //封入枚数
}

type DraftRule struct {
	Rotation int //ピックを何往復するか ex)mtg: 3
	CardNumInPack int //1パックあたりのカード枚数 ex)mtg: 15
	Set []Card
}
