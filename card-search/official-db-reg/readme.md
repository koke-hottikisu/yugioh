# test

app以下にパッケージがない場合
```
docker build -q . | xargs -I@ docker run --rm @ sh -c "cd /app && go test *.go"
```

app以下にパッケージがある場合
```
docker build -q . | xargs -I@ docker run --rm @ sh -c "cd /app && go list ./... | grep -v '^app$' | xargs go test && go test *.go"
```

# perf
```
docker build -q . | xargs -I@ docker run -v (pwd)/app/pprof:/app/pprof @ sh -c "cd /app && go test -bench . -cpuprofile=pprof/cpu.out"
```

# カード画像
オリジナルは`200x290`
イラストは`(26,55)-(176,204)`

