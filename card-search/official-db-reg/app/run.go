package main

import (
	"context"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"image"
	"image/png"
	"io"
	"io/ioutil"
	"log"
	"sync"
	"time"

	"net/http"
	_ "net/http/pprof"

	"github.com/hashicorp/go-retryablehttp"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

var (
	update_timestamp = int64(0)
)

func main() {
	go func() {
		log.Println(http.ListenAndServe("localhost:22220", nil))
	}()

	client, err := getMongoClient("mongodb://carddb-db:27017")
	defer func() {
    if err = client.Disconnect(context.Background()); err != nil {
			log.Print(err)
    }
	}()
	if err != nil {
		log.Printf("Can not get DB client\n%s", err)
	}

	for true {
		if getTimestamp(client) > update_timestamp {
			log.Printf("[info] Carddb is updated. Start fetch and register.")
			update(client)

			update_timestamp = time.Now().Unix()
		}

		time.Sleep(30 * time.Minute)
	}
}

type RegisterBody struct {
	Image string `json:"img" binding:"required"`
	Tag string `json:"tag" binding:"required"`
}
func postUpdate(body RegisterBody) {
	bodystring, err := json.Marshal(body)
	if err != nil {
		log.Printf("%s", err)
	}
	resp, err := retryablehttp.Post("http://localhost:80/register", "application/json", bodystring)
	defer resp.Body.Close()
	if err != nil {
		log.Printf("%s", err)
	}
	if resp.StatusCode != 200 {
		log.Printf("image-searchへの登録に失敗しました")
	}
}

func update(client *mongo.Client) {
	cards_coll := client.Database("yugioh").Collection("cards")

	cur, err := cards_coll.Find(context.Background(), bson.M{}, nil)
	defer cur.Close(context.Background())
	if err != nil {
		log.Printf(err.Error())
	}

	cards := []CardsColl{}
	err = cur.All(context.Background(), &cards)
	if err != nil { fmt.Printf("%s", err) }

	mu1 := sync.Mutex{}
	mu2 := sync.Mutex{}
	for _, card := range cards {
		for _, image_url := range card.Images {
			go func(image_url string, name string, mu1 *sync.Mutex, mu2 *sync.Mutex) {
				mu1.Lock()
				image_body, err := retryablehttp.Get(fmt.Sprintf("https://%s", image_url))
				mu1.Unlock()

				if err != nil {
					fmt.Printf("%s", err)
					return
				}

				illust, err := trimIllust(image_body.Body)
				if err != nil {
					log.Printf("%s", err)
					return
				}

				mu2.Lock()
				postUpdate(RegisterBody{
					Image: illust,
					Tag: name,
				})
				mu2.Unlock()
			}(image_url, card.Name, &mu1, &mu2)
		}
	}
}

func trimIllust(imgR io.Reader) (string, error) {
	img, _, err := image.Decode(imgR)
	if err != nil { fmt.Printf("%s", err) }

	illust, err := cropImage(img, image.Rect(26, 55, 176, 204))
	if err != nil {
		return "", err
	}

	pr, pw := io.Pipe()
	go func(w *io.PipeWriter){
    enc := &png.Encoder{
			CompressionLevel: png.NoCompression,
    }
		enc.Encode(w, illust)
		w.Close()
	}(pw)
	d, err := ioutil.ReadAll(pr)
	if err != nil {
		return "", err
	}

	return base64.StdEncoding.EncodeToString(d), nil
}

func cropImage(img image.Image, crop image.Rectangle) (image.Image, error) {
    type subImager interface {
        SubImage(r image.Rectangle) image.Image
    }

    // img is an Image interface. This checks if the underlying value has a
    // method called SubImage. If it does, then we can use SubImage to crop the
    // image.
    simg, ok := img.(subImager)
    if !ok {
        return nil, fmt.Errorf("image does not support cropping")
    }

    return simg.SubImage(crop), nil
}

type CardsColl struct {
	Name string "name"
	Images []string "imageurl"
}

type DateColl struct {
	Timestamp int64 "timestamp"
}

func getTimestamp(clinet *mongo.Client) (int64) {
  var res DateColl
	clinet.Database("yugioh").Collection("update-timestamp").FindOne(context.Background(), bson.M{}).Decode(&res)
	return res.Timestamp
}


func getMongoClient(db_url string) (*mongo.Client, error) {
	client, err := mongo.Connect(context.Background(), options.Client().ApplyURI(db_url))
	if err != nil {
		log.Println("Can not Connect to db: " + db_url)
	}

	return client, err
}
