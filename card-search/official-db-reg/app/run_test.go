package main

import (
	"encoding/base64"
	"image"
	"io"
	"os"
	"strings"
	"testing"

	_ "image/png"
)

func readImage() (io.Reader, error) {
	pwd, err := os.Getwd()
	if err != nil {
		return nil, err
	}

	file, err := os.Open(pwd + "/run_test/test.png")
	if err != nil {
		return nil, err
	}

	return file, err
}

func TestRun(t *testing.T) {
	imgR, _ := readImage()
	illust_b64, err := trimIllust(imgR)
	if err != nil {
		t.Fatalf("error trimIllust: %s", err)
	}

	illust_byte, _ := base64.StdEncoding.DecodeString(illust_b64)
	illust_img, _, err := image.Decode(strings.NewReader(string(illust_byte)))
	if err != nil {
		t.Fatalf("can not decode trimIllust image: %s", err)
	}

	rect := illust_img.Bounds()
	if rect.Max.X != 150 {
		t.Fatalf("trimIllust後の幅が間違っています。 expect: %d   actual: %d", 151, rect.Max.X)
	}
	if rect.Max.Y != 149 {
		t.Fatalf("trimIllust後の幅が間違っています。 expect: %d   actual: %d", 151, rect.Max.Y)
	}
}

func BenchmarkTrim(b *testing.B) {
	for i := 0; i < b.N; i++ {
		img, _ := readImage()
		b.StartTimer()
		trimIllust(img)
		b.StopTimer()
	}
}
