module app

go 1.16

require (
	github.com/carlescere/scheduler v0.0.0-20170109141437-ee74d2f83d82 // indirect
	github.com/gin-gonic/gin v1.7.3 // indirect
	github.com/hashicorp/go-retryablehttp v0.7.0
	go.mongodb.org/mongo-driver v1.7.1
)
