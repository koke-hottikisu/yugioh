package exporter

import (
	"github.com/gin-gonic/gin"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
	"github.com/prometheus/client_golang/prometheus/promhttp"
)

var (
	ImageNum prometheus.Gauge
)

func NewRegistry() *prometheus.Registry {
	reg := prometheus.NewRegistry()
	ImageNum = promauto.With(reg).NewGauge(
		prometheus.GaugeOpts{
			Namespace:   "",
			Subsystem:   "",
			Name: "image_search_registerd_image_num",
			Help: "Num of registerd image at image-search",
		})
	return reg
}

func PrometheusHandler(reg *prometheus.Registry) gin.HandlerFunc {
	h := promhttp.HandlerFor(reg, promhttp.HandlerOpts{})
	return func(c *gin.Context) {
		h.ServeHTTP(c.Writer, c.Request)
	}
}
