package cbir

import (
	"crypto/md5"
	"encoding/base64"
	"fmt"
	"main/exporter"
	"math"

	"github.com/corona10/goimagehash"
	"gocv.io/x/gocv"
)

type Cbir struct {
	id2Tag map[string]string
	algos []Algo
}

type Algo struct {
	algo CbirAlgo
	weight int
}

func NewCbir() Cbir {
	return Cbir{
		id2Tag:         map[string]string{},
		algos: []Algo{
			{
				algo:	&Ahash{
					indexDB: map[string]*goimagehash.ImageHash{},
				},
				weight: 5,
			},
			{
				algo:	&Hist{
					indexDB: map[string][3]gocv.Mat{},
				},
				weight: 1,
			},
		},
	}
}

type CbirAlgo interface {
	Register(image []byte, id string) (err error)

	// 大きいほど似ている画像
	Score(image []byte) (score map[string]float64, err error)
}

func getID(image []byte) string {
	return fmt.Sprintf("%x", md5.Sum(image))
}

func (c *Cbir)Register(imageBase64 string, tag string) error {
	image, err := base64.StdEncoding.DecodeString(imageBase64)
	if err != nil {
		return err
	}

	// 同じカードが登録されないようにする
	id := getID(image)
	for i := range c.id2Tag {
		if i == id {
			return nil
		}
	}

	c.id2Tag[id] = tag

	for _, algo := range c.algos {
		err = algo.algo.Register(image, id)
		if err != nil {
			return err
		}
	}

	// add exporter
	exporter.ImageNum.Set(float64(len(c.id2Tag)))
	return nil
}

func (c *Cbir)Search(imageBase64 string) (string, error) {
	image, err := base64.StdEncoding.DecodeString(imageBase64)
	if err != nil { return "", err }

	totalScore := map[string]float64{}

	for _, algo := range c.algos {
		score, err := algo.algo.Score(image)
		if err != nil { return "", err }

		data := []float64{}
		for _, s := range score {
			data = append(data, s)
		}
		ave := average(data)
		vari := variance(data, ave)

		for id, s := range score {
			totalScore[id] += (s - ave) / vari * float64(algo.weight)
		}
	}

	maxTag := ""
	max := -math.MaxFloat64
	for id, s := range totalScore {
		if max < s {
			maxTag = c.id2Tag[id]
			max = s
		}
	}

	return maxTag, nil
}

func variance(data []float64, average float64) float64 {
	sum := 0.0
	for _, d := range data {
		sum += math.Pow(d - average, 2)
	}
	return math.Sqrt((sum / float64(len(data))))
}

func average(data []float64) float64 {
	sum := 0.0
	for _, d := range data {
		sum += d
	}

	return sum / float64(len(data))
}

func (c Cbir)Count() int {
	return len(c.id2Tag)
}
