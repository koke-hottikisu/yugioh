package cbir

import (
	"encoding/base64"
	"io/ioutil"
	"main/exporter"
	"os"
	"testing"
)

func readImage(name string) (string, error) {
	pwd, err := os.Getwd()
	if err != nil {
		return "", err
	}

	file, err := os.Open(pwd + "/cbir_test/" + name)
	if err != nil {
		return "", err
	}

	data, err := ioutil.ReadAll(file)
	if err != nil {
		return "", err
	}

	return base64.StdEncoding.EncodeToString(data), nil
}

func TestAll(t *testing.T) {
	cbir := NewCbir()
	exporter.NewRegistry()

	lenna64, err := readImage("lenna.png")
	if err != nil {
		t.Fatal(err)
	}
	lennaS64, err := readImage("lenna-sample.png")
	if err != nil {
		t.Fatal(err)
	}
	parrots64, err := readImage("parrots.png")
	if err != nil {
		t.Fatal(err)
	}

	err = cbir.Register(lenna64, "lenna")
	if err != nil {
		t.Fatal(err)
	}
	err = cbir.Register(parrots64, "parrots")
	if err != nil {
		t.Fatal(err)
	}

	res, err := cbir.Search(lennaS64)
	if err != nil {
		t.Fatal(err)
	}
  if res != "lenna" {
		t.Fatal(res)
	}
}

func TestRegSameImage(t *testing.T) {
	cbir := NewCbir()
	exporter.NewRegistry()

	lenna64, err := readImage("lenna.png")
	if err != nil {
		t.Fatal(err)
	}
	err = cbir.Register(lenna64, "lenna")
	if err != nil {
		t.Fatal(err)
	}
	err = cbir.Register(lenna64, "lenna")
	if err != nil {
		t.Fatal(err)
	}

	if cbir.Count() == 2 {
		t.Fatalf("同じ画像を登録しないようにする機能が働いていない")
	}
	if cbir.Count() != 1 {
		t.Fatalf("画像登録がうまく行っていない\n  expect:%d\n  actual:%d", 1, cbir.Count())
	}
}
