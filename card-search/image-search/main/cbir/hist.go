package cbir

import (
	"gocv.io/x/gocv"
)

type Hist struct {
	indexDB map[string][3]gocv.Mat
}

func getHist(image []byte) ([3]gocv.Mat, error) {
	mat, err := gocv.IMDecode(image, gocv.IMReadUnchanged)
	gocv.CvtColor(mat, &mat, gocv.ColorBGRToHSVFull)
	if err != nil {
		return [3]gocv.Mat{}, err
	}

	hist := [3]gocv.Mat { gocv.NewMat(), gocv.NewMat(), gocv.NewMat() }
	for idx := 0; idx < 3; idx++ {
		gocv.CalcHist([]gocv.Mat{mat}, []int{idx}, gocv.NewMat(), &hist[idx], []int{256}, []float64{0, 256}, false)
	}

	return hist, nil
}

func (h *Hist)Register(image []byte, id string) (err error) {
	hist, err := getHist(image)
	if err != nil {
		return err
	}

	h.indexDB[id] = hist
	return nil
}

func (h *Hist)Score(image []byte) (score map[string]float64, err error) {
	score = map[string]float64{}
	hist, err := getHist(image)
	if err != nil {
		return nil, err
	}

	for t, h := range h.indexDB {
		for idx := 0; idx < 3; idx++ {
			correl := gocv.CompareHist(hist[idx], h[idx], gocv.HistCmpCorrel)
			score[t] += float64(correl)
		}		
	}

	return score, nil
}
