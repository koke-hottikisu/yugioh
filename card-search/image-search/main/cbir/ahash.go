package cbir

import (
	"bytes"
	"image/png"

	"github.com/corona10/goimagehash"
)

type Ahash struct {
	indexDB map[string]*goimagehash.ImageHash
}

func getHash(image []byte) (hash *goimagehash.ImageHash, err error) {
	r := bytes.NewReader(image)
	img, err := png.Decode(r)
	if err != nil { return nil, err}

	return goimagehash.PerceptionHash(img)
}

func (a *Ahash)Register(image []byte, id string) (err error) {
	hash, err := getHash(image)
	if err != nil { return err }

	a.indexDB[id] = hash
	return nil
}

func (a *Ahash)Score(image []byte) (score map[string]float64, err error) {
	score = map[string]float64{}

	hash, err := getHash(image)
	if err != nil { return nil, err }

	for t, h := range a.indexDB {
		dist, err := hash.Distance(h)
		if err != nil {	return nil, err }
		score[t] = -float64(dist)
	}

	return score, nil
}
