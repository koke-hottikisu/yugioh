package main

import (
	"log"
	"main/cbir"
	"main/exporter"

	"github.com/gin-gonic/gin"
	"github.com/gin-contrib/pprof"
	"github.com/prometheus/client_golang/prometheus"
)

var cbirCli cbir.Cbir

func main() {
	reg := exporter.NewRegistry()
	engine := SetupRouter(reg)
	engine.Run(":80")
}

func SetupRouter(reg *prometheus.Registry) *gin.Engine {
	engine := gin.Default()
	cbirCli = cbir.NewCbir()

	pprof.Register(engine)

	engine.POST("/search", search)
	engine.POST("/register", register)
	engine.POST("/registerlist", registerList)

	engine.GET("/metrics", exporter.PrometheusHandler(reg))
	return engine
}

func search(c *gin.Context) {
	type SearchBody struct {
		Query string `json:"query" binding:"required"`
	}
	var json SearchBody
	if err := c.ShouldBindJSON(&json); err != nil {
		c.JSON(400, gin.H{})
		log.Printf("return 400")
	}

	res, err := cbirCli.Search(json.Query)
	if err != nil {
		c.JSON(400, gin.H{})
		log.Printf("return 400")
	}

	c.JSON(200, gin.H{"tag": res})
	log.Printf("return 200 %s", gin.H{"tag": res})
}

func register(c *gin.Context) {
	type RegisterBody struct {
		Image string `json:"img" binding:"required"`
		Tag string `json:"tag" binding:"required"`
	}
	var json RegisterBody
	if err := c.ShouldBindJSON(&json); err != nil {
		c.JSON(400, gin.H{})
	}

	cbirCli.Register(json.Image , json.Tag)
	c.JSON(200, gin.H{})
}

func registerList(c *gin.Context) {
	type RegisterBody struct{
		Image string `json:"img" binding:"required"`
		Tag string `json:"tag" binding:"required"`
	}
	var json []RegisterBody
	if err := c.ShouldBindJSON(&json); err != nil {
		c.JSON(400, gin.H{})
	}

	for _, d := range json {
		cbirCli.Register(d.Image , d.Tag)
	}

	c.JSON(200, gin.H{})
}
