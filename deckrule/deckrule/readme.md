# test

main以下にパッケージがない場合
```
docker build -q . | xargs -I@ docker run --rm @ sh -c "cd /main && go test *.go"
```

main以下にパッケージがある場合
```
docker build -q . | xargs -I@ docker run --rm @ sh -c "cd /main && go list ./... | grep -v '^main$' | xargs go test && go test *.go"
```
