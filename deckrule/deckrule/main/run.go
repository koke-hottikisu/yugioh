package main

import (
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"main/auth"
	"main/db"
	"net/http"
	"strings"
	"time"

	"github.com/gin-gonic/gin"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

func main() {
	ctx, cancel := context.WithTimeout(context.Background(), 10 * time.Second)
	defer cancel()
	client, err := mongo.Connect(ctx, options.Client().ApplyURI("mongodb://localhost:27017"))
	defer func() {
    if err = client.Disconnect(ctx); err != nil {
			panic(err)
    }
	}()
	if err != nil {
		log.Println(err)
		return
	}

	coll := db.RuleCollection(client)
	authcli := auth.AuthClient{}

	r := setupRouter(coll, authcli)
	r.Run(":80")
}

func setupRouter(coll db.RuleCollectionI, authcli auth.AuthI) *gin.Engine {
	r := gin.Default()
	r.GET("/rule", getAllHandler(coll))
	r.POST("/rule", upsertHandler(coll, authcli))
	r.DELETE("/rule", deleteHandler(coll, authcli))
	// /request_own?ruleId=***
	r.GET("/request_own", requestOwnHandler(coll, authcli))
	return r
}

func handleAuth(c *gin.Context, authCli auth.AuthI) (bool, string) {
	authorizationHeader := c.Request.Header.Get("Authorization")
	userKey := strings.Replace(authorizationHeader, "userKey ", "", -1)
	if userKey == "" {
		c.String(http.StatusUnauthorized, "Header \"Authorization: userKey\" is required")
		return false, ""
	}
	ok, userId, err := authCli.GetUserId(userKey)
	if err != nil {
		log.Println(err)
		c.String(http.StatusInternalServerError, "%s", err)
		return false, ""
	}
	if !ok {
		c.String(http.StatusUnauthorized, "userKey is invalid")
		return false, ""
	}
	return true, userId
}

func handleFetchRuleRequestBody(c *gin.Context) (bool, db.Rule) {
	body := c.Request.Body
	bodyData, err := ioutil.ReadAll(body)
	if err != nil {
		log.Println(err)
		c.String(http.StatusInternalServerError, "%s", err)
		return false, db.Rule{}
	}
	body.Close()

	var requBody db.Rule
	err = json.Unmarshal(bodyData, &requBody)
	if err != nil {
		log.Println(err)
		c.String(http.StatusBadRequest, "%s", err)
		return false, db.Rule{}
	}

	return true, requBody
}

func getAllHandler(coll db.RuleCollectionI) (func(*gin.Context)()) {
	return func(c *gin.Context) {
		rules, err := coll.FindAll(c)
		if err != nil {
			log.Println(err)
			c.String(http.StatusInternalServerError, "%s", err)
			return
		}

		if rules == nil {
			rules = []db.Rule{}
		}
		c.JSON(http.StatusOK, rules)
	}
}

func requestOwnHandler(coll db.RuleCollectionI, authCli auth.AuthI) func(*gin.Context)() {
	return func(c *gin.Context) {
		ok, reqBodyRule := handleFetchRuleRequestBody(c)
		if !ok {return}

		ok, userId := handleAuth(c, authCli)
		if !ok {return}

		err := coll.RequestOwn(c, reqBodyRule, userId)
		if err != nil {
			log.Println(err)
			c.String(http.StatusBadRequest, "%s", err)
			return
		}

		c.String(http.StatusOK, "")
	}
}

func upsertHandler(coll db.RuleCollectionI, authCli auth.AuthI) func(*gin.Context)() {
	return func(c *gin.Context) {
		ok, reqBodyRule := handleFetchRuleRequestBody(c)
		if !ok {return}

		ok, userId := handleAuth(c, authCli)
		if !ok {
			return
		}

		if !reqBodyRule.IsOwner(userId) {
			log.Printf("User %s is not owner of Rule %s", userId, reqBodyRule.ID)
			c.String(http.StatusUnauthorized, fmt.Sprintf("User %s is not owner of Rule %s", userId, reqBodyRule.ID))
			return
		}

		upsertRule, deleted, err := coll.Upsert(c, reqBodyRule)
		if err != nil {
			log.Println(err)
			c.String(http.StatusInternalServerError, "%s", err)
			return
		}

		upsertRuleStr, _ := json.Marshal(upsertRule)
		log.Printf("[Info] UpsertRule")
		log.Printf("[Info] is replace? : %t", deleted)
		log.Printf("[Info] %s", upsertRuleStr)
		c.JSON(http.StatusOK, upsertRule)
	}
}

func deleteHandler(coll db.RuleCollectionI, authCli auth.AuthI) func(*gin.Context)() {
	return func(c *gin.Context) {
		ok, reqBodyRule := handleFetchRuleRequestBody(c)
		if !ok {return}

		ok, userId := handleAuth(c, authCli)
		if !ok {
			return
		}

		if !reqBodyRule.IsOwner(userId) {
			log.Printf("User %s is not owner of Rule %s", userId, reqBodyRule.ID)
			c.String(http.StatusUnauthorized, fmt.Sprintf("User %s is not owner of Rule %s", userId, reqBodyRule.ID))
			return
		}

		deleted, err := coll.Delete(c, reqBodyRule)
		if err != nil {
			log.Println(err)
			c.String(http.StatusInternalServerError, "%s", err)
			return
		}

		if !deleted {
			c.String(http.StatusBadRequest, "rule is not exists %s", reqBodyRule)
		}

		c.String(http.StatusOK, "")
	}
}
