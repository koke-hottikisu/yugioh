package auth

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"

	"github.com/hashicorp/go-retryablehttp"
)

type RespGetUserId struct {
	UserId string `json:"userId"`
	Exists bool `json:"exists"`
}

type AuthI interface {
	GetUserId(userKey string) (ok bool, userId string, err error)
}

type AuthClient struct {
	AuthI
}

func (_ AuthClient)GetUserId(userKey string) (ok bool, userId string, err error) {
	resp, err := retryablehttp.Get("http://auth/get_userId?userKey=" + userKey)
	if err != nil {
		return false, "", err
	}
	if resp.StatusCode != 200 {
		return false, "", errors.New(fmt.Sprintf("status code is not 200: %d", resp.StatusCode))
	}

	body, err := ioutil.ReadAll(resp.Body)
	resp.Body.Close()
	if err != nil {
		return false, "", err
	}

	var res RespGetUserId
	json.Unmarshal(body, &res)

	return res.Exists, res.UserId, nil
}
