package main

import (
	"bytes"
	"context"
	"encoding/json"
	"io/ioutil"
	"log"
	"main/auth"
	"main/db"
	"net/http"
	"net/http/httptest"
	"testing"
	"time"

	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type AuthTestCli struct {
	auth.AuthI
}

func (_ AuthTestCli)GetUserId(userKey string) (ok bool, userId string, err error) {
	return true, "testuserid", nil
}

func TestMarshal(t *testing.T) {
	ctx, cancel := context.WithTimeout(context.Background(), 10 * time.Second)
	defer cancel()
	client, err := mongo.Connect(ctx, options.Client().ApplyURI("mongodb://db-test:27017"))
	defer func() {
    if err = client.Disconnect(ctx); err != nil {
			panic(err)
    }
	}()
	if err != nil {
		t.Fatal(err)
		return
	}

	coll := db.RuleCollection(client)
	coll.Drop(context.Background())
	authcli := AuthTestCli{}

	router := setupRouter(coll, authcli)
	ts := httptest.NewServer(router)

	coll.Upsert(context.Background(), db.Rule{})
	resp, err := http.Get(ts.URL + "/rule")
	if err != nil {t.Fatal(err)}
	if resp.StatusCode != http.StatusOK {t.Fatalf("status is not 200: %d", resp.StatusCode)}

	body := resp.Body
	bodyByte, err := ioutil.ReadAll(body)
	if err != nil {t.Fatal(err)}
	body.Close()

	var insRuleList []db.Rule
	err = json.Unmarshal(bodyByte, &insRuleList)
	if err != nil {t.Fatal(err)}
	log.Println(insRuleList[0].ID)
	if insRuleList[0].ID == primitive.NilObjectID {
		t.Fatal("ID is null...")
	}
}

func TestUpsert(t *testing.T) {
	ctx, cancel := context.WithTimeout(context.Background(), 10 * time.Second)
	defer cancel()
	client, err := mongo.Connect(ctx, options.Client().ApplyURI("mongodb://db-test:27017"))
	defer func() {
    if err = client.Disconnect(ctx); err != nil {
			panic(err)
    }
	}()
	if err != nil {
		t.Fatal(err)
		return
	}

	coll := db.RuleCollection(client)
	coll.Drop(context.Background())
	authcli := AuthTestCli{}

	router := setupRouter(coll, authcli)
	ts := httptest.NewServer(router)

	rule1, err := json.Marshal(db.Rule{Name: "rule1", Owner: []string{"testuserid"}})
	if err != nil {t.Fatal(err)}
	req, err := http.NewRequest("POST", ts.URL + "/rule", bytes.NewReader(rule1))
	if err != nil {t.Fatal(err)}
	req.Header.Add("Authorization", "userKey dammykey")
	resp, err := http.DefaultClient.Do(req)
	if err != nil {t.Fatal(err)}
	if resp.StatusCode != http.StatusOK {t.Fatalf("status is not 200: %d", resp.StatusCode)}

	var insRuleList []db.Rule
	resp, err = http.Get(ts.URL + "/rule")
	if err != nil {t.Fatal(err)}
	bodyByte, err := ioutil.ReadAll(resp.Body)
	if err != nil {t.Fatal(err)}
	resp.Body.Close()
	err = json.Unmarshal(bodyByte, &insRuleList)
	if err != nil {t.Fatal(err)}

	rule2 := insRuleList[0]
	rule2.Name = "rule2"
	rule2byte, err := json.Marshal(rule2)
	req, err = http.NewRequest("POST", ts.URL + "/rule", bytes.NewReader(rule2byte))
	if err != nil {t.Fatal(err)}
	req.Header.Add("Authorization", "userKey dammykey")
	resp, err = http.DefaultClient.Do(req)
	if err != nil {t.Fatal(err)}
	if resp.StatusCode != http.StatusOK {t.Fatalf("status is not 200: %d", resp.StatusCode)}

	resp, err = http.Get(ts.URL + "/rule")
	if err != nil {t.Fatal(err)}
	bodyByte, err = ioutil.ReadAll(resp.Body)
	if err != nil {t.Fatal(err)}
	resp.Body.Close()
	err = json.Unmarshal(bodyByte, &insRuleList)
	if err != nil {t.Fatal(err)}
	ruleNum := len(insRuleList)
	if ruleNum != 1 {t.Fatalf("ans is 1, res is %d", ruleNum)}
}
