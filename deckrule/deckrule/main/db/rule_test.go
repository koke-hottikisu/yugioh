package db

import (
	"context"
	"encoding/json"
	"testing"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

func TestUpsert(t *testing.T) {
	ctx, cancel := context.WithTimeout(context.Background(), 10 * time.Second)
	defer cancel()
	client, err := mongo.Connect(ctx, options.Client().ApplyURI("mongodb://db-test:27017"))
	defer func() {
    if err = client.Disconnect(ctx); err != nil {
			panic(err)
    }
	}()
	if err != nil {t.Fatal(err)}

	coll := RuleCollection(client)
	coll.Drop(context.Background())

	rule, deleted, err := coll.Upsert(ctx, Rule{})
	if err != nil {t.Fatal(err)}

	rule, deleted, err = coll.Upsert(ctx, Rule{})
	if err != nil {t.Fatal(err)}

	rule, deleted, err = coll.Upsert(ctx, Rule{})
	if err != nil {t.Fatal(err)}
	if deleted == true {t.Fatal("should not delete but deleted")}

	rule, deleted, err = coll.Upsert(ctx, Rule{ID: rule.ID})
	if err != nil {t.Fatal(err)}
	if deleted == false {
		t.Fatalf("should delete but not deleted")
	}

	rule, deleted, err = coll.Upsert(ctx, Rule{})
	if err != nil {t.Fatal(err)}

	v, err := coll.CountDocuments(ctx, bson.M{})
	if err != nil {t.Fatal(err)}
	if int(v) != 4 {
		t.Fatalf("ans: %d,  res: %d", 4, int(v))
	}
}

func TestUpsertSameID(t *testing.T) {
	ctx, cancel := context.WithTimeout(context.Background(), 10 * time.Second)
	defer cancel()
	client, err := mongo.Connect(ctx, options.Client().ApplyURI("mongodb://db-test:27017"))
	defer func() {
    if err = client.Disconnect(ctx); err != nil {
			panic(err)
    }
	}()
	if err != nil {t.Fatal(err)}

	coll := RuleCollection(client)
	coll.Drop(context.Background())

	rule, _, err := coll.Upsert(ctx, Rule{})
	if err != nil {t.Fatal(err)}

	rule2, _, err := coll.Upsert(ctx, rule)
	if err != nil {t.Fatal(err)}

	if rule.ID.String() != rule2.ID.String() {
		t.Fatalf("%s != %s", rule.ID.String(), rule2.ID.String())
	}
}

func TestMarshal(t *testing.T) {
	ctx, cancel := context.WithTimeout(context.Background(), 10 * time.Second)
	defer cancel()
	client, err := mongo.Connect(ctx, options.Client().ApplyURI("mongodb://db-test:27017"))
	defer func() {
    if err = client.Disconnect(ctx); err != nil {
			panic(err)
    }
	}()
	if err != nil {t.Fatal(err)}

	coll := RuleCollection(client)
	coll.Drop(context.Background())

	rule, _, _ := coll.Upsert(ctx, Rule{})
	ruleid := rule.ID.String()
	ruleByte, _ := json.Marshal(rule)
	ruleString := string(ruleByte)

	var ruleUM Rule
	json.Unmarshal([]byte(ruleString), &ruleUM)
	ruleUMid := ruleUM.ID.String()

	if ruleUMid != ruleid {
		t.Fatalf("%s != %s", ruleUMid, ruleid)
	}
}
