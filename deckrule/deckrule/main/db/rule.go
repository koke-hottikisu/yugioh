package db

import (
	"context"
	"log"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

type Cost struct {
	Name string `json:"name" bson:"name"`
	Cost int `json:"cost" bson:"cost"`
}

type Rule struct {
  ID primitive.ObjectID `json:"id" bson:"_id"`
	Name string `json:"name" bson:"name"`
	Owner []string `json:"owners" bson:"owner"`
	OwnerCandidate []string `json:"ownercandidates" bson:"ownercandidate"`
	Cost []Cost `json:"costcards" bson:"cost"`
}

type RuleCollectionI interface {
	Upsert(ctx context.Context, rule Rule) (Rule, bool, error)
	Delete(ctx context.Context, rule Rule) (bool, error)
	FindAll(ctx context.Context) ([]Rule, error)
	RequestOwn(ctx context.Context, rule Rule, userId string) (error)
}

type RuleCollectionMongo struct {
	*mongo.Collection
}

func RuleCollection(client *mongo.Client) RuleCollectionMongo {
	return RuleCollectionMongo{
		client.Database("deckrule").Collection("rules"),
	}
}

func (c RuleCollectionMongo) RequestOwn(ctx context.Context, rule Rule, userId string) (error) {
	ownerCandidate := rule.OwnerCandidate

	isExists := false
	for _, o := range ownerCandidate {
		if o == userId {
			isExists = true
		}
	}
	if !isExists {
		ownerCandidate = append(ownerCandidate, userId)
	}

	rule.OwnerCandidate = ownerCandidate
	_, _, err := c.Upsert(ctx, rule)
	return err
}

func (c RuleCollectionMongo) Delete(ctx context.Context, rule Rule) (deleted bool, err error) {
	delres, err := c.DeleteOne(ctx, bson.M{"_id": rule.ID})
	if err != nil {
		return false, err
	}
	deleted = delres.DeletedCount != 0
	return deleted, nil
}

func (c RuleCollectionMongo) Upsert(ctx context.Context, rule Rule) (upsertRule Rule, deleted bool, err error) {
	ctx, cancel := context.WithTimeout(ctx, time.Second * 5)
	defer cancel()

	delres, err := c.DeleteOne(ctx, bson.M{"_id": rule.ID})
	if err != nil {
		return Rule{}, false, err
	}
	deleted = delres.DeletedCount != 0

	if !deleted {
		rule.ID = primitive.NewObjectID()
	}
	ins, err := c.InsertOne(ctx, rule)
	if err != nil {
		return Rule{}, false, err
	}
	insertID := ins.InsertedID.(primitive.ObjectID)
	insertRuleRes := c.FindOne(ctx, bson.M{"_id": insertID})
	insertRuleRes.Decode(&upsertRule)
	return upsertRule, deleted, nil
}

func (c RuleCollectionMongo) FindAll(ctx context.Context) (rules []Rule, err error) {
	ctx, cancel := context.WithTimeout(ctx, time.Second * 5)
	defer cancel()

	cur, err := c.Find(ctx, bson.M{})
	if err != nil {
		return nil, err
	}

	for cur.Next(ctx) {
		var v Rule
		if err = cur.Decode(&v); err != nil {
			log.Printf("デコードできないため、Ruleの取得をスキップしました。\n%s", err)
			continue
		}
		rules = append(rules, v)
	}

	return rules, nil
}

func (r Rule) IsOwner(userId string) (isOwner bool) {
	for _, owner := range r.Owner {
		if owner == userId {
			return true
		}
	}
	return false
}
