module main

go 1.14

require (
	github.com/gin-gonic/gin v1.7.3
	github.com/hashicorp/go-retryablehttp v0.7.0
	go.mongodb.org/mongo-driver v1.7.2
)
