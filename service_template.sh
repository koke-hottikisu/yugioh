#!/bin/bash -ex

usage() {
    cat <<EOF
$(basename ${0}) はyugiohを構成するサービスのテンプレートを作成するツールです

Usage:
    $(basename ${0}) service_name main_container_name
EOF
}

SERVICE_NAME=$1
CONTAINER_NAME=$2

rm -rf $SERVICE_NAME
mkdir -p $SERVICE_NAME/$CONTAINER_NAME

cp -r service_template/pod/. $SERVICE_NAME
cp -r service_template/go/. $SERVICE_NAME/$CONTAINER_NAME

find $SERVICE_NAME -type f | xargs sed -i -e "s/SED_SERVICE_NAME/$SERVICE_NAME/g"
find $SERVICE_NAME -type f | xargs sed -i -e "s/SED_CONTAINER_NAME/$CONTAINER_NAME/g"

find $SERVICE_NAME | xargs rename "s/SED_SERVICE_NAME/$SERVICE_NAME/;"
find $SERVICE_NAME | xargs rename "s/SED_CONTAINER_NAME/$CONTAINER_NAME/;"
